package tw.com.gcreate.righttime.views.tab1

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController

import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.zxing.integration.android.IntentIntegrator
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.swipCallback.ClinicNumberNotifySwipeToDeleteCallback
import tw.com.gcreate.righttime.adapters.tab1.ClinicNumberNotifyAdapter
import tw.com.gcreate.righttime.adapters.tab1.ClinicNumberNotifyAdapterPriv
import tw.com.gcreate.righttime.adapters.tab1.ClinicTypeAdapter
import tw.com.gcreate.righttime.adapters.tab1.HomeReservationRecordAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicTypeBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.DataBean
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.model.ModelDataList
import tw.com.gcreate.righttime.sharedpreferences.ReservationDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.TodayNotifyCardDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ReservationRecordModel
import tw.com.gcreate.righttime.util.PdfTool
import tw.com.gcreate.righttime.views.dialog.TeachDialogFragment
import tw.com.gcreate.righttime.views.tab1.customQRcode.CustomCaptureActivity
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestClinicRoomNumber
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ObjectBannerList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ObjectClinicRoomNumber
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel2

/** 首頁  */
class ClinicTypeFragment : Fragment() {

    private lateinit var binding: FragmentClinicTypeBinding
    private lateinit var navController: NavController

    private var mClinicNumberNotifyAdapter: ClinicNumberNotifyAdapter? = null   //  今日就診
    private var mRevisitReminderAdapter: ClinicNumberNotifyAdapterPriv? = null  //  回診提醒
    private var homeReservationRecordAdapter = HomeReservationRecordAdapter(mutableListOf())  //  預約掛號

    private var clinicNotifyNumberLength = 0
    private var clinicNotifyNumberParams: LinearLayout.LayoutParams? = null
    private var todayNotifyList: MutableList<ObjectNotifyCard2> = ArrayList()
    private var notifyListPriv: MutableList<ObjectNotifyCard2> = ArrayList()

    private var isFirstTime = true

    private lateinit var runnable: Runnable

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        private val mHandler = Handler(Looper.getMainLooper())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_type, container, false)
        navController = NavHostFragment.findNavController(this)

        isFirstTime = true

        /** TODO 使用 ViewModel. 在移除今日就診的卡片 snackBar 會導致閃退
        getModelInstance()
        setupBinding(makeApiCall())
         */

        intervalRefreshData()
        initView()
        recyclerViewBindData()

        if (model.appState.gCMemberId.get()!!.isNotEmpty()) {
            getReservationRecordList()
        }

        clinicNotifyNumberParams = binding.clinicNotifyNumberLayout.layoutParams as LinearLayout.LayoutParams
        clinicNotifyNumberLength = clinicNotifyNumberParams!!.height

        binding.refreshLayout.setOnRefreshListener { pinClinicNumbers() }

        //getPinClinicNumbers();
        global.isCheckVersion = false

        return binding.root
    }


    private fun initView() {
        binding.toolbar.materialToolbar.apply {
            title = ""
            setHasOptionsMenu(true)
            (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar.materialToolbar)
            setNavigationIcon(R.drawable.toolbar_icon_qrcode)
            setNavigationOnClickListener {
                val integrator = IntentIntegrator.forSupportFragment(this@ClinicTypeFragment)
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
                integrator.captureActivity = CustomCaptureActivity::class.java
                integrator.setPrompt("掃描QR碼")
                integrator.setCameraId(0)
                integrator.setBeepEnabled(true)
                integrator.setBarcodeImageEnabled(false)
                integrator.initiateScan()
            }
        }
        //  回診提醒 Block 延展箭頭
        binding.clinicNotifyNumberPrivBtn.setOnClickListener {
            if (binding.clinicNotifyNumberPriv.visibility == View.VISIBLE) {
                binding.clinicNotifyNumberPriv.visibility = View.GONE
                binding.imgArrowPriv.setImageResource(R.drawable.icon_unfold)
            } else {
                binding.clinicNotifyNumberPriv.visibility = View.VISIBLE
                binding.imgArrowPriv.setImageResource(R.drawable.icon_collapse)
            }
        }

        loadBanner()
        showViewOrHide()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbarfunction, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.go_to_teach -> {
                val teachDialogFragment: DialogFragment = TeachDialogFragment()
                teachDialogFragment.show(requireActivity().supportFragmentManager, "simple dialog")
            }
            R.id.action_search -> {
                val action = ClinicTypeFragmentDirections.actionClinicTypeFragmentToClinicSearchFragment()
                findNavController().navigate(action)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    /**   Banner function    */
    private fun loadBanner() {
        if(model.honeBannerList.get().isNullOrEmpty()){
            GcreateApiClient.getBanner(object : ApiController<ObjectBannerList>(requireActivity(), false) {
                override fun onSuccess(response: ObjectBannerList) {
                    model.honeBannerList.set(response)
                    setBanner(model.honeBannerList.get())
                }
                override fun onFail(httpResponse: Response<ObjectBannerList>): Boolean {
                    setBanner(null)
                    return super.onFail(httpResponse)
                }
            })
        }else{
            setBanner(model.honeBannerList.get())
        }
    }

    private fun setBanner(response: ObjectBannerList?) {
        val banner = binding.clinicTypeBanner.banner
        banner.layoutParams.height = ((global.windowHeightPixels * 0.123).toInt())

        if (response == null) {
            //  Set Default Data
            banner.setAdapter(object : BannerImageAdapter<DataBean>(DataBean.bannerImages()) {
                override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
                    val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(activity as Context, data.imageRes)[0])
                    Glide.with(holder.itemView).load(dm).into(holder.imageView)
                }
            }).addBannerLifecycleObserver(this@ClinicTypeFragment).indicator = CircleIndicator(activity)

        } else {
            val imagesUrl: MutableList<String> = ArrayList()
            for (item in response) {
                imagesUrl.add(item.url)
            }
            banner.setAdapter(object : BannerImageAdapter<String>(imagesUrl) {
                override fun onBindView(holder: BannerImageHolder, data: String, position: Int, size: Int) {
                    Glide.with(holder.itemView).load(imagesUrl[position]).into(holder.imageView)
                }
            }).addBannerLifecycleObserver(this@ClinicTypeFragment).indicator = CircleIndicator(activity)

            banner.setOnBannerListener { _, position ->
                if (response[position].link.isNotEmpty()) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(response[position].link))
                    startActivity(browserIntent)
//                    val intent = Intent(activity, WebViewActivity::class.java)
//                    intent.putExtra("bannerUrlLink", response[position].link)
//                    startActivity(intent)
                }
            }
        }

        // 輪播方式
        banner.setPageTransformer(AlphaPageTransformer())
            .setIndicatorSelectedColor(ContextCompat.getColor(activity as Context, R.color.colorYellow)).setIndicatorSpace(16)
            .setIndicatorWidth(15, 15).start()

    }

    /** QR code  scan feedback
     * */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 12345) {
            refreshListLayout()
        } else {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    Toast.makeText(activity, "已取消掃描", Toast.LENGTH_SHORT).show()
                } else if (result.contents.split("#").toTypedArray().size > 1 && result.contents.split("#").toTypedArray()[1] == "clinicID") {
                    val action = ClinicTypeFragmentDirections.actionClinicTypeFragmentToClinicInfoFragment()
                    action.clinicId = result.contents.split("#").toTypedArray()[2].toInt()
                    action.isComeFormQR = true
                    navController.navigate(action)
                } else {
                    AlertDialog.Builder(requireActivity()).setTitle("無效QR碼.").setMessage("請掃描診所特殊QR碼").setPositiveButton("重新掃描") { _, _ ->
                        val integrator = IntentIntegrator.forSupportFragment(this@ClinicTypeFragment)
                        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
                        integrator.captureActivity = CustomCaptureActivity::class.java
                        integrator.setPrompt("掃描QR碼")
                        integrator.setCameraId(0)
                        integrator.setBeepEnabled(true)
                        integrator.setBarcodeImageEnabled(false)
                        integrator.initiateScan()
                    }.setNegativeButton("取消") { _, _ -> }.show()
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    /** 畫面重整
     * */
    private fun refreshList() {
        notifyListPriv.clear()
        todayNotifyList.clear()
        for (i in TodayNotifyCardDataMaintain.getNotifyCardList(requireContext())) {
            if (i.date.toInt() - global.date.toInt() != 0) {
                notifyListPriv.add(i)
            } else {
                todayNotifyList.add(i)
            }
        }
        binding.refreshLayout.isRefreshing = false
    }

    private fun refreshListLayout() {
        refreshList()
//        viewModel!!.getTodayNotifyData()
        mClinicNumberNotifyAdapter!!.refresh(todayNotifyList)
        mRevisitReminderAdapter!!.refresh(notifyListPriv)

        homeReservationRecordAdapter.data = ReservationDataMaintain.getReservationDataList(requireContext())!!

        showViewOrHide()

    }

    /** RecyclerView Binding Data
     * **/
    private fun recyclerViewBindData() {
        /*  今日就診    */
        binding.clinicNotifyNumber.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            isNestedScrollingEnabled = false
            hasFixedSize()
        }

        /*  回診提醒 */
        binding.clinicNotifyNumberPriv.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        /*  選擇科別    */
        binding.clinictypeRecycle.layoutManager = GridLayoutManager(activity, 2)
        binding.clinictypeRecycle.adapter = ClinicTypeAdapter(navController, ModelDataList.getClinicTypeList())

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri?)
    }

    private fun pinClinicNumbers() {
        refreshList()

        if (isFirstTime) {
            /*  今日就診 */
            mClinicNumberNotifyAdapter = ClinicNumberNotifyAdapter(navController, todayNotifyList, requireView(), model)
            binding.clinicNotifyNumber.adapter = mClinicNumberNotifyAdapter

//                viewModel!!.getTodayNotifyData()

            /*  今日就診滑動刪除 */
            val itemTouchHelper = ItemTouchHelper(ClinicNumberNotifySwipeToDeleteCallback(
                mClinicNumberNotifyAdapter!!))
            itemTouchHelper.attachToRecyclerView(binding.clinicNotifyNumber)

            /*  回診提醒 */
            mRevisitReminderAdapter = ClinicNumberNotifyAdapterPriv(navController, notifyListPriv, requireView(), model)
            binding.clinicNotifyNumberPriv.adapter = mRevisitReminderAdapter


            /*  預約掛號 */
            binding.clinicReservationRecrd.apply {
                layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                //homeReservationRecordAdapter = HomeReservationRecordAdapter(mutableListOf())
                adapter = homeReservationRecordAdapter
            }
        }
        getClinicRoomNumbers()
        isFirstTime = false
    }

    private fun getClinicRoomNumbers() {
        if (TodayNotifyCardDataMaintain.getNotifyCardList(requireContext()).isEmpty()) return

        val roomIdList = mutableListOf<Int>()

        for (notifyCard in todayNotifyList) {
            roomIdList.add(notifyCard.roomId)
        }

        val requestBody = RequestClinicRoomNumber(roomIdList)
        GcreateApiClient.getClinicRoomNumber(requestBody, object : ApiController<MutableList<ObjectClinicRoomNumber>>(requireActivity(), false) {
            override fun onSuccess(response: MutableList<ObjectClinicRoomNumber>) {

                TodayNotifyCardDataMaintain.updateNotifyList(requireActivity(), response)

                refreshListLayout()

                clinicNotifyNumberParams!!.height = clinicNotifyNumberLength * todayNotifyList.size
                binding.clinicNotifyNumberLayout.layoutParams = clinicNotifyNumberParams

                binding.refreshLayout.isRefreshing = false
            }

            override fun onFail(httpResponse: Response<MutableList<ObjectClinicRoomNumber>>): Boolean {
                binding.refreshLayout.isRefreshing = false
                return super.onFail(httpResponse)
            }
        })
    }

    private fun intervalRefreshData() {
        runnable = Runnable {
            pinClinicNumbers()
            mHandler.postDelayed(runnable, 5000)
        }
        mHandler.postDelayed(runnable, 1000)
    }


    private fun showViewOrHide() {
        //  開關顯示回診提醒 Block
        if (notifyListPriv.isNullOrEmpty()) {
            binding.clinicNotifyNumberPrivBtn.visibility = View.GONE
        } else {
            binding.clinicNotifyNumberPrivBtn.visibility = View.VISIBLE
        }
        //  開關今日就診  Block
        if (todayNotifyList.isNullOrEmpty()) binding.clinicNotifyNumberLayout.visibility = View.GONE
        else binding.clinicNotifyNumberLayout.visibility = View.VISIBLE
        // 預約掛號

    }


    private fun getReservationRecordList() {
        // 4.2.1 根據串接方的使用者 Id 取得該名使用者所有預約紀錄
        val requestBody = RequestOhBotModel(
            "/appointmentReservation",
            "GET",
            mutableListOf(),
            mutableListOf(
                RequestQuestItem("originId", model.appState.gCMemberId.get()!!),
                RequestQuestItem("limit", "20"),
            ),
            null
        )
        GcreateApiClient.getOhBotDataForPost2(requestBody, object : ApiController<ResponseModel2>(requireActivity(), false) {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ResponseModel2) {
                val recordList = mutableListOf<ReservationRecordModel>()
                if (response.ohbot_obj != null){
                    for (item in response.ohbot_obj) {
                        if (item.AppointmentOrder.status == "wait") {
                            // 1.3
                            val transPath = "/shop/${item.ShopId}"
                            GcreateApiClient.getOhBotDataForGet1(transPath, item.Member.OrgId,
                                object : ApiController<ResponseModel1>(requireActivity(), false) {
                                    override fun onSuccess(response: ResponseModel1) {
                                        recordList.add(ReservationRecordModel(
                                            response.ohbot_obj!!.name,
                                            item.AppointmentServiceId,
                                            item.start.substring(0, 10),
                                            String.format("%s%02d%s",
                                                item.start.substring(0, 10),
                                                item.start.substring(11, 13).toInt() + 8,
                                                item.start.substring(13, 16))
                                        ))
                                        homeReservationRecordAdapter.refreshDate(binding, recordList)
                                    }
                                }
                            )
                        }
                    }
                }else{
                    Toast.makeText(requireActivity(),response.message,Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFail(httpResponse: Response<ResponseModel2>): Boolean {
                binding.clinicReservationRecordLayout.visibility = View.GONE
                return super.onFail(httpResponse)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mHandler.removeCallbacks(runnable)
    }

    /* Model test */
    /*
    private fun getOhBotData() {
        val token =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjllNGExMTUxLThjZmUtNDVlZi1iMWYxLTVmNWUyMjNjOTBjOCIsInRpbWVzdGFtcCI6MTY0Njc5MTYwNTY0OCwiaWF0IjoxNjQ2NzkxNjA1fQ.4UeZ8euD3XzZ9b_zRBvh94yk5mvFcF7yQfNmdLdlRV0"
        OhBotTechApiClient.getOhBotTechShopInfo(token, object : ApiController<ResponseShopInfo>(requireActivity(), true) {
            override fun onSuccess(response: ResponseShopInfo) {
                val responseBody = response[0]

            }

        })
    }

    private fun getModelInstance(): ClinicTypeViewModel? {
        if (viewModel == null) {
            viewModel =
                ViewModelProvider(requireActivity(), ClinicTypeViewModelModelFactory(binding, navController)).get(
                    ClinicTypeViewModel::class.java)
        }

        return viewModel
    }

    private fun setupBinding(viewModel: ClinicTypeViewModel) {
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()

        binding.clinicNotifyNumber.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = viewModel.getTodayNotifyAdapter()
            hasFixedSize()
            isNestedScrollingEnabled = false

        }
        /*  今日就診滑動刪除 */
        val itemTouchHelper = ItemTouchHelper(ClinicNumberNotifySwipeToDeleteCallback(viewModel.getTodayNotifyAdapter()))
        itemTouchHelper.attachToRecyclerView(binding.clinicNotifyNumber)
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): ClinicTypeViewModel {
        viewModel!!.getTodayNotifyListObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setTodayNotifyList(it)
        })

        viewModel!!.getTodayNotifyData()

        return viewModel!!
    }
    */
}