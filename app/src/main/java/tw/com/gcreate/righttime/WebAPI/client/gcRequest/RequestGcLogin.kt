package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestGcLogin(
    val cell_phone: String,
    val password: String,
    val fireBaseToken:String,
)