package tw.com.gcreate.righttime.views.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.aigestudio.wheelpicker.WheelPicker
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoReservationWheelPickerBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager


class ClinicInfoFragmentReservationWheelPicker(val mode: Int, val dataList: MutableList<String>) : DialogFragment() {

    private lateinit var binding: FragmentClinicInfoReservationWheelPickerBinding

    companion object {
        var serviceItemSelectedPosition = 0
        var serviceDoctorSelectedPosition = 0
        var serviceAvailableTimeSelectedPosition = 0
        var reservationRelativesSelectedPosition = 0
    }

    override fun onStart() {
        super.onStart()

        dialog!!.window!!.setLayout((global.windowWidthPixels * 0.8).toInt(), (global.windowHeightPixels * 0.5).toInt())
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_wheel_picker)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_info_reservation_wheel_picker, container, false)

        binding.imgCloseNP.setOnClickListener { dialog!!.dismiss() }

        when (mode) {
            1 -> {
                binding.NPTitle.text = "請選擇您的門診項目"
                serviceItemWheelPicker()
            }

            2 -> {
                binding.NPTitle.text = "請選擇門診醫生"
                serviceDoctorWheelPicker()
            }

            3 -> {
                binding.NPTitle.text = "請選擇門診日期"
                serviceAvailableTimeWheelPicker()
            }

            4 -> {
                binding.NPTitle.text = "請選擇掛號對象"
                reservationRelativeItemWheelPicker()
            }
        }

        return binding.root
    }

    private fun serviceItemWheelPicker() {
        val np = binding.numberPicker
        np.setSelectedItemPosition(serviceItemSelectedPosition, false)
        np.data = dataList
        np.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    serviceItemSelectedPosition = position
                    UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(1, dataList[position])
                    dialog!!.dismiss()
                }
            }
            override fun onWheelScrollStateChanged(state: Int) {}
        })
    }

    private fun serviceDoctorWheelPicker() {
        val np = binding.numberPicker
        np.setSelectedItemPosition(serviceDoctorSelectedPosition, false)
        np.data = dataList
        np.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    serviceDoctorSelectedPosition = position
                    UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(2, dataList[position])
                    dialog!!.dismiss()
                }
            }
            override fun onWheelScrollStateChanged(state: Int) {}
        })
    }

    private fun serviceAvailableTimeWheelPicker() {
        val np = binding.numberPicker
        np.setSelectedItemPosition(serviceAvailableTimeSelectedPosition, false)
        np.data = dataList
        np.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    serviceAvailableTimeSelectedPosition = position
                    UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(3, dataList[position])
                    dialog!!.dismiss()
                }
            }
            override fun onWheelScrollStateChanged(state: Int) {}
        })
    }

    private fun reservationRelativeItemWheelPicker() {
        val np = binding.numberPicker
        np.setSelectedItemPosition(reservationRelativesSelectedPosition, false)
        np.data = dataList
        np.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    reservationRelativesSelectedPosition = position
                    UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(4, dataList[position])
                    dialog!!.dismiss()
                }
            }

            override fun onWheelScrollStateChanged(state: Int) {}
        })
    }

}