package tw.com.gcreate.righttime.views.login.forgetPassword

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentForgetInputMobileBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseGcIsRegistered


class ForgetInputMobileFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentForgetInputMobileBinding.inflate(inflater, container, false)
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, true)

        binding.apply {

            tilMobile.editText!!.apply {
                addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        binding.tilMobile.error = null
                    }

                    override fun afterTextChanged(s: Editable?) {
                        if (s!!.isNotEmpty()) {
                            binding.btnCheck.apply {
                                alpha = 1.0f
                                isEnabled = true
                            }

                        } else {
                            binding.btnCheck.apply {
                                alpha = 0.5f
                                isEnabled = false
                            }
                        }
                    }
                })
                setOnEditorActionListener { _, _, _ ->
                    SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                    binding.tiePhone.clearFocus()
                    true
                }
            }

            btnCheck.setOnClickListener {
                if (binding.tilMobile.editText!!.text.trim().length < 10) {
                    binding.tilMobile.error = "輸入的手機號碼有誤。"
                } else {
                    val params = JsonObject()
                    params.addProperty("cell_phone", binding.tilMobile.editText!!.text.trim().toString())
                    GcreateApiClient.gcWhetherRegistered(params, object : ApiController<ResponseGcIsRegistered>(requireContext(), false) {
                        override fun onSuccess(response: ResponseGcIsRegistered) {
                            if (!response.member_id.isNullOrEmpty()) {
                                runBlocking {
                                    model.appState.setGCMemberId(response.member_id)
                                }
                                val action = ForgetInputMobileFragmentDirections.actionForgetInputMobileFragmentToOtpCheckFragment()
                                action.phone = binding.tilMobile.editText!!.text.toString()
                                findNavController().navigate(action)
                            } else {
                                showAlertDialog()
                            }
                        }
                    })

                }
            }
        }
        return binding.root
    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.apply {
            setTitle("")
            setMessage("此電話號碼尚未註冊。")
            setPositiveButton("確定") { dialog: DialogInterface?, _: Int ->
                dialog!!.dismiss()
            }
            setCancelable(false)
            create().show()
        }
    }
}