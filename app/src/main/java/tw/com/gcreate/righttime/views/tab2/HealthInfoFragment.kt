package tw.com.gcreate.righttime.views.tab2

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab2.HealthInfoContainAdapter
import tw.com.gcreate.righttime.adapters.tab2.HealthInfoCategoryAdapter
import tw.com.gcreate.righttime.util.layouManager.CenterLayoutManager
import tw.com.gcreate.righttime.databinding.FragmentHealthInfoBinding
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseHeHoArticle
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseHeHoCategory
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseHeHoCategoryItem
import java.util.*

@SuppressLint("NotifyDataSetChanged")
class HealthInfoFragment : Fragment() {

    private lateinit var binding: FragmentHealthInfoBinding
    private var healthInfoContainAdapter = HealthInfoContainAdapter(mutableListOf(), R.layout.card_healtinfocontent)
    private var healthInfoCategoryAdapter = HealthInfoCategoryAdapter(mutableListOf())

    companion object {
        @JvmField
        var healthTitleDefaultPosition = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHealthInfoBinding.inflate(inflater, container, false)

        getHeHoCategory()

        binding.rvHealthInfoItem.apply {
            val centerLayoutManager = CenterLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            layoutManager = centerLayoutManager

            adapter = healthInfoCategoryAdapter

            healthInfoCategoryAdapter.setOnItemClickListener { adapter, _, position ->
                healthTitleDefaultPosition = position
                centerLayoutManager.smoothScrollToPosition(binding.rvHealthInfoItem, RecyclerView.State(), position)
                binding.tvHealthInfoArticleTitle.text = (adapter.data[position] as ResponseHeHoCategoryItem).name
                healthInfoCategoryAdapter.notifyDataSetChanged()
                getHeHoArticle((adapter.data[position] as ResponseHeHoCategoryItem).type_id)
            }
        }

        binding.rvHealthInfoContainer.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = healthInfoContainAdapter
        }

        binding.refreshLayout.setOnRefreshListener {
            getHeHoArticle(healthInfoCategoryAdapter.data[healthTitleDefaultPosition].type_id)
        }

        return binding.root
    }

    private fun getHeHoCategory() {
        GcreateApiClient.getHehoCategory(object : ApiController<ResponseHeHoCategory>(requireActivity(), false) {
            override fun onSuccess(response: ResponseHeHoCategory) {
                Collections.sort(response) { o1, o2 ->
                    if (o1.type_id <= o2.type_id) {
                        -1
                    } else {
                        1
                    }
                }
                healthInfoCategoryAdapter.data = response
                healthInfoCategoryAdapter.notifyDataSetChanged()

                binding.tvHealthInfoArticleTitle.text = healthInfoCategoryAdapter.data[0].name
                getHeHoArticle(healthInfoCategoryAdapter.data[0].type_id)
            }
        })
    }

    private fun getHeHoArticle(typeId: Int) {
        GcreateApiClient.getHeHoArticle(typeId, object : ApiController<ResponseHeHoArticle>(requireActivity(), false) {
            override fun onSuccess(response: ResponseHeHoArticle) {
                if (typeId != 6) {
                    binding.rvHealthInfoContainer.adapter = HealthInfoContainAdapter(response, R.layout.card_healtinfocontent2)
                } else {
                    binding.rvHealthInfoContainer.adapter = HealthInfoContainAdapter(response, R.layout.card_healtinfocontent2)
                }
                healthInfoContainAdapter.notifyDataSetChanged()
                binding.refreshLayout.isRefreshing = false
            }
        })
    }

    override fun onStop() {
        super.onStop()
        healthTitleDefaultPosition = 0
    }

}