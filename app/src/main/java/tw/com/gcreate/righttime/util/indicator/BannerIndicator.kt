package tw.com.gcreate.righttime.util.indicator

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import tw.com.gcreate.righttime.R

/*
    診所介紹上方圖片輪播指示器
* */
class BannerIndicator @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
    private var number = 0
    private var position = 0
    private val paint = Paint()
    private val selectColor = 0
    private val unSelectColor = 0
    private val radius: Float
    private val space: Float
    private val colorActive = -0x38fb
    private val colorInactive = -0x1
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // indicator start position in window
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        val startPosition = width / 2 - (radius * 2 * number + space * (number - 1)) / 2
        canvas.save()
        for (i in 0 until number) {
            if (i == position) {
                paint.color = colorActive
            } else {
                paint.color = colorInactive
            }
            // indicator distance
            canvas.drawCircle(startPosition + radius * (1 * i + 1) + i * space, (height / 2).toFloat(), 10f, paint)
        }
        canvas.restore()
    }

    fun setNumber(number: Int) {
        this.number = number
    }

    fun setPosition(position: Int) {
        this.position = position
        invalidate()
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.BannerIndicator)
        radius = typedArray.getDimension(R.styleable.BannerIndicator_radius, 10f)
        space = typedArray.getDimension(R.styleable.BannerIndicator_space, 10f)
        typedArray.recycle()
    }
}