package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseClinicInfo(
    val address: String,
    val afternoonFrom: String,
    val afternoonTo: String,
    val announcement: String,
    val clinic_id: Int,
    val contact: String,
    val doctors: MutableList<Doctor>,
    val eveningFrom: String,
    val eveningTo: String,
    val extension: String,
    val intro: String,
    val isDetailOpen: Boolean,
    val isfull: Any,
    val lat: Double,
    val lng: Double,
    val morningFrom: String,
    val morningTo: String,
    val name: String,
    val open: Boolean,
    val pics: List<Pic>,
    val room: MutableList<Room>,
    val schedule: Schedule,
    val socialInfo: List<SocialInfo>,
    val type: List<Type>,
    val updated_time: String,
    var pic: String = "",
    val orgId: String?,
    val shopId: String?,
)

/** 社群資訊 */
data class SocialInfo(
    val facebook: String?,
    val line: String?,
    val website: String?,
)

/** 醫生資訊 */
data class Doctor(
    val education: List<String>,
    val experience: List<String>,
    val name: String,
    val pic: String,
    val sex: String,
)

/** 診所圖片*/
data class Pic(
    val content: String?,
    val seq: Int,
    val title: String,
    val type: String,
    val url: String,
)

/** 科別*/
data class Type(
    val name: String,
    val type_id: Int,
)

data class Room(
    val currentDoctor: String,
    val currentNum: Int,
    val name: String,
    val room_id: Int,
)

/** 日期*/
data class Schedule(
    val friday: Friday,
    val monday: Monday,
    val saturday: Saturday,
    val sunday: Sunday,
    val thursday: Thursday,
    val tuesday: Tuesday,
    val wednesday: Wednesday,
)

data class Sunday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Monday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Tuesday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Wednesday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Thursday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Friday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

data class Saturday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>,
)

