package tw.com.gcreate.righttime.adapters.tab4

import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.RvItemNotifyBinding
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseNotify

class NotifyAdapter(data: MutableList<ResponseNotify>) :
    BaseQuickAdapter<ResponseNotify, DataBindBaseViewHolder>(R.layout.rv_item_notify, data) {


    override fun convert(holder: DataBindBaseViewHolder, item: ResponseNotify) {

        val binding: RvItemNotifyBinding = holder.getViewBinding() as RvItemNotifyBinding
        binding.executePendingBindings()

        holder.setText(R.id.tv_notify_title, item.notifyTitle)
        holder.setText(R.id.tv_notify_subtitle, item.notifySubTitle)
        holder.setText(R.id.tv_notify_time, item.notifyTime)
    }

}