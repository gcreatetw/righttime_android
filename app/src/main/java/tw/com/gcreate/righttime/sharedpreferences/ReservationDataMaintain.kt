package tw.com.gcreate.righttime.sharedpreferences


import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ReservationRecordModel

object ReservationDataMaintain{
    var reservationDataList: MutableList<ReservationRecordModel> = mutableListOf()

    fun getReservationDataList(context: Context): MutableList<ReservationRecordModel>? {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val responseString = sp.getString("ReservationDataList", null)


        val gson = Gson()
        if (!responseString.isNullOrEmpty()){
            reservationDataList = gson.fromJson(responseString, object : TypeToken<MutableList<ReservationRecordModel>>() {}.type)
        }

        return reservationDataList
    }

    fun addReservationRecord(context: Context, newData: ReservationRecordModel) {
        reservationDataList!!.add(newData)
        saveReservationRecord(context,reservationDataList!!)
    }

    fun saveReservationRecord(context: Context, reservationDataList: MutableList<ReservationRecordModel>) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("ReservationDataList", Gson().toJson(reservationDataList))
        editor.apply()
    }


}