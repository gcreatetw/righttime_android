package tw.com.gcreate.righttime.adapters.tab5

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import tw.com.gcreate.righttime.R

class MemberCentre1Adapter(data: MutableList<String>) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.rv_item_member_centre, data) {

    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.tv_member_centre_item, item)

    }
}
