package tw.com.gcreate.righttime.webAPI.client.gcResponse


class ResponseClinicList : ArrayList<ClinicListItem>()

data class ClinicListItem(
    val address: String,
    var center_len: Double,
    val contact: String,
    val extension: String?,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    var open: Boolean,
    val pic: String?,
    val updated_time: String,
) : Comparable<ClinicListItem> {
    override fun compareTo(other: ClinicListItem): Int {
        return if (!this.open && other.open) {
            1
        } else if (center_len < other.center_len || this.open && !other.open) {
            -1
        } else {
            0
        }
    }
}