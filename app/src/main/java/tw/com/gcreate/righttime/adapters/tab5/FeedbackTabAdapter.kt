package tw.com.gcreate.righttime.adapters.tab5

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import tw.com.gcreate.righttime.views.tab5.feedback.RecommendFragment
import tw.com.gcreate.righttime.views.tab5.feedback.SuggestionFragment


class FeedbackTabAdapter(baseFragment: Fragment, private val dateList: List<String?>) : FragmentStateAdapter(baseFragment) {
    //tab數量
    override fun getItemCount(): Int {
        return dateList.size
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return RecommendFragment()
            1 -> return SuggestionFragment()
        }

        return RecommendFragment()
    }

}