package tw.com.gcreate.righttime.views.tab1.customQRcode

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.CompoundBarcodeView
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.CustomCaptureBinding

class CustomCaptureActivity : AppCompatActivity() {
    private var capture: CaptureManager? = null
    private var barcodeScannerView: CompoundBarcodeView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = CustomCaptureBinding.inflate(layoutInflater)
        setContentView(binding.root) // 自定义布局

        binding.clinicInfoToolbar.apply {
            materialToolbar.apply {
                setNavigationIcon(R.drawable.toolbar_icon_back)
                setNavigationOnClickListener {
                    onBackPressed()
                }
            }
            toolbarLogo.visibility = View.GONE
        }

        capture = CaptureManager(this, binding.dbvCustom)
        capture!!.initializeFromIntent(intent, savedInstanceState)
        capture!!.decode()
    }

    override fun onResume() {
        super.onResume()
        capture!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture!!.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture!!.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        capture!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == 4){
            finish()
        }
        return false
    }
}