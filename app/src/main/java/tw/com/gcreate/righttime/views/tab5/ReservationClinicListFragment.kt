package tw.com.gcreate.righttime.views.tab5

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab5.ReservationClinicListAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentReservationClinicListBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel2
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Shop

class ReservationClinicListFragment : Fragment() {

    private val mAdapter = ReservationClinicListAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentReservationClinicListBinding.inflate(inflater, container, false)

        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, false)
        getReservationClinicList()

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
            mAdapter.setEmptyView(R.layout.layout_no_reservation_record)
            mAdapter.setOnItemClickListener { adapter, _, position ->
                val action = ReservationClinicListFragmentDirections.actionReservationClinicListFragmentToReservationRecordFragment()
                action.reservationClinicName = (adapter.data[position] as Shop).name
                action.reservationOrgId = (adapter.data[position] as Shop).OrgId
                action.reservationShopId = (adapter.data[position] as Shop).id
                findNavController().navigate(action)
            }
        }

        return binding.root
    }

    private fun getReservationClinicList() {
        // 4.1.1 根據串接方的使用者 Id 取得該名使用者所註冊的所有店家
        val requestBody = RequestOhBotModel(
            "/member",
            "GET",
            mutableListOf(),
            mutableListOf(RequestQuestItem("originId", model.appState.gCMemberId.get()!!)),
            null
        )
        GcreateApiClient.getOhBotDataForPost2(requestBody, object : ApiController<ResponseModel2>(requireActivity(), false) {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ResponseModel2) {
                val shopList = mutableListOf<Shop>()
                if (response.ohbot_obj != null) {
                    for (item in response.ohbot_obj) {
                        shopList.add(item.Shop)
                    }
                    mAdapter.data = shopList
                    mAdapter.notifyDataSetChanged()
                } else {
                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}