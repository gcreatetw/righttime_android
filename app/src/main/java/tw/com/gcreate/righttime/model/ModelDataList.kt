package tw.com.gcreate.righttime.model

import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.webAPI.client.ClinicTypeDataBean
import tw.com.gcreate.righttime.webAPI.client.RegisteredInputData
import java.util.*

object ModelDataList {

    fun getClinicTypeList(): MutableList<ClinicTypeDataBean>{
        val dataList = mutableListOf<ClinicTypeDataBean>()
        dataList.add(ClinicTypeDataBean("2",R.drawable.category_icon_otolaryngology,"耳鼻喉科", "Otolaryngology\n"))
        dataList.add(ClinicTypeDataBean("4",R.drawable.category_icon_pediatrics,"小兒科", "Pediatrics\n"))
        dataList.add(ClinicTypeDataBean("3",R.drawable.category_icon_gynecology,"婦產科", "Gynecology\n"))
        dataList.add(ClinicTypeDataBean("8",R.drawable.category_icon_psychiatrist,"身心科", "Psychiatrist\n"))
        dataList.add(ClinicTypeDataBean("1",R.drawable.category_icon_chinese_medicine,"中醫", "Chinese Medicine\n"))
        dataList.add(ClinicTypeDataBean("6",R.drawable.category_icon_ophthalmology,"眼科", "Ophthalmology\n"))
        dataList.add(ClinicTypeDataBean("7",R.drawable.category_icon_dermatology,"皮膚科", "Dermatology\n"))
        dataList.add(ClinicTypeDataBean("10",R.drawable.category_icon_internal_medicine,"內科", "Chinese Medicine\n"))
        dataList.add(ClinicTypeDataBean("9",R.drawable.category_icon_family_medicine,"家醫科", "Family Medicine\n"))
        dataList.add(ClinicTypeDataBean("5",R.drawable.category_icon_dentistry,"牙科", "Dentistry\n"))
        dataList.add(ClinicTypeDataBean("13",R.drawable.category_icon_rehabilitation,"復健科", "Rehabilitation\n"))
        dataList.add(ClinicTypeDataBean("15",R.drawable.category_icon_psychological_counseling,"心理諮商", "Psychological\n counseling"))
        dataList.add(ClinicTypeDataBean("16",R.drawable.category_icon_physiotherapy,"物理治療", "Psychological\n"))
        dataList.add(ClinicTypeDataBean("11",R.drawable.category_icon_other,"其他", "Others\n"))

        return dataList
    }


    fun registeredData(): MutableList<RegisteredInputData> {
        val dataList = mutableListOf<RegisteredInputData>()
        val nameText = String.format("%-5s%s", "姓", "名")
        val genderText = String.format("%-5s%s", "姓", "別")
        val birthdayText = String.format("%-5s%s", "生", "日")

        dataList.add(RegisteredInputData(nameText, "請輸入姓名"))
        dataList.add(RegisteredInputData("身分證", "請輸入身分證號碼"))
        dataList.add(RegisteredInputData(genderText, "請輸入性別"))
        dataList.add(RegisteredInputData(birthdayText, "YYYY-MM-DD"))

        return dataList
    }
}