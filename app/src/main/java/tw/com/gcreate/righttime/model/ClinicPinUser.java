package tw.com.gcreate.righttime.model;

public class ClinicPinUser {
    int clinicId;
    String clinicName;
    int roomId;
    String roomName;
    int bookingNum;
    int notifyNum;

    public ClinicPinUser(String clinicName, int clinicId, String roomName, int roomId, int bookingNum, int notifyNum) {
        this.clinicName = clinicName;
        this.clinicId = clinicId;
        this.roomId = roomId;
        this.roomName = roomName;
        this.bookingNum = bookingNum;
        this.notifyNum = notifyNum;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public int getClinicId() {
        return clinicId;
    }

    public void setClinicId(int clinicId) {
        this.clinicId = clinicId;
    }

    public int getRoomId() {
        return roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getBookingNum() {
        return bookingNum;
    }

    public void setBookingNum(int bookingNum) {
        this.bookingNum = bookingNum;
    }

    public int getNotifyNum() {
        return notifyNum;
    }

    public void setNotifyNum(int notifyNum) {
        this.notifyNum = notifyNum;
    }
}
