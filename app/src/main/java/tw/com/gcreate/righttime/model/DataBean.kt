package tw.com.gcreate.righttime.model

import tw.com.gcreate.righttime.R

class DataBean {

    var imageRes: Int = 0
    var imageUrl: String? = null
    var title: String? = null
    var viewType: Int = 0
    lateinit var cityName_ch: String
    lateinit var cityName_en: String
    var cityID: Int = 0

    constructor(imageRes: Int, title: String?, viewType: Int) {
        this.imageRes = imageRes
        this.title = title
        this.viewType = viewType

    }

    constructor(imageUrl: String?, title: String?, viewType: Int) {
        this.imageUrl = imageUrl
        this.title = title
        this.viewType = viewType
    }

    //    constructor(imageUrl: String? , title: String? , viewType: Int) {
    //        this.imageUrl = imageUrl
    //        this.title = title
    //        this.viewType = viewType
    //    }

    constructor(imageRes: Int, cityName_ch: String, cityName_en: String, cityID: Int) {
        this.imageRes = imageRes
        this.cityName_ch = cityName_ch
        this.cityName_en = cityName_en
        this.cityID = cityID
    }

    companion object {

        //测试数据，如果图片链接失效请更换
        //        val bannerImageWithoutWeb: List<DataBean>
        //            get() {
        //                val list: MutableList<DataBean> = ArrayList()
        //                list.add(DataBean(Global.pdfToBitmap(mContext, )R.mipmap.home_bn01 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn02 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn03 , null , 1))
        //                return list
        //            }

        fun bannerImages(): List<DataBean> {
            val list: MutableList<DataBean> = ArrayList()
            list.add(DataBean(R.raw.bn_helo, null, 1))
            return list
        }

        fun initTeachImages(): List<DataBean> {
            val list: MutableList<DataBean> = ArrayList()
            list.add(DataBean(R.mipmap.teach_page_image01, null, 1))
            list.add(DataBean(R.mipmap.teach_page_image02, null, 1))
            list.add(DataBean(R.mipmap.teach_page_image03, null, 1))
            list.add(DataBean(R.mipmap.teach_page_image04, null, 1))
            list.add(DataBean(R.mipmap.teach_page_image05, null, 1))
            return list
        }
    }

}
