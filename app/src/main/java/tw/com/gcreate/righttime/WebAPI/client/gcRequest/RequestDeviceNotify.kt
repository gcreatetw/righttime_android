package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestDeviceNotify(
    val room_id: Int,
    val deviceToken: String,
    val targetNum: String,
    val notifyNum: String,
    val period: String,
    val date: String,
)