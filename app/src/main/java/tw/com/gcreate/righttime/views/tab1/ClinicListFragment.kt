package tw.com.gcreate.righttime.views.tab1

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab1.ClinicListAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicListBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.listener.UpdateTextListener
import tw.com.gcreate.righttime.listener.UpdateTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.views.dialog.TeachDialogFragment
import tw.com.gcreate.righttime.views.dialog.WheelPickerDialogFragment
import tw.com.gcreate.righttime.views.tab1.customQRcode.CustomCaptureActivity
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.Center
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestClinicList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ClinicListItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseCityList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicTypeList

/** 診所列表    */
class ClinicListFragment : Fragment(), UpdateTextListener {

    private lateinit var binding: FragmentClinicListBinding

    private val mAdapter = ClinicListAdapter(mutableListOf())
    private val args: ClinicListFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        var cityID = 0
        var clinicTypeID = 0
        private var userSelectCity: String = "周邊熱搜"
        private var userSelectClinicType: String = "全部科別"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_list, container, false)
        UpdateTextListenerManager.getInstance().registerListener(this)

        clinicTypeID = args.clinicTypeID
        userSelectClinicType = args.userSelectClinicType

        setView()
        getApiData()

        return binding.root
    }

    private fun getApiData() {
        if (model.cityList.size == 0) {
            GcreateApiClient.getCityList(object : ApiController<ResponseCityList>(requireActivity(), false) {
                override fun onSuccess(response: ResponseCityList) {
                    model.cityList.addAll(response)
                }

                override fun onFail(httpResponse: Response<ResponseCityList>): Boolean {
                    Toast.makeText(requireActivity(), resources.getText(R.string.text_rest_api_fail), Toast.LENGTH_SHORT).show()
                    return super.onFail(httpResponse)
                }
            })
        }

        if (model.clinicTypeList.size == 0) {
            GcreateApiClient.getClinicType(object : ApiController<ResponseClinicTypeList>(requireActivity(), false) {
                override fun onSuccess(response: ResponseClinicTypeList) {
                    model.clinicTypeList.addAll(response)
                }

                override fun onFail(httpResponse: Response<ResponseClinicTypeList>): Boolean {
                    Toast.makeText(requireActivity(), resources.getText(R.string.text_rest_api_fail), Toast.LENGTH_SHORT).show()
                    return super.onFail(httpResponse)
                }
            })
        }

        getClinicList()
    }

    private fun setView() {

        binding.apply {
            clinicListToolbar.materialToolbar.apply {
                title = ""
                setHasOptionsMenu(true)
                (activity as AppCompatActivity?)!!.setSupportActionBar(binding.clinicListToolbar.materialToolbar)
                setNavigationIcon(R.drawable.toolbar_icon_back)
                setNavigationOnClickListener {
                    findNavController().popBackStack()
                }
            }

            //  縣市
            tvCityNumberPicker.apply {
                if (userSelectCity != "周邊熱搜") text = userSelectCity
                setOnClickListener {
                    val dialogFragment: DialogFragment = WheelPickerDialogFragment(1)
                    dialogFragment.show(requireActivity().supportFragmentManager, "simple dialog")
                }
            }

            //  科別
            tvClinicTypeNumberPicker.apply {
                text = userSelectClinicType
                setOnClickListener {
                    val dialogFragment: DialogFragment = WheelPickerDialogFragment(2)
                    dialogFragment.show(requireActivity().supportFragmentManager, "simple dialog")
                }
            }

            rvClinicList.apply {
                hasFixedSize()
                layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                adapter = mAdapter
                mAdapter.setEmptyView(R.layout.layout_no_data)
                mAdapter.setOnItemClickListener { _, _, position ->
                    model.selectedClinicSimple.set(mAdapter.data[position])

                    val action = ClinicListFragmentDirections.actionClinicListFragmentToClinicInfoFragment()
                    action.clinicId = mAdapter.data[position].id
                    if (mAdapter.data[position].pic != null) {
                        action.clinicAvatarUrl = mAdapter.data[position].pic!!
                    }
                    action.isComeFormQR = false
                    findNavController().navigate(action)
                }
            }

            refreshLayout.setOnRefreshListener { getClinicList() }
        }

    }

    private fun getClinicList() {

        var inputCityId = ""
        var inputCenter: Center? = null
        val inputClinicList = arrayListOf<Int>()

        val inputClinicTypeId = if (clinicTypeID == 0) {
            ""
        } else {
            clinicTypeID.toString()
        }

        //  附近熱搜
        if (cityID == 0 && global.latitude < 200 && global.longitude < 200) {
            inputCenter = Center(10000, global.latitude, global.longitude)
        } else {
            inputCityId = cityID.toString()
        }

        val requestBody = RequestClinicList(
            inputCityId,
            inputClinicTypeId,
            inputClinicList,
            inputCenter
        )

        GcreateApiClient.getClinicList(requestBody, object : ApiController<ResponseClinicList>(requireActivity(), false) {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ResponseClinicList) {
                for (a in response) {
                    a.center_len = global.getDistance(a.lat, a.lng)
                }

                response.sortWith(
                    Comparator.comparing(ClinicListItem::open)
                        .reversed()
                        .thenComparing(ClinicListItem::center_len)
                )
                mAdapter.data = response
                mAdapter.notifyDataSetChanged()
                binding.refreshLayout.isRefreshing = false
            }

            override fun onFail(httpResponse: Response<ResponseClinicList>): Boolean {
                Toast.makeText(activity, "資料更新失敗，請檢查網路狀態", Toast.LENGTH_LONG).show()
                return super.onFail(httpResponse)
            }
        })

    }

    override fun updateWheelPicker(isSelectCity: Boolean, text: String) {
        //  Update 城市 or 診別 TextView
        if (isSelectCity) {
            binding.tvCityNumberPicker.text = text
            userSelectCity = text
        } else {
            binding.tvClinicTypeNumberPicker.text = text
            userSelectClinicType = text
        }
        //  更新顯示診所
        getClinicList()
    }

    override fun onStop() {
        super.onStop()
        UpdateTextListenerManager.getInstance().unRegisterListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cliniclist, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_search) {
            val action = ClinicListFragmentDirections.actionClinicListFragmentToClinicSearchFragment()
            binding.root.findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }
}