package tw.com.gcreate.righttime.views.tab3

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.swipCallback.ClinicNameCardSwipeToDeleteCallback
import tw.com.gcreate.righttime.adapters.tab3.FavClinicListAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentFavListBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.FavClinicListDataMaintain
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestClinicList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicList

class FavListFragment : Fragment() {

    private lateinit var binding: FragmentFavListBinding
    private var mAdapter: FavClinicListAdapter? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFavListBinding.inflate(inflater, container, false)

        binding.favList.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            mAdapter =
                FavClinicListAdapter(findNavController(), FavClinicListDataMaintain.getFavClinicDataList(requireContext()), binding.root, model)

            val itemTouchHelper = ItemTouchHelper(ClinicNameCardSwipeToDeleteCallback(mAdapter!!))
            itemTouchHelper.attachToRecyclerView(binding.favList)

            adapter = mAdapter
            mAdapter?.setEmptyView(R.layout.layout_no_favdata)
        }

        binding.refreshLayout.apply {
            setOnRefreshListener { getFavList() }
        }

        getClinicList()

        return binding.root
    }

    private fun getFavList() {
        if (FavClinicListDataMaintain.getFavClinicDataList(requireContext()).size > 0) {
            getClinicList()
        } else {
            binding.refreshLayout.isRefreshing = false
        }
    }

    private fun getClinicList() {

        val favClinicIDs = mutableListOf<Int>()
        val favDataList = FavClinicListDataMaintain.getFavClinicDataList(requireContext())

        for (item in favDataList) {
            favClinicIDs.add(item.id)
        }

        val requestBody = RequestClinicList("", "", favClinicIDs, null)
        GcreateApiClient.getClinicList(requestBody, object : ApiController<ResponseClinicList>(requireActivity(), false) {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ResponseClinicList) {
                for (i in response.indices) {
                    for (j in favDataList.indices) {
                        if (response[i].id == favDataList[j].id) {
                            favDataList[j].open = response[i].open
                        }
                    }
                }

                try {
                    FavClinicListDataMaintain.saveFavClinicList(requireContext(), favDataList)
                }catch (e:Exception){

                }

                mAdapter!!.refresh(favDataList)
                binding.refreshLayout.isRefreshing = false
            }

            override fun onFail(httpResponse: Response<ResponseClinicList>): Boolean {
                Toast.makeText(activity, "資料更新失敗，請檢查網路狀態", Toast.LENGTH_LONG).show()
                return super.onFail(httpResponse)
            }
        })
    }
}