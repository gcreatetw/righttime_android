package tw.com.gcreate.righttime.views.tab1

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab1.ClinicListAdapter
import tw.com.gcreate.righttime.adapters.tab1.SearchAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicSearchBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.SearchRecordDataMaintain
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestSearch
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ClinicListItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicList
import java.util.*

class ClinicSearchFragment : Fragment() {

    private lateinit var binding: FragmentClinicSearchBinding
    private lateinit var recordList: MutableList<String>
    private lateinit var mSearchAdapter: SearchAdapter
    private val mClinicListAdapter = ClinicListAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentClinicSearchBinding.inflate(inflater, container, false)
        recordList = SearchRecordDataMaintain.getSearchRecordDataList(requireContext())

        initView()

        return binding.root
    }

    private fun initView() {
        binding.searchToolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.colorWhite))
        (activity as AppCompatActivity).setSupportActionBar(binding.searchToolbar)

        binding.searchToolbar.setNavigationOnClickListener {
            requireView().findNavController().navigateUp()
        }

        binding.imgCancel.setOnClickListener {
            binding.apply {
                etSearch.setText("")
                lyRecordContent.visibility = View.VISIBLE
                lySearchContent.visibility = View.GONE
            }
        }

        binding.rvLatestSearch.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            mSearchAdapter = SearchAdapter(recordList)
            adapter = mSearchAdapter

            mSearchAdapter.setOnItemClickListener { adapter, view, position ->
                binding.apply {
                    etSearch.setText(recordList[position])
                    lyRecordContent.visibility = View.GONE
                    lySearchContent.visibility = View.VISIBLE
                }
                getSearchApiData(recordList[position])
            }

        }

        binding.rvSearchContent.apply {
            hasFixedSize()
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = mClinicListAdapter
            mClinicListAdapter.setEmptyView(R.layout.layout_no_data)
            mClinicListAdapter.setOnItemClickListener { _, _, position ->
                model.selectedClinicSimple.set(mClinicListAdapter.data[position])

                val action = ClinicSearchFragmentDirections.actionClinicSearchFragmentToClinicInfoFragment()
                action.clinicId = mClinicListAdapter.data[position].id
                if (mClinicListAdapter.data[position].pic != null) {
                    action.clinicAvatarUrl = mClinicListAdapter.data[position].pic!!
                }
                action.isComeFormQR = false
                findNavController().navigate(action)
            }
        }

        binding.etSearch.apply {

            setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    if (binding.etSearch.text.isNullOrBlank()) {
                        binding.lyRecordContent.visibility = View.VISIBLE
                        binding.lySearchContent.visibility = View.GONE
                    } else {
                        binding.lyRecordContent.visibility = View.GONE
                        binding.lySearchContent.visibility = View.VISIBLE
                    }
                } else {
                    if (binding.etSearch.text.isNullOrBlank()) {
                        binding.lyRecordContent.visibility = View.VISIBLE
                        binding.lySearchContent.visibility = View.GONE
                    }
                }
            }

            setOnEditorActionListener { v, actionId, event ->
                hideSoftKeyBroad()

                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.etSearch.clearFocus()
                    val keyword = binding.etSearch.text.toString().trim()
                    if (keyword.trim { it <= ' ' }.isNotEmpty()) {
                        saveSearchRecordKeyWord(keyword)
                        getSearchApiData(keyword)
                        binding.lyRecordContent.visibility = View.GONE
                        binding.lySearchContent.visibility = View.VISIBLE
                    }
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun saveSearchRecordKeyWord(keyword: String) {
        if (recordList.size == 5) {
            // 移除最舊的搜尋關鍵字
            recordList.removeAt(4)
            binding.rvLatestSearch.adapter?.notifyItemRemoved(4)
            // 加入最新的搜尋關鍵字
            recordList.add(0, keyword)
            binding.rvLatestSearch.adapter?.notifyItemInserted(0)
        } else {
            // 加入最新的搜尋關鍵字
            recordList.add(0, keyword)
            binding.rvLatestSearch.adapter?.notifyItemInserted(0)
        }

        hideSoftKeyBroad()
        SearchRecordDataMaintain.saveSearchRecordDataList(requireContext(), recordList)
    }

    private fun getSearchApiData(keyword: String) {
        val requestBody = RequestSearch(keyword)
        GcreateApiClient.search(requestBody, object : ApiController<ResponseClinicList>(requireActivity(), false) {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ResponseClinicList) {
                for (a in response) {
                    a.center_len = global.getDistance(a.lat, a.lng)
                }

                response.sortWith(
                    Comparator.comparing(ClinicListItem::open)
                        .reversed()
                        .thenComparing(ClinicListItem::center_len)
                )
                mClinicListAdapter.data = response
                mClinicListAdapter.notifyDataSetChanged()
            }

            override fun onFail(httpResponse: Response<ResponseClinicList>): Boolean {
                Toast.makeText(activity, "資料更新失敗，請檢查網路狀態", Toast.LENGTH_LONG).show()
                return super.onFail(httpResponse)
            }
        })
    }

}