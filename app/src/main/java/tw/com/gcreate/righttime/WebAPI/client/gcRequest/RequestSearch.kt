package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestSearch(
    val search: String,
)