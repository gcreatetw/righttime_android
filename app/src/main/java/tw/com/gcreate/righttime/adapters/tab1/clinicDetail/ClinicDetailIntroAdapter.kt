package tw.com.gcreate.righttime.adapters.tab1.clinicDetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Pic

class ClinicDetailIntroAdapter(private val picsBeans: List<Pic>) : RecyclerView.Adapter<ClinicDetailIntroAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.clinic_intro, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val picsBean = picsBeans!![position]
        //輪播
//        ResponseClinicInfo.PicsBean picsBean = picsBeans.get(position % picsBeans.size());
//        holder.intro_ImageView.setImageDrawable(R.mipmap.doctor_banner);
        Glide.with(holder.itemView).load(picsBean.url).into(holder.intro_ImageView)
        holder.intro_Title.text = picsBean.title
        holder.intro_Content.text = picsBean.content
    }

    override fun getItemCount(): Int {
        return picsBeans?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val intro_Title: TextView
        val intro_Content: TextView
        val intro_ImageView: ImageView

        init {
            intro_ImageView = itemView.findViewById<View>(R.id.intro_ImageVIew) as ImageView
            intro_Title = itemView.findViewById<View>(R.id.intro_Title) as TextView
            intro_Content = itemView.findViewById<View>(R.id.intro_Content) as TextView
        }
    }
}