package tw.com.gcreate.righttime.util

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.ProgressInfo
import tw.com.gcreate.righttime.views.login.OtpType
import tw.com.gcreate.righttime.views.login.forgetPassword.OtpCheckFragmentDirections
import java.util.concurrent.TimeUnit

class OtpUtil(private val mActivity: FragmentActivity, val model: MainViewModel, val auth: FirebaseAuth, private val otpType: OtpType) {

    private val TAG = "OtpLog"

    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    companion object {
        private var storedVerificationId: String = ""
        var isAlreadyGetOTP = false
        var resendToken000 = PhoneAuthProvider.ForceResendingToken.zza()
    }

    /** 參考 DOC
     * https://firebase.google.com/docs/auth/android/phone-auth?authuser=1#enable-app-verification
     * */
    fun getOTP(fragment: Fragment, phoneForVerify: String) {
        val phoneNumber = "+886$phoneForVerify"

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(fragment, auth, credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {

                when (e) {
                    is FirebaseAuthInvalidCredentialsException -> {
                        mActivity.findViewById<TextView>(R.id.tv_error_message).text = "您已暫時被停用，請稍後再試"
                    }
                    is FirebaseTooManyRequestsException -> {
                        mActivity.findViewById<TextView>(R.id.tv_error_message).text = "簡訊驗證請求次數超過，請稍後再試"
                    }
                    else -> {
                        mActivity.findViewById<TextView>(R.id.tv_error_message).text = "簡訊驗證請求次數超過，請稍後再試"
                    }
                }
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {

                storedVerificationId = verificationId
                resendToken = token
                resendToken000 = token
                isAlreadyGetOTP = true

            }
        }
        if (isAlreadyGetOTP) {
            Log.d(TAG, "resendVerificationCode")
            resendVerificationCode(phoneNumber, resendToken000, auth)
        } else {
            Log.d(TAG, "startPhoneNumberVerification")
            startPhoneNumberVerification(phoneNumber, auth)
        }
    }

    private fun startPhoneNumberVerification(phoneNumber: String, auth: FirebaseAuth) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)                // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(mActivity)                     // Activity (for callback binding)
            .setCallbacks(callbacks)                    // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    var mobile = ""

    fun verifyPhoneNumberWithCode(fragment: Fragment, OTP: String, phone: String) {
        mobile = phone
        if (storedVerificationId.isNotEmpty()) {
            val credential = PhoneAuthProvider.getCredential(storedVerificationId, OTP)
            signInWithPhoneAuthCredential(fragment, auth, credential)
        }
    }

    private fun resendVerificationCode(phoneNumber: String, token: PhoneAuthProvider.ForceResendingToken?, auth: FirebaseAuth) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)                // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(mActivity)                     // Activity (for callback binding)
            .setCallbacks(callbacks)                    // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token)        // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }

    private fun signInWithPhoneAuthCredential(fragment: Fragment, auth: FirebaseAuth, credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(mActivity) { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                val user = task.result?.user
                Log.d(TAG, "OTP verify success")
                if (otpType == OtpType.Registed) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        fragment.requireView().findViewById<View>(R.id.ly_register_otp).visibility = View.GONE
                        fragment.requireView().findViewById<View>(R.id.ly_register_info).visibility = View.VISIBLE
                    }, 1000)
                } else if (otpType == OtpType.ForgetPassword) {
                    val action = OtpCheckFragmentDirections.actionOtpCheckFragmentToResetPasswordFragment()
                    fragment.findNavController().navigate(action)
                }
            } else {
                Log.d(TAG, "OTP verify fail")
                // Sign in failed, display a message and update the UI

                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    // The verification code entered was invalid
                    Log.d(TAG, " The verification code entered was invalid = " + task.exception)
                }

            }
        }
    }
}