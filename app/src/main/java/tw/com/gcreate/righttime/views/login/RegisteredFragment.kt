package tw.com.gcreate.righttime.views.login

import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentLogintypeRegisteredBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListener
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.model.ModelDataList
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.util.OtpUtil
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.views.dialog.WheelPickerDialogFragment
import tw.com.gcreate.righttime.views.membercentre.DatePickerDialogFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.*
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseGcIsRegistered
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat


class RegisteredFragment(private val mode: LoginType, private val orgId: String) : Fragment(), UpdateWheelPickerTextListener {

    private lateinit var binding: FragmentLogintypeRegisteredBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLogintypeRegisteredBinding.inflate(inflater, container, false)
        UpdateWheelPickerTextListenerManager.getInstance().registerListener(this)

        setView()

        return binding.root
    }

    private fun setView() {

        binding.lyRegisterOtp.apply {
            fixTvPhone.text = String.format("%-5s%s", "手", "機")

            tvGetOtp.setOnClickListener {
                if (edMobileInput.text.length < 10) {
                    tvErrorMessage.text = "請輸入正確的手機號碼"
                } else {
                    tvErrorMessage.text = ""
                    edMobileInput.clearFocus()
                    SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)

                    // OTP 前判斷有無註冊過
                    val paramObject = JsonObject()
                    paramObject.addProperty("cell_phone", edMobileInput.text.toString())
                    GcreateApiClient.gcWhetherRegistered(paramObject, object : ApiController<ResponseGcIsRegistered>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseGcIsRegistered) {
                            if (response.member_id.isNullOrEmpty()) {
                                // 尚未註冊
                                OtpUtil(requireActivity(), model, Firebase.auth, OtpType.Registed).getOTP(this@RegisteredFragment,
                                    edMobileInput.text.toString())
                            } else {
                                // 已經註冊.
                                val errorString = "此電話號碼已經註冊"
                                tvErrorMessage.text = errorString
                            }
                        }
                    })
                }
            }

            btnRegisterCheck.setOnClickListener {
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                when {
                    edOtpInput.text.isNullOrEmpty() -> {
                        val errorString = "請輸入OTP驗證碼"
                        tvErrorMessage.text = errorString
                    }
                    edOtpInput.text.length < 6 -> {
                        val errorString = "OTP 驗證碼格式不符"
                        tvErrorMessage.text = errorString
                    }
                    else -> {
                        OtpUtil(requireActivity(), model, Firebase.auth, OtpType.Registed).verifyPhoneNumberWithCode(
                            this@RegisteredFragment,
                            edOtpInput.text.toString(),
                            edMobileInput.text.toString())
                    }
                }
            }

            btnCancel.setOnClickListener {
                findNavController().popBackStack()
            }

            tvPolicy.setOnClickListener {
                val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToPolicyWebViewFragment()
                findNavController().navigate(action)
            }
        }

        binding.lyRegisterInfo.apply {
            fixTvPassword.text = String.format("%-5s%s", "密", "碼")
            tvInputPassword.editText!!.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                    tvInputPassword.clearFocus()
                }
                true
            }

            val dataList = ModelDataList.registeredData()

            lyRegisterName.apply {
                fixTvInputTitle.text = dataList[0].title
                edInputContent.hint = dataList[0].hintText
            }

            lyRegisterIdentifyId.apply {
                fixTvInputTitle.text = dataList[1].title
                edInputContent.apply {
                    hint = dataList[1].hintText
                    inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    maxEms = 10
                    filters = arrayOf<InputFilter>(LengthFilter(10))

                    val digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
                    keyListener = DigitsKeyListener.getInstance(digits)
                    setRawInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME)

                    setOnEditorActionListener { _, actionId, _ ->
                        if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                            textCheck(editableText.toString())
                            SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                            clearFocus()
                        }
                        true
                    }
                }
            }

            lyRegisterGender.apply {
                fixTvInputTitle.text = dataList[2].title
                edInputContent.apply {
                    hint = dataList[2].hintText
                    isFocusable = false
                    isClickable = true
                    setOnClickListener {
                        val dialogFragment: DialogFragment = WheelPickerDialogFragment(3)
                        dialogFragment.show(requireActivity().supportFragmentManager, "")
                    }
                }
            }

            lyRegisterBirthday.apply {
                fixTvInputTitle.text = dataList[3].title
                edInputContent.apply {
                    hint = dataList[3].hintText
                    isFocusable = false
                    isClickable = true
                    setOnClickListener {
                        val datePickerDialogFragment = DatePickerDialogFragment(null)
                        datePickerDialogFragment.show(requireActivity().supportFragmentManager, "")
                    }
                }
            }

            btnRegisterCheck.setOnClickListener {
                // 註冊GC會員
                registeredGcreateMember()
            }

            btnCancel.setOnClickListener {
                findNavController().popBackStack()
            }

        }
    }

    override fun updateWheelPicker(mode: Int, textString: String?) {
        when (mode) {
            4 -> binding.lyRegisterInfo.lyRegisterGender.edInputContent.setText(textString.toString())
            5 -> binding.lyRegisterInfo.lyRegisterBirthday.edInputContent.setText(textString.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateWheelPickerTextListenerManager.getInstance().unRegisterListener(this)
    }

    private fun textCheck(inputText: String) {
        var errorMessage = ""
        val charArray = inputText.toCharArray()
        if (inputText.isEmpty()) {
            errorMessage = "請輸入身分證號碼"
        } else if (charArray[0] > 'Z' || charArray[0] < 'A') {
            errorMessage = "身分證格式不符"
        } else {
            for (i in 1 until charArray.size) {
                if (charArray[i] > '9' || charArray[i] < '0') {
                    errorMessage = "身分證格式不符"
                }
            }
        }
        binding.lyRegisterInfo.tvErrorMessage.text = errorMessage
    }

    private fun registeredGcreateMember() {
        val inputMobile = binding.lyRegisterOtp.edMobileInput.text.toString()
        val inputPassword = binding.lyRegisterInfo.tvInputPassword.editText!!.text.toString()
        val inputName = binding.lyRegisterInfo.lyRegisterName.edInputContent.text.toString()
        val inputIdentifyId = binding.lyRegisterInfo.lyRegisterIdentifyId.edInputContent.text.toString()
        val inputGender = binding.lyRegisterInfo.lyRegisterGender.edInputContent.text.toString()
        val inputBirthday = binding.lyRegisterInfo.lyRegisterBirthday.edInputContent.text.toString()

        if (inputPassword.isNotEmpty() && inputName.isNotEmpty() && inputIdentifyId.isNotEmpty() &&
            inputGender.isNotEmpty() && inputBirthday.isNotEmpty()
        ) {
            // call registered api
            val gender = if (inputGender == "男性") "M" else "F"
            val requestBody = RequestGcRegistered(inputMobile, inputPassword, inputIdentifyId, inputName, inputBirthday, gender, global.FireBasToken)

            GcreateApiClient.gcRegistered(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (response.success) {
                        // GC 註冊成功 換註冊思言會員。但要先取得 GC member id
                        runBlocking {
                            model.appState.setGCLoginMobile(inputMobile)
                            model.appState.setGCLoginPassword(inputPassword)
                        }
                        getGcreateMemberId()
                    }
                }
            })

        } else {
            val errorMessage = "請完整輸入個人資訊"
            binding.lyRegisterInfo.tvErrorMessage.text = errorMessage
        }
    }

    private fun getGcreateMemberId() {
        val paramObject = JsonObject()
        paramObject.addProperty("cell_phone", binding.lyRegisterOtp.edMobileInput.text.toString())
        GcreateApiClient.gcWhetherRegistered(paramObject, object : ApiController<ResponseGcIsRegistered>(requireActivity(),
            false) {
            override fun onSuccess(response: ResponseGcIsRegistered) {

                // 存下 GC memberID
                runBlocking {
                    model.appState.setGCMemberId(response.member_id!!)
                }
                // TODO
                if (orgId != "QQ") {
                    createOhBotMember(response.member_id!!)
                } else {
                    setOhBotIdToGcDB(response.member_id!!, "null")
                }
            }
        })
    }

    private fun createOhBotMember(gcMemberId: String) {
        // 2.1.2 建⽴⽤⼾使⽤者
        val inputMobile = binding.lyRegisterOtp.edMobileInput.text.toString()
        val inputName = binding.lyRegisterInfo.lyRegisterName.edInputContent.text.toString()
        val inputGender = binding.lyRegisterInfo.lyRegisterGender.edInputContent.text.toString()
        val inputBirthday = binding.lyRegisterInfo.lyRegisterBirthday.edInputContent.text.toString()
        val gender = if (inputGender == "男性") "male" else "female"

        val requestBody = RequestOhBotModel(
            "/user/createClientUser",
            "POST",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(),
            RequestOhBotUserCreate("right_time", gcMemberId, inputMobile, inputName, inputBirthday, gender)
        )

        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                // 將思言會員ID 傳回 GC DB
                setOhBotIdToGcDB(gcMemberId, response.ohbot_obj!!.id)
            }
        })
    }

    private fun setOhBotIdToGcDB(gcMemberId: String, ohbotId: String) {
        val relativeList = RelativesDataMaintain.getRelativesDataList(requireContext())
        val inputUserName = binding.lyRegisterInfo.lyRegisterName.edInputContent.text.toString()
        val inputPersonalId = binding.lyRegisterInfo.lyRegisterIdentifyId.edInputContent.text.toString()
        val inputUserBirthday = binding.lyRegisterInfo.lyRegisterBirthday.edInputContent.text.toString()

        relativeList.add(Relative(inputUserName, "自己", inputPersonalId, inputUserBirthday))
        RelativesDataMaintain.saveRelativesList(requireContext(), relativeList)

        val requestBody = RequestUserInfoModify(gcMemberId, null, ohbotId, RelativesDataMaintain.getRelativesDataList(requireContext()))

        GcreateApiClient.gcUserInfoModify(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
            override fun onSuccess(response: ResponseSimpleFormat) {
                // 註冊流程結束。
                if (mode == LoginType.ClinicInfo) {
                    // 進到診所資訊
                    val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToClinicInfoFragmentReservation()
                    findNavController().navigate(action)
                } else {
                    // 回會員資訊
                    val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToMemberCentreFragment2()
                    findNavController().navigate(action)
                }
            }
        })
    }
}