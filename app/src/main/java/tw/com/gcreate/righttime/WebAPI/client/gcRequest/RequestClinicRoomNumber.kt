package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestClinicRoomNumber(
    val rooms: MutableList<Int>
)