package tw.com.gcreate.righttime.views.reservation.deprecated

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentMemberCentreAddMemberBinding
import tw.com.gcreate.righttime.listener.UpdateReservationTextListenerManager
import tw.com.gcreate.righttime.listener.UpdateVaccineTextListenerManager
import tw.com.gcreate.righttime.util.SomeFunc


class MemberCentreFragmentAddMember() : Fragment() , TextView.OnEditorActionListener {

    private lateinit var binding: FragmentMemberCentreAddMemberBinding

    companion object {
        var isReservation = true
    }


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_member_centre_add_member , container , false)

        binding.edMedicalMember.setOnEditorActionListener(this)
        binding.edMedicalMemberId.setOnEditorActionListener(this)
        binding.edMedicalMemberBirthday.setOnEditorActionListener(this)


        val navController = Navigation.findNavController(requireActivity() , R.id.clinic_info_reservation_container)


        return binding.root
    }

    override fun onEditorAction(v: TextView? , actionId: Int , event: KeyEvent?): Boolean {
        SomeFunc.hideSoftKeyBoard(requireActivity() , binding.root)
        binding.edMedicalMember.clearFocus()
        binding.edMedicalMemberId.clearFocus()
        binding.edMedicalMemberBirthday.clearFocus()

        return true
    }


}