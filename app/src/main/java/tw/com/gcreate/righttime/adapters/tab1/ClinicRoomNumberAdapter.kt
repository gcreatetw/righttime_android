package tw.com.gcreate.righttime.adapters.tab1

import android.annotation.SuppressLint
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.views.dialog.SetNumberDialogFragment
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Room


import java.text.SimpleDateFormat
import java.util.*


class ClinicRoomNumberAdapter(
    private val navController: NavController, dataList: MutableList<Room>, private val model: MainViewModel,
) : BaseQuickAdapter<Room, DataBindBaseViewHolder>(R.layout.clinic_room_number_card, dataList) {

    @SuppressLint("NotifyDataSetChanged")
    fun refresh(roomData: MutableList<Room>) {
        data = roomData
        notifyDataSetChanged()
    }

    override fun convert(holder: DataBindBaseViewHolder, item: Room) {
        holder.setText(R.id.textClinicRoom, item.name)
        holder.setText(R.id.textClinicRoomNumber, item.currentNum.toString())

        val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.TAIWAN)
        val dts = sdf.format(Date())
        holder.setText(R.id.textUpdateTime, "更新於: $dts")

        holder.itemView.setOnClickListener {
            val clinicInfo = model.selectedClinicInfo.get()!!
            global.isComeFromClinicInfo = true

            model.tempNotifyCardInfo.set(ObjectNotifyCard2(
                clinicInfo.clinic_id,
                clinicInfo.name,
                clinicInfo.pic,
                item.room_id,
                item.name,
                item.currentDoctor,
                item.currentNum,
                0,
                0,
                "",
                clinicInfo.morningFrom,
                clinicInfo.morningTo,
                clinicInfo.afternoonFrom,
                clinicInfo.afternoonTo,
                clinicInfo.eveningFrom,
                clinicInfo.eveningTo
            ))

            val newFragment: DialogFragment = SetNumberDialogFragment(false, navController)
            newFragment.show((holder.itemView.context as FragmentActivity).supportFragmentManager, "tag")
        }
    }
}