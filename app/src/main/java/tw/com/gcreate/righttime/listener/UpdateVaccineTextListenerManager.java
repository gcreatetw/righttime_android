package tw.com.gcreate.righttime.listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class UpdateVaccineTextListenerManager {
    /**
     * 單例模式
     */
    public static UpdateVaccineTextListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private final List<UpdateReservationTextListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static UpdateVaccineTextListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new UpdateVaccineTextListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(UpdateReservationTextListener iListener) {
        if (iListenerList.size() != 0) {
            iListenerList.removeAll(iListenerList);
        }
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(UpdateReservationTextListener iListener) {
        iListenerList.remove(iListener);
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(boolean isNewMember,String text) {
        for (UpdateReservationTextListener updateNameListener : iListenerList) {
            updateNameListener.UpdateReservationMemberText(isNewMember,text);

        }
    }


}

