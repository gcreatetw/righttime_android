package tw.com.gcreate.righttime.webAPI;

import java.util.List;

public class ObjectFBLatestNews {

    /**
     * data : [{"
     * full_picture":"https://external.xx.fbcdn.net/safe_image.php?d=AQBd0TUkSZNQsmNu&url=https%3A%2F%2Fs.yimg.com%2Fos%2Fmit%2Fmedia%2Fm%2Fsocial%2Fimages%2Fsocial_default_logo-1481777.png&_nc_hash=AQAo-X2dxlY0P6IE","message":"A型肝炎 在韓國蔓延。台灣 106年以後出生的寶貝是有提供補助的疫苗接種。\n\n但是！！！50歲以下的大家是沒有抗體的！\n\n預防的最快方法就是施打疫苗⋯\n提醒 要去韓國或是其他東南亞國家遊玩的 ，可以考慮先打 A肝疫苗喔^_^","created_time":"2019-10-09T05:29:11+0000","id":"229016187579945_698423043972588"},{"full_picture":"https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/72430564_697855674029325_5189675164907864064_n.jpg?_nc_cat=109&_nc_oc=AQkyxwkfAtsGVCTAq-qj1R7e_EXoZiZ5ZND49rFmHgEB3YZtkbZKSmsFjGg2f0xGjqQ&_nc_ht=scontent.xx&oh=5cf9ca49de18fc99c95226fb1efb2fd6&oe=5E639718","message":"10月起週二晚上也是雙線診唷～\n歡迎多加利用：）\n\n# 週一到週五晚診皆雙線看診\n# 快放假了","created_time":"2019-10-08T09:59:24+0000","id":"229016187579945_697855694029323"},{"message":"謝謝雪花般飄來的應徵信\n但是我們診所在新竹新莊街\n不是台北新莊喔..","created_time":"2019-10-07T16:26:19+0000","id":"229016187579945_697371017411124"},{"full_picture":"https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/72268619_693039591177600_3636933694427496448_o.jpg?_nc_cat=102&_nc_oc=AQmpHFbprqJfVhXD9xPlbYAABH3HUzJfcMT_P2XeiIkKT6utQTyFYbHr2lkNZbptdu4&_nc_ht=scontent.xx&oh=da541fe802c781fad0b6e2889e7e43f7&oe=5E2AAA11","message":"小朋友在等看診時的創作（驚）\n這麼有天份 \n能不按讚嗎！\n\n# 張醫師小時侯也很愛畫畫：）\n# 是不是應該辦個兒童畫圖比賽呢","created_time":"2019-10-01T15:12:19+0000","id":"229016187579945_693041261177433"},{"message":"今晚雙診\n不讓您等\n邱黃聯手\n協力防守","created_time":"2019-10-01T09:55:14+0000","id":"229016187579945_692861781195381"}]
     * paging : {"cursors":{"before":"Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16YzVNVGN5TnpNek9USTNNelE0TVRRMU5nOE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmprNE5ESXpNRFF6T1RjeU5UZAzREd1IwYVcxbEJsMmRjQ2NC","after":"Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16a3lNRGMxTkRZAME5qTXhOakEzTkRNeE13OE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmpVNU5UYzNOakUwTlRJek56azREd1IwYVcxbEJsMU8xNDhC"},"next":"https://graph.facebook.com/v3.0/229016187579945/posts?access_token=EAAS8LGISx9wBAAA63SGCPvaMpdErnzXVncg2LcPm8UWKK2k2sPijKMztPI3RffHtTo4JoSVZBL1It0dpZAIusIymg4l7tEaTfAmjOBVZCgGSoXNFOta9DGnXvwSYmgZAVesghtnoDZCDRIfIZCIDYYI6JPlWZBdCg5bE8w5oPWzagZDZD&pretty=1&fields=full_picture%2Cmessage%2Ccreated_time&limit=25&after=Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16a3lNRGMxTkRZAME5qTXhOakEzTkRNeE13OE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmpVNU5UYzNOakUwTlRJek56azREd1IwYVcxbEJsMU8xNDhC"}
     */

    private PagingBean paging;
    private List<DataBean> data;

    public PagingBean getPaging() {
        return paging;
    }

    public void setPaging(PagingBean paging) {
        this.paging = paging;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class PagingBean {
        /**
         * cursors : {"before":"Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16YzVNVGN5TnpNek9USTNNelE0TVRRMU5nOE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmprNE5ESXpNRFF6T1RjeU5UZAzREd1IwYVcxbEJsMmRjQ2NC","after":"Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16a3lNRGMxTkRZAME5qTXhOakEzTkRNeE13OE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmpVNU5UYzNOakUwTlRJek56azREd1IwYVcxbEJsMU8xNDhC"}
         * next : https://graph.facebook.com/v3.0/229016187579945/posts?access_token=EAAS8LGISx9wBAAA63SGCPvaMpdErnzXVncg2LcPm8UWKK2k2sPijKMztPI3RffHtTo4JoSVZBL1It0dpZAIusIymg4l7tEaTfAmjOBVZCgGSoXNFOta9DGnXvwSYmgZAVesghtnoDZCDRIfIZCIDYYI6JPlWZBdCg5bE8w5oPWzagZDZD&pretty=1&fields=full_picture%2Cmessage%2Ccreated_time&limit=25&after=Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16a3lNRGMxTkRZAME5qTXhOakEzTkRNeE13OE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmpVNU5UYzNOakUwTlRJek56azREd1IwYVcxbEJsMU8xNDhC
         */

        private CursorsBean cursors;
        private String next;

        public CursorsBean getCursors() {
            return cursors;
        }

        public void setCursors(CursorsBean cursors) {
            this.cursors = cursors;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public static class CursorsBean {
            /**
             * before : Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16YzVNVGN5TnpNek9USTNNelE0TVRRMU5nOE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmprNE5ESXpNRFF6T1RjeU5UZAzREd1IwYVcxbEJsMmRjQ2NC
             * after : Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXlNamt3TVRZAeE9EYzFOems1TkRVNk16a3lNRGMxTkRZAME5qTXhOakEzTkRNeE13OE1ZAWEJwWDNOMGIzSjVYMmxrRHg4eU1qa3dNVFl4T0RjMU56azVORFZAmTmpVNU5UYzNOakUwTlRJek56azREd1IwYVcxbEJsMU8xNDhC
             */

            private String before;
            private String after;

            public String getBefore() {
                return before;
            }

            public void setBefore(String before) {
                this.before = before;
            }

            public String getAfter() {
                return after;
            }

            public void setAfter(String after) {
                this.after = after;
            }
        }
    }

    public static class DataBean {
        /**
         * full_picture : https://external.xx.fbcdn.net/safe_image.php?d=AQBd0TUkSZNQsmNu&url=https%3A%2F%2Fs.yimg.com%2Fos%2Fmit%2Fmedia%2Fm%2Fsocial%2Fimages%2Fsocial_default_logo-1481777.png&_nc_hash=AQAo-X2dxlY0P6IE
         * message : A型肝炎 在韓國蔓延。台灣 106年以後出生的寶貝是有提供補助的疫苗接種。
         * <p>
         * 但是！！！50歲以下的大家是沒有抗體的！
         * <p>
         * 預防的最快方法就是施打疫苗⋯
         * 提醒 要去韓國或是其他東南亞國家遊玩的 ，可以考慮先打 A肝疫苗喔^_^
         * created_time : 2019-10-09T05:29:11+0000
         * id : 229016187579945_698423043972588
         */

        private String full_picture;
        private String message;
        private String created_time;
        private String id;

        public String getFull_picture() {
            return full_picture;
        }

        public void setFull_picture(String full_picture) {
            this.full_picture = full_picture;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
