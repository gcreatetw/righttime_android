package tw.com.gcreate.righttime.adapters.tab1

import android.annotation.SuppressLint
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import com.chad.library.adapter.base.BaseQuickAdapter
import com.google.android.material.snackbar.Snackbar
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.ClinicNumberNotifyCardBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.sharedpreferences.TodayNotifyCardDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.views.dialog.SetNumberDialogFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestDeviceNotify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseDeviceNotify
import java.text.SimpleDateFormat
import java.util.*

class ClinicNumberNotifyAdapter(
    private val navController: NavController,
    private var notifyCardList: MutableList<ObjectNotifyCard2>,
    var v: View,
    val model: MainViewModel,
) : BaseQuickAdapter<ObjectNotifyCard2, DataBindBaseViewHolder>(R.layout.clinic_number_notify_card, notifyCardList) {


    var listener: View.OnClickListener? = null
    private var deletedNotifyCard: ObjectNotifyCard2? = null
    private var deletedNotifyCardPosition = 0

    private val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.TAIWAN)

    override fun convert(holder: DataBindBaseViewHolder, item: ObjectNotifyCard2) {
        val binding: ClinicNumberNotifyCardBinding = holder.getViewBinding() as ClinicNumberNotifyCardBinding
        binding.executePendingBindings()

        holder.setText(R.id.textClinicName, item.clinicName)
        holder.setText(R.id.textClinicRoom, item.roomName)
        holder.setText(R.id.textClinicRoomNumber, item.currentNum.toString())
        holder.setText(R.id.textClinicNotifyNumber, item.bookingNum.toString())

        holder.setText(R.id.textUpdateTimeNotifyCard, "更新於: ${sdf.format(Date())}")


        holder.itemView.setOnClickListener {
            model.tempNotifyCardInfo.set(item)

            val newFragment: DialogFragment = SetNumberDialogFragment(true, navController)
            newFragment.show((holder.itemView.context as FragmentActivity?)!!.supportFragmentManager, "dialog")
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun refresh(list: MutableList<ObjectNotifyCard2>) {
        data = list
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        deletedNotifyCard = notifyCardList[position]
        deletedNotifyCardPosition = position
        notifyCardList.removeAt(position)

        TodayNotifyCardDataMaintain.removeFromNotifyList(context,
            deletedNotifyCard!!.roomId,
            deletedNotifyCard!!.date,
            deletedNotifyCard!!.bookingTime)
        refresh(notifyCardList)
        showUndoSnackBar()
    }

    private fun showUndoSnackBar() {
        val view = v.findViewById<View>(R.id.clinic_notify_number)
        val snackbar = Snackbar.make(view, R.string.text_snack_bar, Snackbar.LENGTH_LONG)
        snackbar.setAction(R.string.snack_bar_undo) { undeleteItem() }
        snackbar.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(snackbar: Snackbar, event: Int) {
                if (event == DISMISS_EVENT_TIMEOUT) {
                    val requestBody = RequestDeviceNotify(deletedNotifyCard!!.roomId,
                        global.FireBasToken,
                        "",
                        "",
                        deletedNotifyCard!!.bookingTime,
                        global.dateToApiFormat(deletedNotifyCard!!.date))

                    GcreateApiClient.setDeviceNotify(global.UUID, requestBody, object : ApiController<ResponseDeviceNotify>(context, false) {
                        override fun onSuccess(response: ResponseDeviceNotify) {
                            if (response.success) {
                                Toast.makeText(context, context.resources.getText(R.string.text_notify_del_success), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(context, context.resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                            }
                        }

                        override fun onFail(httpResponse: Response<ResponseDeviceNotify>): Boolean {
                            Toast.makeText(context, context.resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                            return super.onFail(httpResponse)
                        }
                    })
                }
            }

            override fun onShown(snackbar: Snackbar) {}
        })
        snackbar.show()
    }

    private fun undeleteItem() {
        notifyCardList.add(deletedNotifyCardPosition, deletedNotifyCard!!)
        TodayNotifyCardDataMaintain.addToNotifyList(context, deletedNotifyCard!!)
        refresh(notifyCardList)
    }

}