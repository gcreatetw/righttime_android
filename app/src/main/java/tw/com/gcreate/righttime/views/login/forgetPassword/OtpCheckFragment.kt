package tw.com.gcreate.righttime.views.login.forgetPassword

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentOtpCheckBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.OtpUtil
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.views.login.OtpType

class OtpCheckFragment : Fragment() {

    private val args: OtpCheckFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentOtpCheckBinding.inflate(inflater, container, false)
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, true)

        showAlertDialog()

        val optUtil = OtpUtil(requireActivity(), model, Firebase.auth, OtpType.ForgetPassword)


        optUtil.getOTP(this, args.phone)

        binding.apply {
            fixTvPhone.apply {
                val showText = String.format("輸入傳送給 +886%s的驗證碼", args.phone.substring(1, args.phone.length))
                val spannableString = SpannableString(showText)
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
                spannableString.setSpan(colorSpan, 6, 19, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString
            }

            tilOpt.editText!!.apply {
                addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        binding.tilOpt.error = null
                    }

                    override fun afterTextChanged(s: Editable?) {
                        if (s!!.isNotEmpty()) {
                            binding.btnCheck.apply {
                                alpha = 1.0f
                                isEnabled = true
                            }

                        } else {
                            binding.btnCheck.apply {
                                alpha = 0.5f
                                isEnabled = false
                            }
                        }
                    }
                })
                setOnEditorActionListener { _, _, _ ->
                    binding.tieOtp.clearFocus()
                    binding.tilOpt.clearFocus()
                    SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                    true
                }
            }

            fixTvNoRecieveOtp.setOnClickListener {
                optUtil.getOTP(this@OtpCheckFragment, args.phone)
            }

            btnCheck.setOnClickListener {
                if (binding.tilOpt.editText!!.text.trim().length < 6) {
                    binding.tilOpt.error = "輸入的驗證碼有誤。"
                } else {
                    optUtil.verifyPhoneNumberWithCode(this@OtpCheckFragment, binding.tieOtp.text!!.trim().toString(), args.phone)
                }
            }

        }

        return binding.root
    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.apply {
            setTitle("")
            setMessage("簡訊驗證碼已送出。")
            setPositiveButton("確定") { dialog: DialogInterface?, _: Int ->
                dialog!!.dismiss()
            }
            setCancelable(false)
            create().show()
        }
    }

}