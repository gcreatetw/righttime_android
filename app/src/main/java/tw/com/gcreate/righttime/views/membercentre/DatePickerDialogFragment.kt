package tw.com.gcreate.righttime.views.membercentre

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.DialogDatepickerBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import java.util.*


class DatePickerDialogFragment(private val dateString: String?) : DialogFragment() {

    private lateinit var binding: DialogDatepickerBinding
    private var day = 1
    private var month = 1
    private var year = 2021


    override fun onStart() {
        super.onStart()
        dialog!!.window!!.setLayout((global.windowWidthPixels * 0.8).toInt(), (global.windowHeightPixels * 0.5).toInt())
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_wheel_picker)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Locale.setDefault(Locale.TAIWAN)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_datepicker, container, false)

        val datePicker = binding.datePicker

        if (dateString != null) {
            datePicker.init(
                dateString.substring(0, 4).toInt(),
                dateString.substring(5, 7).toInt() - 1,
                dateString.substring(8, 10).toInt(),
                null)
        }

        binding.btnCheck.setOnClickListener {
            day = datePicker.dayOfMonth
            month = datePicker.month + 1
            year = datePicker.year

            if (dateString == null) {
                val feedbackText = String.format("%s-%02d-%02d", year, month, day)
                UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(5, feedbackText)
            } else {
                val feedbackText = String.format("%s-%02d-%02d", year, month, day)
                UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(6, feedbackText)
            }

            dismiss()
        }

        binding.imgCloseNP.setOnClickListener {
            dismiss()
        }

        return binding.root
    }

}