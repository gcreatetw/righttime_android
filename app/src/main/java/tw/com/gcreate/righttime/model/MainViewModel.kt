package tw.com.gcreate.righttime.model

import androidx.databinding.ObservableField
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.webAPI.client.gcResponse.*

class MainViewModelFactory(
    private val dataStore: DataStore<Preferences>,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(dataStore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class MainViewModel(
    private val dataStore: DataStore<Preferences>,
) : ViewModel() {

    val appState = AppState(dataStore)

    var selectedClinicInfo = ObservableField<ResponseClinicInfo>()
    var selectedClinicSimple = ObservableField<ClinicListItem>()
    var tempNotifyCardInfo = ObservableField<ObjectNotifyCard2>()

    var me = ObservableField<CrashlyticsReport.Session.User>(
    )

    val cityList = mutableListOf<CityListItem>()
    val clinicTypeList = mutableListOf<ClinicTypeItem>()

    var honeBannerList = ObservableField<ObjectBannerList>()
    var clinicBannerList = ObservableField<ObjectBannerList>()

}