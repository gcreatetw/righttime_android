package tw.com.gcreate.righttime.webAPI.client

import com.google.gson.JsonObject
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.gcRequest.*
import tw.com.gcreate.righttime.webAPI.client.gcResponse.*
import tw.com.gcreate.righttime.webAPI.server.ApiClient


object GcreateApiClient {

    // 登入
    fun gcLogin(requestBody: RequestGcLogin, controller: ApiController<ResponseGcLogin>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.gcLogin(headers, requestBody))
    }

    // 註冊
    fun gcRegistered(requestBody: RequestGcRegistered, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.gcRegistered(headers, requestBody))
    }

    // 判斷電話有無註冊
    fun gcWhetherRegistered(requestBody: JsonObject, controller: ApiController<ResponseGcIsRegistered>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.gcWhetherRegistered(headers, requestBody))
    }

    // 會員資料修改
    fun gcUserInfoModify(requestBody: RequestUserInfoModify, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.gcUserInfoModify(headers, requestBody))
    }


    // App version
    fun getAppVersion(controller: ApiController<ResponseAppVersion>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getAppVersion(headers))
    }

    // 全臺縣市
    fun getCityList(controller: ApiController<ResponseCityList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getCityList(headers))
    }


    // Banner
    fun getBanner(controller: ApiController<ObjectBannerList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getBannerList(headers))
    }

    fun getClinicBanner(controller: ApiController<ObjectBannerList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getClinicBannerList(headers))
    }

    /* 診所相關*/
    // 診所科別
    fun getClinicType(controller: ApiController<ResponseClinicTypeList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getClinicType(headers))
    }

    // 診所列表
    fun getClinicList(requestBody: RequestClinicList, controller: ApiController<ResponseClinicList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getClinicList(headers, requestBody))
    }

    // 取得診所的診間號碼
    fun getClinicRoomNumber(requestBody: RequestClinicRoomNumber, controller: ApiController<MutableList<ObjectClinicRoomNumber>>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getClinicRoomNumber(headers, requestBody))
    }

    // 單一診所資訊
    fun getClinicInfo(clinicId: Int, controller: ApiController<ResponseClinicInfo>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getClinicInfo(headers, clinicId))
    }

    // 搜尋
    fun search(requestBody: RequestSearch, controller: ApiController<ResponseClinicList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.search(headers, requestBody))
    }

    // 推薦 & 問題回饋
    fun sendFeedBack(requestBody: RequestFeedback, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.sendFeedBack(headers, requestBody))
    }

    // 通知提醒 & 取消提醒
    fun setDeviceNotify(deviceID: String, requestBody: RequestDeviceNotify, controller: ApiController<ResponseDeviceNotify>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Device-ID"] = deviceID
        controller.enqueue(ApiClient.gcreateApi.setDeviceNotify(headers, requestBody))
    }

    /* Tab2 **/
    // HeHO 分類
    fun getHehoCategory(controller: ApiController<ResponseHeHoCategory>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getHehoCategory(headers))
    }

    fun getHeHoArticle(typeId: Int, controller: ApiController<ResponseHeHoArticle>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getHeHoArticle(headers, typeId))
    }

    /* Tab4 推播 **/
    fun getNotifications(gcMemberId: String, controller: ApiController<ResponseNotification>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.gcreateApi.getNotifications(headers, gcMemberId))
    }

    /* GC x Ohbot */
    fun getOhBotDataForGet1(trans_path: String, orgId: String, controller: ApiController<ResponseModel1>) {
        controller.enqueue(ApiClient.gcreateApi.getOhBotDataForGet1(trans_path, orgId))
    }

    fun getOhBotDataForGet2(trans_path: String, orgId: String, controller: ApiController<ResponseModel2>) {
        controller.enqueue(ApiClient.gcreateApi.getOhBotDataForGet2(trans_path, orgId))
    }

    fun getOhBotDataForPost1(requestBody: RequestOhBotModel, controller: ApiController<ResponseModel1>) {
        controller.enqueue(ApiClient.gcreateApi.getOhBotDataForPost1(requestBody))
    }

    fun getOhBotDataForPost2(requestBody: RequestOhBotModel, controller: ApiController<ResponseModel2>) {
        controller.enqueue(ApiClient.gcreateApi.getOhBotDataForPost2(requestBody))
    }
}