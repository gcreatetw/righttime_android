package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseModel1(
    val success: Boolean,
    val message: String?,
    val ohbot_obj: OhBotObject? ,
)

data class ResponseModel2(
    val success: Boolean,
    val message: String?,
    val ohbot_obj: MutableList<OhBotObject>?,
)

data class OhBotObject(
    val id: String,
    val name: String,
    val phone: String,
    val address: String,
    val createdAt: String,
    val updatedAt: String,
    val ContractPlanId: String,
    val OrgId: String,
    val DepositWalletId: String,
    val AdminUseWalletId: String,
    val SalesWalletId: String,
    val SalesDepositWalletId: String,
    val AppointmentDepositWalletId: String,
    val ContractPlan: ContractPlan,
    val count: Int,
    val role: String,
    val roleAllShop: Boolean,
    val customizeFeatures: Any,
    val allowAllOrg: Boolean,
    val allowOrgs: List<Any>,
    val isOhbotSuccessAdmin: Boolean,
    val isRemove: Boolean,
    val UserInfo: UserInfo,
    val Members: MutableList<Member>,
    val level: Int,
    val isBlock: Boolean,
    val origin: String?,
    val originText: String?,
    val UserInfoId: String,
    val WalletId: String?,
    val order: Int,
    val isPublic: Boolean,
    val AppointmentServices: List<AppointmentService>,
    val Image: Image?,
    val description: String,
    val showPrice: String,
    val showTime: Int,
    val AppointmentUnits: List<AppointmentUnit>,
    val AppointmentServiceAttaches: List<AppointmentServiceAttache>,
    val timeUnit: Int,
    val times: List<String>,
    val code: String,
    val status: String,
    val start: String,
    val end: String,
    val peopleCount: Int,
//    val skipPeopleCount: Int,
    val userType: String,
    val userName: String,
    val userPhone: String,
    val userComment: String,
    val adminComment: String,
    val numberCodeCount: Int,
    val tomorrowTimerNotifyId: String?,
    val autoCompletedTimerNotifyId: String?,
    val numberCode: Int?,
    val remark: String?,
    val totalBookingTime: Int,
    val totalShowTime: Int,
    val totalPrice: Int,
    val isOvertime: Boolean,
    val fromNotSpecify: Boolean,
    val isOther: Boolean,
    val MemberId: String,
    val ShopId: String,
    val UserId: String,
    val AppointmentOrderId: String,
    val AppointmentUnitId: String,
    val AppointmentServiceId: String,
    val OhbookBookingId: String?,
    val AppointmentOrder: AppointmentOrder,
    val AppointmentUnit: AppointmentUnit,
    val AppointmentService: AppointmentService,
    val Member: Member,
    val OhbookBooking: OhbookBooking,
    val msg: String,
    val Shop: Shop,
)