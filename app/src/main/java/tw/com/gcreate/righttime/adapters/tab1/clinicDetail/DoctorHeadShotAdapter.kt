package tw.com.gcreate.righttime.adapters.tab1.clinicDetail

import android.annotation.SuppressLint
import android.graphics.Color
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.DoctorHeadshotBinding
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Doctor

class DoctorHeadShotAdapter(data: MutableList<Doctor>) : BaseQuickAdapter<Doctor, DataBindBaseViewHolder>(R.layout.doctor_headshot, data) {

    companion object {
        var currentPosition = 0
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun convert(holder: DataBindBaseViewHolder, item: Doctor) {
        val binding: DoctorHeadshotBinding = holder.getViewBinding() as DoctorHeadshotBinding
        binding.executePendingBindings()

        Glide.with(context).load(item.pic).error(R.drawable.icon_doctor).into(binding.doctorHeadshotImg)
        holder.setText(R.id.doctorHeadshotName, String.format("%s 醫生", item.name))

        if (getItemPosition(item) == currentPosition) {
            binding.doctorHeadshotImg.borderColor = Color.parseColor("#FFFFC705")
            binding.doctorHeadshotImg.borderWidth = 2f
            binding.doctorHeadshotName.setTextColor(Color.parseColor("#FFFFC705"))
        } else {
            binding.doctorHeadshotImg.borderColor = Color.parseColor("#FF222222")
            binding.doctorHeadshotImg.borderWidth = 2f
            binding.doctorHeadshotName.setTextColor(Color.parseColor("#FF222222"))
        }
    }
}