package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestFeedback(
    val title: String,
    val content: String,
    val type: String,
    val email: String,
)