package tw.com.gcreate.righttime.webAPI.client.gcResponse

import tw.com.gcreate.righttime.webAPI.client.gcRequest.Relative

data class ResponseGcLogin(
    val birth: String,
    val cell_phone: String,
    val login: Boolean,
    val member_id: String,
    val name: String,
    val ohbot_id: String,
    val person_id: String,
    val relatives: MutableList<Relative>,
    val sex: String,
    val success: Boolean
)