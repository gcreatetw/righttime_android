package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestUserInfoModify(
    val member_id: String,
    val new_password: String?,
    val ohbot_id: String?,
    val relatives: MutableList<Relative>?
)