package tw.com.gcreate.righttime.webAPI.client.gcRequest

/** @param MemberId 要預約服務的人的 ID
 *  @param AppointmentUnitId 要預約那位服務人員的 ID
 *  @param AppointmentServiceId 要預約那個服務的 ID
 *  @param AppointmentServiceAttachIds 附加服務 ID
 *  @param start 預約時段
 *  @param userComment 備註
 * */
data class RequestOhBotServiceReservationCreate(
    val MemberId: String,
    val AppointmentUnitId: String,
    val AppointmentServiceId: String,
    val AppointmentServiceAttachIds: List<String>,
    val start: String,
    val userComment: String
)