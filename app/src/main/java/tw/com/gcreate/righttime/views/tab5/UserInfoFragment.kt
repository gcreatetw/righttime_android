package tw.com.gcreate.righttime.views.tab5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentMemberUserInfoBinding
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListener
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.views.membercentre.DatePickerDialogFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.Relative
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestUserInfoModify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat


class UserInfoFragment : Fragment(), UpdateWheelPickerTextListener {

    private lateinit var binding: FragmentMemberUserInfoBinding
    private var dateString: String = ""

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMemberUserInfoBinding.inflate(inflater, container, false)
        UpdateWheelPickerTextListenerManager.getInstance().registerListener(this)

        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbarMember2.materialToolbar, "", binding.toolbarMember2.toolbarLogo, false)

        val relativesList = RelativesDataMaintain.getRelativesDataList(requireContext())


        for (item in relativesList) {
            if (item.kinship == "自己") {
                binding.apply {
                    tvMemberName.setText(item.name)
                    tvMemberPersonalId.setText(item.personalId)
                    tvMemberBirthday.text = item.birthday
                    dateString = item.birthday
                }
            }
        }

        binding.btnDelete.setOnClickListener {
            val action = UserInfoFragmentDirections.actionMemberCentreFragment2ToPolicyWebViewFragment()
            action.linkType = 1
            findNavController().navigate(action)
        }

        binding.btnOk.apply {
            setOnClickListener {
                if (text.contains("編輯")) {
                    text = "    儲存    "
                    binding.apply {
                        tvMemberName.isEnabled = true
                        tvMemberPersonalId.isEnabled = true
                        tvMemberBirthday.isEnabled = true
                        tvMemberBirthday.setOnClickListener {
                            val datePickerDialogFragment = DatePickerDialogFragment(dateString)
                            datePickerDialogFragment.show(requireActivity().supportFragmentManager, "upgrade_dialog")
                        }
                    }
                } else if (text.contains("儲存")) {
                    text = "    編輯    "
                    binding.apply {
                        tvMemberName.isEnabled = false
                        tvMemberPersonalId.isEnabled = false
                        tvMemberBirthday.isEnabled = false
                        val relativeDataList = RelativesDataMaintain.getRelativesDataList(requireContext())
                        val inputRelativeName = tvMemberName.text.toString()
                        val inputRelativeKinship = "自己"
                        val inputRelativePersonalId = tvMemberPersonalId.text.toString()
                        val inputRelativeBirthday = tvMemberBirthday.text.toString()

                        if (inputRelativeName.isNotEmpty() && inputRelativeKinship.isNotEmpty() && inputRelativePersonalId.isNotEmpty() && inputRelativeBirthday.isNotEmpty()) {
                            relativeDataList.removeAt(0)
                            relativeDataList.add(0, Relative(inputRelativeName, inputRelativeKinship, inputRelativePersonalId, inputRelativeBirthday))

                            val requestBody = RequestUserInfoModify(model.appState.gCMemberId.get()!!, null, "", relativeDataList)

                            GcreateApiClient.gcUserInfoModify(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    RelativesDataMaintain.saveRelativesList(requireContext(), relativeDataList)
                                }
                            })

                        } else {
                            Toast.makeText(requireActivity(), "欄位不能空白", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        return binding.root
    }

    override fun updateWheelPicker(mode: Int, textString: String?) {
        dateString = textString!!
        if (mode == 6) {
            binding.tvMemberBirthday.text = textString
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateWheelPickerTextListenerManager.getInstance().unRegisterListener(this)
    }

}