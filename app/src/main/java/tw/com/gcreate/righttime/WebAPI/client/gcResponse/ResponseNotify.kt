package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseNotify(
    val notifyTitle: String,
    val notifySubTitle: String,
    val notifyTime: String,
)

