package tw.com.gcreate.righttime.listener

import android.view.View
import java.util.*

internal abstract class ClickListener : View.OnClickListener {
    private var lastClickTime: Long = 0
    private var id = -1

    companion object {
        private const val MIN_CLICK_DELAY_TIME = 1000 // 设置1秒内只能点击一次
    }


    override fun onClick(v: View) {
        val currentTime: Long = Calendar.getInstance().timeInMillis
        val mId: Int = v.id
        if (id != mId) {
            id = mId
            lastClickTime = currentTime
            onNoDoubleClick(v)
            return
        }
        if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime
            onNoDoubleClick(v)
        }
    }

    protected abstract fun onNoDoubleClick(v: View?)


}