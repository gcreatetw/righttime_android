package tw.com.gcreate.righttime.views.dialog

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.aigestudio.wheelpicker.WheelPicker.OnWheelChangeListener
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.DialogWheelPickerBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.listener.UpdateTextListenerManager
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.views.tab1.ClinicListFragment.Companion.cityID
import tw.com.gcreate.righttime.views.tab1.ClinicListFragment.Companion.clinicTypeID
import java.util.*

class WheelPickerDialogFragment(private val mode: Int) : DialogFragment() {

    private lateinit var binding: DialogWheelPickerBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        private var wheelPickerSelectedPosition = 0
    }

    override fun onStart() {
        super.onStart()

        dialog!!.window!!.setLayout((global.windowWidthPixels * 0.8).toInt(), (global.windowHeightPixels * 0.5).toInt())
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.bg_wheel_picker)

    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_wheel_picker, container, false)

        binding.imgCloseNP.setOnClickListener { dialog!!.dismiss() }

        when (mode) {
            1 -> {
                binding.NPTitle.text = "請選擇區域"
                binding.NPSubtitle.text = "* 周邊熱搜範圍為 10km *"
                cityWheelPicker()
            }
            2 -> {
                binding.NPTitle.text = "請選擇科別"
                binding.NPSubtitle.visibility = View.INVISIBLE
                clinicTypeWheelPicker()
            }
            3 -> {
                binding.NPTitle.text = "請選擇您的性別"
                binding.NPSubtitle.visibility = View.INVISIBLE
                setRegisteredGender()
            }
        }

        return binding.root
    }

    private fun cityWheelPicker() {
        val np = binding.numberPicker
        val cityName = ArrayList<String?>()

        cityName.add("周邊熱搜")
        if (model.cityList.size != 0) {
            for (city in model.cityList) {
                cityName.add(city.name)
            }
            if (wheelPickerSelectedPosition == 0 && global.latitude > 200 && global.longitude > 200) {
                np.setSelectedItemPosition(1, false)
                wheelPickerSelectedPosition = 1
                cityID = model.cityList[0].city_id
            } else if (wheelPickerSelectedPosition == 0) {
                np.setSelectedItemPosition(0, false)
            } else {
                np.setSelectedItemPosition(wheelPickerSelectedPosition, false)
            }

            np.data = cityName
            np.setOnWheelChangeListener(object : OnWheelChangeListener {
                override fun onWheelScrolled(offset: Int) {
                    if (offset == 0) {
                        binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                    }
                }

                override fun onWheelSelected(position: Int) {
                    binding.btnCheck.setOnClickListener {
                        if (position == 0) {
                            cityID = 0
                            wheelPickerSelectedPosition = position
                        } else {
                            cityID = model.cityList[position - 1].city_id
                            wheelPickerSelectedPosition = position
                        }
                        UpdateTextListenerManager.getInstance().sendBroadCast(true, cityName[position])
                        dialog!!.dismiss()
                    }
                }

                override fun onWheelScrollStateChanged(state: Int) {}
            })
        }
    }

    private fun clinicTypeWheelPicker() {
        val np = binding.numberPicker
        val typeName = ArrayList<String?>()

        typeName.add("全部科別")
        if (model.clinicTypeList.size != 0) {
            for (type in model.clinicTypeList) {
                typeName.add(type.name)
            }
            if (clinicTypeID > 12) {
                np.setSelectedItemPosition(clinicTypeID - 1, false)
            } else {
                np.setSelectedItemPosition(clinicTypeID, false)
            }

            np.data = typeName
            np.setOnWheelChangeListener(object : OnWheelChangeListener {
                override fun onWheelScrolled(offset: Int) {
                    if (offset == 0) {
                        binding.btnCheck.setOnClickListener { dialog!!.dismiss() }
                    }
                }

                override fun onWheelSelected(position: Int) {
                    binding.btnCheck.setOnClickListener {
                        clinicTypeID = if (position == 0) {
                            0
                        } else {
                            model.clinicTypeList[position - 1].type_id
                        }
                        UpdateTextListenerManager.getInstance().sendBroadCast(false, typeName[position])
                        dialog!!.dismiss()
                    }
                }

                override fun onWheelScrollStateChanged(state: Int) {}
            })
        }
    }

    private fun setRegisteredGender() {
        val np = binding.numberPicker
        val genderList = listOf("男性", "女性")

        np.data = genderList
        np.isCyclic = false

        np.setOnWheelChangeListener(object : OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
                if (offset == 0) {
                    binding.btnCheck.setOnClickListener {
                        UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(4, genderList[0])
                        dialog!!.dismiss() }
                }
            }

            override fun onWheelSelected(position: Int) {
                binding.btnCheck.setOnClickListener {
                    UpdateWheelPickerTextListenerManager.getInstance().sendBroadCast(4, genderList[position])
                    dialog!!.dismiss()
                }
            }

            override fun onWheelScrollStateChanged(state: Int) {}
        })
    }
}