package tw.com.gcreate.righttime.adapters.deprecated.customCalendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.global

class CalendarWeekAdapter(private val dataList: MutableList<String>) : RecyclerView.Adapter<CalendarWeekAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_item_day , parent , false)
        v.layoutParams.width = (global.windowWidthPixels * 0.8 / 7 * 0.85).toInt()
        v.layoutParams.height = (global.windowWidthPixels * 0.8 / 7 * 0.85).toInt()
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.date.text = dataList[position]
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        // initial clinicTypeCard
        val date: TextView = v.findViewById(R.id.tv_value)
    }
}