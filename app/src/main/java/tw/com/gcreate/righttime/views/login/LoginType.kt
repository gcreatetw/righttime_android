package tw.com.gcreate.righttime.views.login

enum class LoginType {
    ClinicInfo,
    MemberCentre,
}