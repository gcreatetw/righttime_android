package tw.com.gcreate.righttime.adapters.tab1

import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.RvItemSearchBinding

class SearchAdapter(data: MutableList<String>) : BaseQuickAdapter<String, DataBindBaseViewHolder>(R.layout.rv_item_search, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: String) {
        val binding: RvItemSearchBinding = holder.getViewBinding() as RvItemSearchBinding
        binding.keyword = item
        binding.executePendingBindings()
    }

}