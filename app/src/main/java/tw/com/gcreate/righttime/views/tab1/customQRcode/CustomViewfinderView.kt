package tw.com.gcreate.righttime.views.tab1.customQRcode

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.LinearGradient
import android.graphics.Shader
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.journeyapps.barcodescanner.ViewfinderView
import tw.com.gcreate.righttime.R
import java.util.*

class CustomViewfinderView(context: Context?, attrs: AttributeSet?) : ViewfinderView(context, attrs) {
    private var laserLinePosition = 0
    private var position: FloatArray = floatArrayOf(0f, 0.5f, 1f)
    private var colors = intArrayOf(0x00ffffff, -0x1, 0x00ffffff)
    private var linearGradient: LinearGradient? = null

    /**
     * 重寫draw方法繪製自己的掃描框
     *
     * @param canvas
     */
    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        refreshSizes()
        if (framingRect == null || previewFramingRect == null) {
            return
        }
        val frame = framingRect
        val previewFrame = previewFramingRect
        val width = width
        val height = height
        //繪製4個角
        val color = ContextCompat.getColor(context, R.color.colorWhite)
        paint.color = color //定義畫筆的顏色
        canvas.drawRect(frame.left.toFloat(), frame.top.toFloat(), (frame.left + 100).toFloat(), (frame.top + 30).toFloat(), paint)
        canvas.drawRect(frame.left.toFloat(), frame.top.toFloat(), (frame.left + 30).toFloat(), (frame.top + 100).toFloat(), paint)
        canvas.drawRect((frame.right - 100).toFloat(), frame.top.toFloat(), frame.right.toFloat(), (frame.top + 30).toFloat(), paint)
        canvas.drawRect((frame.right - 30).toFloat(), frame.top.toFloat(), frame.right.toFloat(), (frame.top + 100).toFloat(), paint)
        canvas.drawRect(frame.left.toFloat(), (frame.bottom - 30).toFloat(), (frame.left + 100).toFloat(), frame.bottom.toFloat(), paint)
        canvas.drawRect(frame.left.toFloat(), (frame.bottom - 100).toFloat(), (frame.left + 30).toFloat(), frame.bottom.toFloat(), paint)
        canvas.drawRect((frame.right - 100).toFloat(), (frame.bottom - 30).toFloat(), frame.right.toFloat(), frame.bottom.toFloat(), paint)
        canvas.drawRect((frame.right - 30).toFloat(), (frame.bottom - 100).toFloat(), frame.right.toFloat(), frame.bottom.toFloat(), paint)
        // Draw the exterior (i.e. outside the framing rect) darkened
        paint.color = if (resultBitmap != null) resultColor else maskColor
        canvas.drawRect(0f, 0f, width.toFloat(), frame.top.toFloat(), paint)
        canvas.drawRect(0f, frame.top.toFloat(), frame.left.toFloat(), (frame.bottom + 1).toFloat(), paint)
        canvas.drawRect((frame.right + 1).toFloat(), frame.top.toFloat(), width.toFloat(), (frame.bottom + 1).toFloat(), paint)
        canvas.drawRect(0f, (frame.bottom + 1).toFloat(), width.toFloat(), height.toFloat(), paint)
        if (resultBitmap != null) {
            // Draw the opaque result bitmap over the scanning rectangle
            paint.alpha = CURRENT_POINT_OPACITY
            canvas.drawBitmap(resultBitmap, null, frame, paint)
        } else {
            //  paint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
            //  scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
            val middle = frame.height() / 2 + frame.top
            laserLinePosition += 5
            if (laserLinePosition > frame.height()) {
                laserLinePosition = 0
            }
            linearGradient = LinearGradient((frame.left + 1).toFloat(),
                (frame.top + laserLinePosition).toFloat(),
                (frame.right - 1).toFloat(),
                (frame.top + 10 + laserLinePosition).toFloat(),
                colors,
                position,
                Shader.TileMode.CLAMP)
            // Draw a red "laser scanner" line through the middle to show decoding is active

            //  paint.setColor(laserColor);
            paint.shader = linearGradient
            //繪製掃描線
//            canvas.drawRect(frame.left + 1, frame.top+laserLinePosition , frame.right -1 , frame.top +10+laserLinePosition, paint);
            paint.shader = null
            val scaleX = frame.width() / previewFrame.width().toFloat()
            val scaleY = frame.height() / previewFrame.height().toFloat()
            val currentPossible = possibleResultPoints
            val currentLast = lastPossibleResultPoints
            val frameLeft = frame.left
            val frameTop = frame.top
            if (currentPossible.isEmpty()) {
                lastPossibleResultPoints = null
            } else {
                possibleResultPoints = ArrayList(5)
                lastPossibleResultPoints = currentPossible
                paint.alpha = CURRENT_POINT_OPACITY
                paint.color = resultPointColor
                for (point in currentPossible) {
                    canvas.drawCircle((frameLeft + (point.x * scaleX).toInt()).toFloat(), (
                            frameTop + (point.y * scaleY).toInt()).toFloat(),
                        POINT_SIZE.toFloat(), paint)
                }
            }
            if (currentLast != null) {
                paint.alpha = CURRENT_POINT_OPACITY / 2
                paint.color = resultPointColor
                val radius = POINT_SIZE / 2.0f
                for (point in currentLast) {
                    canvas.drawCircle((frameLeft + (point.x * scaleX).toInt()).toFloat(), (
                            frameTop + (point.y * scaleY).toInt()).toFloat(),
                        radius, paint)
                }
            }
            postInvalidateDelayed(16,
                frame.left,
                frame.top,
                frame.right,
                frame.bottom)
            // postInvalidate();
        }
    }
}