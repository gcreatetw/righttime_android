package tw.com.gcreate.righttime.views

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.config.IndicatorConfig
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.listener.OnPageChangeListener
import com.youth.banner.transformer.DepthPageTransformer
import retrofit2.Response
import tw.com.gcreate.righttime.MainActivity
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.ActivityInitBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.DataBean
import tw.com.gcreate.righttime.util.PdfTool
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiErrorManager
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseAppVersion


class InitActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInitBinding

    companion object {
        private const val GOTO_MAIN_ACTIVITY = 0

        @SuppressLint("StaticFieldLeak")
        lateinit var apiErrorManager: ApiErrorManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_init)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) return@OnCompleteListener
            global.FireBasToken = task.result
        })


        global.context = this
        global.UUID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        global.initloadDB()

        apiErrorManager = ApiErrorManager(applicationContext)

        getWindowsSize()

        // Set  welcome image
        Glide.with(this).load(R.mipmap.welocme).into(binding.imgLoading)

        //check mobile network
        val mConnectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val mNetworkInfo = mConnectivityManager.activeNetworkInfo

        //Add for VersionAPI
        if (mNetworkInfo != null && mNetworkInfo.isConnected) {
            GcreateApiClient.getAppVersion(object : ApiController<ResponseAppVersion>(this@InitActivity, false) {
                override fun onSuccess(response: ResponseAppVersion) {
                    // isOpen == false 跳到維修畫面 , true 正常進 App
                    if (!response.APP_Android.isOpen) {
                        goToAnnounceActivity(response.APP_Android.message)
                    } else {
                        initialView()
                    }
                }

                override fun onFail(httpResponse: Response<ResponseAppVersion>): Boolean {
                    Toast.makeText(this@InitActivity, resources.getText(R.string.text_rest_api_fail), Toast.LENGTH_SHORT).show()
                    return super.onFail(httpResponse)
                }
            })

        } else {
            initialView()
        }
    }

    private fun goToAnnounceActivity(appAnnounceMessage: String) {
        val intent = Intent(this, AnnounceActivity::class.java)
        intent.putExtra("message", appAnnounceMessage)
        startActivity(intent)
        finish()
    }

    /** 起始教學頁面
     * */
    private fun initialView() {
        val banner = binding.initBanner.banner
        banner.layoutParams.height = global.windowHeightPixels

        if (global.getTeachMode()) {
            binding.imgLoading.visibility = View.INVISIBLE
            banner.visibility = View.VISIBLE

            //  Set Default Data
            banner.setAdapter(object : BannerImageAdapter<DataBean>(DataBean.initTeachImages()) {
                override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
//                    val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(applicationContext, data.imageRes)[0])
                    Glide.with(holder.itemView).load(data.imageRes).into(holder.imageView)
                }
            }, false).addBannerLifecycleObserver(this).indicator = CircleIndicator(this)

            // 輪播方式
            banner.setPageTransformer(DepthPageTransformer())
                // Indicator parameters
                .setIndicatorSelectedColor(ContextCompat.getColor(this, R.color.colorYellow))
                .setIndicatorSpace(16)
                .setIndicatorWidth(15, 15)
                .setIndicatorMargins(IndicatorConfig.Margins((global.windowHeightPixels * 0.05).toInt()))
                .isAutoLoop(false)
                .stop()

            banner.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    if (position == DataBean.initTeachImages().size - 1) {
                        binding.buttonFinish.visibility = View.VISIBLE
                        binding.buttonFinish.setOnClickListener {
                            global.setTeachMode(false)
                            val intent = Intent()
                            intent.setClass(this@InitActivity, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                    } else {
                        binding.buttonFinish.visibility = View.INVISIBLE
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        } else {
            binding.imgLoading.visibility = View.VISIBLE
            banner.visibility = View.INVISIBLE
            binding.buttonFinish.visibility = View.INVISIBLE
            mHandler.sendEmptyMessageDelayed(GOTO_MAIN_ACTIVITY, 1000)
        }
    }


    private val mHandler = Handler { msg ->
        when (msg.what) {
            GOTO_MAIN_ACTIVITY -> {
                val intent = Intent()
                intent.setClass(this@InitActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
                true
            }
            else -> {
                true
            }
        }
    }

    private fun getWindowsSize() {
        // Dialog 視窗大小
        val dm = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(dm)
        global.windowWidthPixels = dm.widthPixels
        global.windowHeightPixels = dm.heightPixels
    }
}