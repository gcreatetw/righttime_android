package tw.com.gcreate.righttime.util

import android.content.AsyncQueryHandler
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import android.util.Log
import android.widget.Toast
import tw.com.gcreate.righttime.BuildConfig
import tw.com.gcreate.righttime.model.ModelForGoogleCalendar
import java.util.*

/** Reference article
 *  https://billthefarmer.github.io/blog/adding-a-calendar-event/
 * */

class CalendarAsyncQueryHandler(private val context: Context , cr: ContentResolver?) : AsyncQueryHandler(cr) {

    override fun onQueryComplete(token: Int , cookie: Any? , cursor: Cursor?) {
        super.onQueryComplete(token , cookie , cursor)
        // Use the cursor to move through the returned records
        cursor!!.moveToFirst()

        // Get the field values
        val calendarID = cursor.getLong(CALENDAR_ID_INDEX)
        val values = cookie as ContentValues
        values.put(CalendarContract.Events.CALENDAR_ID , calendarID)
        values.put(CalendarContract.Events.EVENT_TIMEZONE , "Asia/Taipei")
        startInsert(EVENT , null , CalendarContract.Events.CONTENT_URI , values)
    }

    override fun onInsertComplete(token: Int , cookie: Any? , uri: Uri) {
        if (BuildConfig.DEBUG) Log.d(TAG , "Insert complete " + uri.lastPathSegment)

        when (token) {
            EVENT -> {
                val eventID = uri.lastPathSegment!!.toLong()
                val values = ContentValues()
                values.put(CalendarContract.Reminders.MINUTES , 10) // 提前1分钟提醒
                values.put(CalendarContract.Reminders.EVENT_ID , eventID)
                values.put(CalendarContract.Reminders.METHOD , CalendarContract.Reminders.METHOD_ALERT)
                startInsert(REMINDER , null , CalendarContract.Reminders.CONTENT_URI , values)
            }
        }

        Toast.makeText(context,"添加google行程成功",Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val TAG = "Debug"

        // The indices for the projection array above.
        private const val CALENDAR_ID_INDEX = 0
        private const val CALENDAR = 0
        private const val EVENT = 1
        private const val REMINDER = 2
        private var calendarAsyncQueryHandler: CalendarAsyncQueryHandler? = null

        // Projection arrays
        private val CALENDAR_PROJECTION = arrayOf(CalendarContract.Calendars._ID)

        // insertEvent
        fun insertEvent(context: Context , startTime: ModelForGoogleCalendar , endTime: ModelForGoogleCalendar , title: String? , description: String?) {
            val resolver = context.contentResolver

//            if (calendarAsyncQueryHandler == null)
                calendarAsyncQueryHandler = CalendarAsyncQueryHandler(context , resolver)

            //添加日历事件
            val startMillis: Long = Calendar.getInstance().run {
                set(startTime.year , startTime.month - 1 , startTime.date , startTime.hourOfDay , startTime.minute)
                timeInMillis
            }
            val endMillis: Long = Calendar.getInstance().run {
                set(endTime.year , endTime.month - 1 , endTime.date , endTime.hourOfDay , endTime.minute)
                timeInMillis
            }

            val values = ContentValues().apply {
                put(CalendarContract.Events.DTSTART , startMillis)
                put(CalendarContract.Events.DTEND , endMillis)
                put(CalendarContract.Events.TITLE , title)
                put(CalendarContract.Events.DESCRIPTION , description)
                put(CalendarContract.Events.HAS_ALARM , 1) //设置有闹钟提醒
            }

            if (BuildConfig.DEBUG) Log.d(TAG , "Calendar query start")
            calendarAsyncQueryHandler!!.startQuery(CALENDAR , values , CalendarContract.Calendars.CONTENT_URI , CALENDAR_PROJECTION , null , null , null)
        }
    }
}