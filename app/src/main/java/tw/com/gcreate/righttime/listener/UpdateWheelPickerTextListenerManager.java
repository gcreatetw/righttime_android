package tw.com.gcreate.righttime.listener;

import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class UpdateWheelPickerTextListenerManager {
    /**
     * 單例模式
     */
    public static UpdateWheelPickerTextListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private final List<UpdateWheelPickerTextListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static UpdateWheelPickerTextListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new UpdateWheelPickerTextListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(UpdateWheelPickerTextListener iListener) {

        if (iListenerList.size() != 0) {
            iListenerList.removeAll(iListenerList);
        }
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(UpdateWheelPickerTextListener iListener) {
        iListenerList.remove(iListener);
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(int mode,String text) {
        for (UpdateWheelPickerTextListener updateNameListener : iListenerList) {
            updateNameListener.updateWheelPicker(mode,text);

        }
    }


}

