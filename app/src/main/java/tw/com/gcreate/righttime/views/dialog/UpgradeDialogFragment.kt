package tw.com.gcreate.righttime.views.dialog

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import info.hoang8f.android.segmented.SegmentedGroup
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.global
import java.util.*

class UpgradeDialogFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_upgrade, container, false)

        val finishButton = view.findViewById<Button>(R.id.buttonFinish)

        finishButton.setOnClickListener {
            val appPackageName = global.context.packageName
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (e : ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
            dismiss()
        }

        val closeButton = view.findViewById<ImageView>(R.id.imageViewClose)
        closeButton.setOnClickListener { dismiss() }


        /*手機版本比對 google store  上的版本*/
        /*
        VersionChecker versionChecker = new VersionChecker();
        try {
            String latestVersion = versionChecker.execute().get();
            String currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            //Log.d("Version", latestVersion + " : " + currentVersion);
            if (latestVersion != null) {
                if (!currentVersion.substring(0,1).equals(latestVersion.substring(0,1))){
                    //getDialog().setCanceledOnTouchOutside(true);
                    closeButton.setVisibility(View.GONE);
                }
            }
        } catch(ExecutionException e){
            e.printStackTrace();
        } catch(InterruptedException e){
            e.printStackTrace();
        } catch(PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }
*/
        return view
    }

    override fun onStart() {
        super.onStart()
        isCancelable = false
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}