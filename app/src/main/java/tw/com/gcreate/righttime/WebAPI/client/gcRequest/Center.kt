package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class Center(
    val distance: Int,
    val lat: Double,
    val lng: Double
)