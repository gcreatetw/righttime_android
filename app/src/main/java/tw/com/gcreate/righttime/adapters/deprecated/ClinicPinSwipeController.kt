package tw.com.gcreate.righttime.adapters.deprecated

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.view.MotionEvent
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import tw.com.gcreate.righttime.R

class ClinicPinSwipeController(val context: Context, buttonsActions: ClinicPinSwipeControllerActions?) : ItemTouchHelper.Callback() {
    internal enum class ButtonsState {
        GONE,  //LEFT_VISIBLE,
        RIGHT_VISIBLE
    }

    private var swipeBack = false
    private var buttonShowedState = ButtonsState.GONE
    private var buttonInstance1: RectF? = null
    private var buttonInstance2: RectF? = null
    private var currentItemViewHolder: RecyclerView.ViewHolder? = null
    private val buttonsActions: ClinicPinSwipeControllerActions? = null

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}
    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        if (swipeBack) {
            swipeBack = buttonShowedState != ButtonsState.GONE
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        var dX = dX
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (buttonShowedState != ButtonsState.GONE) {
                //if (buttonShowedState == ButtonsState.LEFT_VISIBLE) dX = Math.max(dX, buttonWidth);
                if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) dX = Math.min(dX, -buttonWidth)
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            } else {
                setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
        if (buttonShowedState == ButtonsState.GONE) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
        currentItemViewHolder = viewHolder
    }

    private fun setTouchListener(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        recyclerView.setOnTouchListener { v, event ->
            swipeBack = event.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP
            if (swipeBack) {
                if (dX < -buttonWidth) buttonShowedState = ButtonsState.RIGHT_VISIBLE
                //else if (dX > buttonWidth) buttonShowedState  = ButtonsState.LEFT_VISIBLE;
                if (buttonShowedState != ButtonsState.GONE) {
                    setTouchDownListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    setItemsClickable(recyclerView, false)
                }
            }
            false
        }
    }

    private fun setTouchDownListener(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        recyclerView.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                setTouchUpListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
            false
        }
    }

    private fun setTouchUpListener(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        recyclerView.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                super@ClinicPinSwipeController.onChildDraw(c, recyclerView, viewHolder, 0f, dY, actionState, isCurrentlyActive)
                recyclerView.setOnTouchListener { v, event -> false }
                setItemsClickable(recyclerView, true)
                swipeBack = false
                if (buttonsActions != null && buttonInstance1 != null && buttonInstance1!!.contains(event.x, event.y)) {
                    if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
                        buttonsActions.onRight1Clicked(viewHolder.adapterPosition)
                    }
                    //else if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
                    //    buttonsActions.onLeftClicked(viewHolder.getAdapterPosition());
                    //}
                }
                if (buttonsActions != null && buttonInstance2 != null && buttonInstance2!!.contains(event.x, event.y)) {
                    if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
                        buttonsActions.onRight2Clicked(viewHolder.adapterPosition)
                    }
                    //else if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
                    //    buttonsActions.onLeftClicked(viewHolder.getAdapterPosition());
                    //}
                }
                buttonShowedState = ButtonsState.GONE
                currentItemViewHolder = null
            }
            false
        }
    }

    private fun setItemsClickable(recyclerView: RecyclerView, isClickable: Boolean) {
        for (i in 0 until recyclerView.childCount) {
            recyclerView.getChildAt(i).isClickable = isClickable
        }
    }

    private fun drawButtons(c: Canvas, viewHolder: RecyclerView.ViewHolder) {
        val buttonWidthWithoutPadding = buttonWidth - 20
        val corners = 20f
        val itemView = viewHolder.itemView
        val p = Paint()

//        RectF leftButton = new RectF(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + buttonWidthWithoutPadding, itemView.getBottom());
//        p.setColor(Color.GRAY);
//        c.drawRoundRect(leftButton, corners, corners, p);
//        drawText("EDIT", c, leftButton, p);
        val fpixels = itemView.right
        val pixels = (fpixels + 0.5f).toInt()

//        RectF rightButton1 = new RectF(itemView.getRight() - buttonWidthWithoutPadding, itemView.getTop(), itemView.getRight() - buttonWidthWithoutPadding/2, itemView.getBottom());
//        p.setColor(Color.GRAY);
//        p.setTextSize(14);
//        c.drawRoundRect(rightButton1, corners, corners, p);
//        drawText("EDIT", c, rightButton1, p);

//        RectF rightButton2 = new RectF(itemView.getRight() - buttonWidthWithoutPadding/2, itemView.getTop(), itemView.getRight(), itemView.getBottom());
        val rightButton2 = RectF(itemView.right - buttonWidthWithoutPadding,
            (itemView.top + 40).toFloat(),
            (itemView.right - 40).toFloat(),
            (itemView.bottom - 40).toFloat())
        p.color = Color.RED
        p.textSize = 10f
        c.drawRoundRect(rightButton2, corners, corners, p)
        drawText(context.resources.getString(R.string.btn_delete), c, rightButton2, p)
        buttonInstance1 = null
        buttonInstance2 = null
        if (buttonShowedState == ButtonsState.RIGHT_VISIBLE) {
            //buttonInstance1 = rightButton1;
            buttonInstance2 = rightButton2
        }
        //else if (buttonShowedState == ButtonsState.LEFT_VISIBLE) {
        //    buttonInstance = leftButton;
        //}
    }

    private fun drawText(text: String, c: Canvas, button: RectF, p: Paint) {
        val textSize = 40f
        p.color = Color.WHITE
        p.isAntiAlias = true
        p.textSize = textSize
        val textWidth = p.measureText(text)
        c.drawText(text, button.centerX() - textWidth / 2, button.centerY() + textSize / 2, p)
    }

    fun onDraw(c: Canvas) {
        if (currentItemViewHolder != null) {
            drawButtons(c, currentItemViewHolder!!)
        }
    }

    companion object {
        private const val buttonWidth = 300f
    }

//    init {
//        this.buttonsActions = buttonsActions
//        mContext = context
//    }
}