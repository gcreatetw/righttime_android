package tw.com.gcreate.righttime.adapters.tab5

import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.google.android.material.button.MaterialButton
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.webAPI.client.gcResponse.OhBotObject

class ReservationRecordAdapter(private val clinicName: String, data: MutableList<OhBotObject>) :
    BaseQuickAdapter<OhBotObject, BaseViewHolder>(R.layout.rv_item_reservation_record, data) {

    override fun convert(holder: BaseViewHolder, item: OhBotObject) {
        if(data.isNotEmpty()){

            holder.setText(R.id.tv_clinic_name, clinicName)
            holder.setText(R.id.tv_reservation_doctor, item.AppointmentUnit.name)

            try {
                if (item.AppointmentOrder.userComment.split(',')[0].split('：').size > 1) {
                    holder.setText(R.id.tv_reservation_name, item.AppointmentOrder.userComment.split(',')[0].split('：')[1])
                }
            }catch (e:Exception){
                holder.setText(R.id.tv_reservation_name, item.userName)
            }


            holder.setText(R.id.tv_reservation_date, item.start.substring(0, 10))
            holder.setText(R.id.tv_reservation_time, changeTime(item.start.substring(11, 16)))
            holder.setText(R.id.tv_reservation_status, item.AppointmentOrder.status)

            if (data[holder.layoutPosition].AppointmentOrder.status != "wait") {
                holder.itemView.findViewById<MaterialButton>(R.id.btn_cancel_reservation).visibility = View.GONE
            } else {
                holder.itemView.findViewById<MaterialButton>(R.id.btn_cancel_reservation).visibility = View.VISIBLE
            }

        }

    }

    private fun changeTime(item: String): String {
        val hour = item.substring(0, 2).toInt() + 8
        return String.format("%02d", hour) + item.substring(2, 5)
    }

}