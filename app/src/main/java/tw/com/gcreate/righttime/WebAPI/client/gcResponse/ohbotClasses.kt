package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class Member(
    val id: String,
    val level: Int,
    val isBlock: Boolean,
    val origin: String,
    val originText: String?,
    val createdAt: String,
    val updatedAt: String,
    val UserId: String,
    val OrgId: String,
    val ShopId: String,
    val UserInfoId: String,
    val WalletId: String,
    val UserInfo: UserInfo,
)

data class UserInfo(
    val id: String,
    val name: String,
    val phone: String,
    val birthday: String,
    val email: String?,
    val gender: String,
    val addressCode: Any,
    val address: String?,
    val custom: Custom,
    val origin: String,
    val originId: String,
    val createdAt: String,
    val updatedAt: String,
    val UserId: String,
    val OrgId: String,
    val ImageId: Any,
)

class Custom
class AppointmentReservationServiceAttach

data class AppointmentService(
    val id: String,
    val name: String,
    val description: String,
    val price: Int,
    val showPrice: String,
    val showTime: Int,
    val bookingTime: Int,
    val order: Int,
    val Image: Image,
    val isRemove: Boolean,
    val isPublic: Boolean,
    val createdAt: String,
    val updatedAt: String,
    val ImageId: String?,
    val ShopId: String,
)

data class AppointmentOrder(
    val id: String,
    val code: String,
    var status: String,
    val start: String,
    val end: String,
    val peopleCount: Int,
    val skipPeopleCount: Boolean,
    val userType: String,
    val userName: String,
    val userPhone: String,
    val userComment: String,
    val adminComment: String?,
    val origin: String,
    val originText: String?,
    val tomorrowTimerNotifyId: String?,
    val autoCompletedTimerNotifyId: String?,
    val numberCodeCount: Int,
    val totalPrice: Int,
    val isRemove: Boolean,
    val createdAt: String,
    val updatedAt: String,
    val MemberId: String,
    val ShopId: String,
)

data class AppointmentUnit(
    val id: String,
    val name: String,
    val description: String,
    val bookingMax: Int,
    val order: Int,
    val createdAt: String,
    val isRemove: Boolean,
    val isPublic: Boolean,
    val Image: Image?,
    val updatedAt: String,
    val ImageId: String?,
    val ShopId: String,
)

data class Image(
    val UserId: String,
    val createdAt: String,
    val filename: String,
    val id: String,
    val type: String,
    val updatedAt: String,
)

data class AppointmentServiceAttache(
    val AppointmentReservation_ServiceAttach: AppointmentReservationServiceAttach,
    val ShopId: String,
    val bookingTime: Int,
    val createdAt: String,
    val id: String,
    val isPublic: Boolean,
    val isRemove: Boolean,
    val name: String,
    val order: Int,
    val price: Int,
    val showPrice: String,
    val showTime: Int,
    val updatedAt: String,
)

data class OhbookBooking(
    val id: String,
    val start: String,
    val end: String,
    val volume: Int,
    val name: String,
    val description: String,
    val createdAt: String,
    val updatedAt: String,
    val OhbookProviderId: String,
)

data class Shop(
    val id: String,
    val name: String,
    val phone: String,
    val address: String,
    val isRemove: Boolean,
    val createdAt: String,
    val updatedAt: String,
    val ContractPlanId: String,
    val OrgId: String,
    val DepositWalletId: String,
    val AdminUseWalletId: String,
    val SalesWalletId: String,
    val SalesDepositWalletId: String,
    val AppointmentDepositWalletId: String?,
    val Org: Org,
)

data class Org(
    val id: String,
    val code: String,
    val name: String,
    val isRemove: Boolean,
    val createdAt: String,
    val updatedAt: String,
)

data class ContractPlan(
    val id: String,
    val name: String,
    val shopLimit: Int,
    val memberLimit: Int,
    val useAppointment: Boolean,
    val appointmentUnitLimit: Int,
    val appointmentReservationLimitPerMonth: Int,
    val useAppointmentDeposit: Boolean,
    val useSales: Boolean,
    val useClassTicket: Boolean,
    val useEcommerce: Boolean,
    val useMemberHistory: Boolean,
    val useCoupon: Boolean,
    val useMemberGame: Boolean,
    val createdAt: String,
    val updatedAt: String,
    val ContractId: String,
    val OrgId: String,
)