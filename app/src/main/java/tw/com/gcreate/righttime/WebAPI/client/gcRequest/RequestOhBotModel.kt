package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestOhBotModel(
    val trans_path: String,
    val trans_type: String,
    val trans_header_list: MutableList<RequestQuestItem>?,
    val trans_query_list: MutableList<RequestQuestItem>?,
    val trans_request: Any?,
)

data class RequestQuestItem(
    val item: String,
    val value: String,
)