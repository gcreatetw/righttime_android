package tw.com.gcreate.righttime.views.tab5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab5.ReservationRecordAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentMemberReservationRecordBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.OhBotObject
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel2

/** 掛號紀錄 */
class ReservationRecordFragment : Fragment() {

    private val args: ReservationRecordFragmentArgs by navArgs()
    private lateinit var mAdapter: ReservationRecordAdapter

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentMemberReservationRecordBinding.inflate(inflater, container, false)
        mAdapter = ReservationRecordAdapter(args.reservationClinicName, mutableListOf())

        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, false)
        findUserShopMemberId()

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

            mAdapter.addChildClickViewIds(R.id.btn_sync_google, R.id.btn_cancel_reservation)
            adapter = mAdapter
            mAdapter.setEmptyView(R.layout.layout_no_reservation_record)
            mAdapter.setOnItemChildClickListener { _, view, position ->
                when (view.id) {
                    R.id.btn_sync_google -> {
                        /* Google
                        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                            applyPermission()
                        } else {
                            val startTime = ModelForGoogleCalendar(2021, 7, 20, 10, 50)
                            val endTime = ModelForGoogleCalendar(2021, 7, 20, 11, 0)
                            CalendarAsyncQueryHandler.insertEvent(requireActivity(), startTime, endTime, "GC 好棒棒", "Hello World")
                        }
                        */
                    }
                    R.id.btn_cancel_reservation -> {
                        // 3.3.5 取消預約記錄
                        val requestBody = RequestOhBotModel(
                            "/shop/${args.reservationShopId}/appointmentReservation/${mAdapter.data[position].id}/cancel",
                            "PUT",
                            mutableListOf(RequestQuestItem("orgId", args.reservationOrgId)),
                            mutableListOf(),
                            null
                        )
                        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
                            override fun onSuccess(response: ResponseModel1) {
                                if (response.ohbot_obj != null) {
                                    Toast.makeText(requireActivity(), response.ohbot_obj.msg, Toast.LENGTH_SHORT).show()
                                    mAdapter.data[position].AppointmentOrder.status = "cancel"
                                    itemAnimator = null
                                    mAdapter.notifyItemChanged(position)
                                    view.visibility = View.GONE
                                }
                            }

                            override fun onFail(httpResponse: Response<ResponseModel1>): Boolean {
                                when (httpResponse.code()) {
                                    400 -> {
                                        Toast.makeText(requireActivity(), "此預約已結束", Toast.LENGTH_SHORT).show()
                                    }
                                }
                                return super.onFail(httpResponse)
                            }
                        })
                    }
                }
            }
        }

        return binding.root
    }

    private fun getUserReservationList(shopMemberId: String) {
        // 3.3.3  查詢某使用者的預約記錄清單
        val requestBody = RequestOhBotModel(
            "/shop/${args.reservationShopId}/appointmentReservation",
            "GET",
            mutableListOf(RequestQuestItem("orgId", args.reservationOrgId)),
            mutableListOf(
                RequestQuestItem("MemberId", shopMemberId)
            ),
            null
        )
        GcreateApiClient.getOhBotDataForPost2(requestBody, object : ApiController<ResponseModel2>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel2) {
                if (response.ohbot_obj != null) {
                    val dataList: MutableList<OhBotObject> = response.ohbot_obj
                    mAdapter.data = dataList
                    mAdapter.notifyDataSetChanged()
                }
            }
        })
    }

    // 找User在這診所有沒有註冊
    private fun findUserShopMemberId() {
        // 2.1.1 檢查使⽤者是否已經存在
        val requestBody = RequestOhBotModel(
            "/user/findByOriginId",
            "GET",
            mutableListOf(RequestQuestItem("orgId", args.reservationOrgId)),
            mutableListOf(RequestQuestItem("originId", model.appState.gCMemberId.get()!!)),
            null
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                if (response.ohbot_obj != null) {
                    if (response.ohbot_obj.Members.isNotEmpty()) {
                        // 有註冊過店家。但不一定有在這間店家註冊過
                        for (item in response.ohbot_obj.Members) {
                            if (item.ShopId == args.reservationShopId) {
                                // 表示有找到
                                getUserReservationList(item.id)
                            }
                        }
                    }
                } else {
                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}