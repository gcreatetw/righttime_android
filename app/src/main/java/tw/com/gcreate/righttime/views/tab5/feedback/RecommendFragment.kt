package tw.com.gcreate.righttime.views.tab5.feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentRecommendBinding
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestFeedback
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat


class RecommendFragment : Fragment() {

    private lateinit var binding: FragmentRecommendBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentRecommendBinding.inflate(inflater, container, false)

        binding.apply {
            edRecommendClinicName.setOnEditorActionListener { _, _, _ ->
                binding.edRecommendClinicName.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            edRecommendClinicPhone.setOnEditorActionListener { _, _, _ ->
                binding.edRecommendClinicPhone.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            edRecommendClinicAddress.setOnEditorActionListener { _, _, _ ->
                binding.edRecommendClinicAddress.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            tvCheck.setOnClickListener {
                sendFeedback()
            }

        }

        return binding.root
    }

    private fun sendFeedback() {

        if (!binding.edRecommendClinicName.text.isNullOrEmpty() && !binding.edRecommendClinicName.text.isNullOrEmpty() && !binding.edRecommendClinicName.text.isNullOrEmpty()) {
            val requestBody = RequestFeedback(
                binding.edRecommendClinicName.text.toString(),
                binding.edRecommendClinicAddress.text.toString(),
                "recommend",
                binding.edRecommendClinicPhone.text.toString())

            GcreateApiClient.sendFeedBack(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (response.success) {
                        Toast.makeText(context, resources.getText(R.string.text_feedback_success), Toast.LENGTH_LONG).show()
                        binding.edRecommendClinicName.text.clear()
                        binding.edRecommendClinicAddress.text.clear()
                        binding.edRecommendClinicPhone.text.clear()
                    } else {
                        Toast.makeText(context, resources.getText(R.string.text_feedback_fail), Toast.LENGTH_LONG).show()
                    }
                }
            })
//            if (isPromote) paramObject.addProperty("type", "recommend") else paramObject.addProperty("type", "suggest")
        } else {
            Toast.makeText(context, resources.getText(R.string.text_feedback_none), Toast.LENGTH_LONG).show()
        }
    }


}