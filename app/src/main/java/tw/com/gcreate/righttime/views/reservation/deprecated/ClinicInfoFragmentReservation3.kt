package tw.com.gcreate.righttime.views.reservation.deprecated

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoReservation3Binding
import tw.com.gcreate.righttime.model.ModelForGoogleCalendar
import tw.com.gcreate.righttime.util.CalendarAsyncQueryHandler
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservationWheelPicker

class ClinicInfoFragmentReservation3 : Fragment() {

    private lateinit var binding: FragmentClinicInfoReservation3Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_info_reservation3, container, false)

        binding.btnGoogleCalendar.setOnClickListener {

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                applyPermission()
            } else {
                val startTime = ModelForGoogleCalendar(2021, 7, 20, 10, 50)
                val endTime = ModelForGoogleCalendar(2021, 7, 20, 11, 0)
                CalendarAsyncQueryHandler.insertEvent(requireActivity(), startTime, endTime, "GC 好棒棒", "Hello World")
            }

        }

        binding.btnCheck.setOnClickListener {
            //  跳轉畫面
            ClinicInfoFragmentReservationWheelPicker.serviceItemSelectedPosition = 0
            ClinicInfoFragmentReservationWheelPicker.serviceDoctorSelectedPosition = 0
            ClinicInfoFragmentReservationWheelPicker.serviceAvailableTimeSelectedPosition = 0
            ClinicInfoFragmentReservationWheelPicker.reservationRelativesSelectedPosition = 0

            requireActivity().onBackPressed()
        }

        return binding.root
    }

    /** google calendar 權限*/
    private fun applyPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR), 1)
        }
    }


}