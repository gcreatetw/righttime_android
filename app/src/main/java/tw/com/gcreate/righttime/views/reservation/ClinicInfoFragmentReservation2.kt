package tw.com.gcreate.righttime.views.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoReservation2Binding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.ReservationDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ReservationRecordModel
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotServiceReservationCreate

class ClinicInfoFragmentReservation2 : Fragment() {

    private lateinit var binding: FragmentClinicInfoReservation2Binding
    private val args: ClinicInfoFragmentReservation2Args by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentClinicInfoReservation2Binding.inflate(inflater, container, false)

        binding.apply {
            tvClinicName.text = args.reservationClinicName
            tvReservationDoctor.text = args.reservationDoctorName
            tvReservationDate.text = args.reservationDate
            tvReservationTime.text = changeTime(args.reservationTime)
            tvReservationRelative.text = args.reservationUserName

            btnCheck.setOnClickListener {
                createReservationOrder()
            }

            btnCancel.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        return binding.root
    }

    private fun createReservationOrder() {
        val startTime = args.reservationDate + "T" + args.reservationTime + ":00.000Z"

        // 3.3.2 建⽴預約
        val requestBody = RequestOhBotModel(
            "/shop/${model.selectedClinicInfo.get()!!.shopId!!}/appointmentReservation",
            "POST",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(),
            RequestOhBotServiceReservationCreate(
                args.reservationMemberId, args.reservationDoctorId, args.reservationServiceId,
                mutableListOf(), startTime, args.reservationUserComment)
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                if(response.ohbot_obj != null){
                    Toast.makeText(requireActivity(), "預約建立成功", Toast.LENGTH_SHORT).show()
                    ReservationDataMaintain.addReservationRecord(
                        binding.root.context,
                        ReservationRecordModel(args.reservationClinicName, args.reservationServiceId, args.reservationDate, args.reservationTime))

                    val action = ClinicInfoFragmentReservationDirections.actionClinicInfoFragmentReservationToClinicTypeFragment()
                    ClinicInfoFragmentReservation.navController!!.navigate(action)

                }else{
                    Toast.makeText(requireActivity(), "預約建立失敗", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun changeTime(item: String): String {
        val hour = item.substring(0, 2).toInt() + 8
        return String.format("%02d", hour) + item.substring(2, 5)
    }
}