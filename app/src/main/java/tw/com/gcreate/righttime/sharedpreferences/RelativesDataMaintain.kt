package tw.com.gcreate.righttime.sharedpreferences


import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.gcreate.righttime.webAPI.client.gcRequest.Relative

object RelativesDataMaintain {
    var relativesDataList: MutableList<Relative> = mutableListOf()

    fun getRelativesDataList(context: Context): MutableList<Relative> {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val responseString = sp.getString("RelativesDataList", null)

        val gson = Gson()
        if (!responseString.isNullOrEmpty()) {
            relativesDataList = gson.fromJson(responseString, object : TypeToken<MutableList<Relative>>() {}.type)
        }

        return relativesDataList
    }

    fun addRelativesListItem(context: Context, newData: Relative) {
        relativesDataList.add(newData)
        saveRelativesList(context, relativesDataList!!)

    }

    fun saveRelativesList(context: Context, relativesDataList: MutableList<Relative>) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("RelativesDataList", Gson().toJson(relativesDataList))
        editor.apply()
    }

    fun removeRelativesListItem(context: Context, pos: Int) {
        getRelativesDataList(context)
        relativesDataList.removeAt(pos)
        saveRelativesList(context, relativesDataList)
    }

}