package tw.com.gcreate.righttime.views.login.forgetPassword

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.runBlocking
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentResetPasswordBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestUserInfoModify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat


class ResetPasswordFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentResetPasswordBinding.inflate(inflater, container, false)
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, true)

        binding.apply {
            tieNewPassword2.setOnEditorActionListener { _, _, _ ->
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                binding.tilNewPassword1.clearFocus()
                binding.tilNewPassword2.clearFocus()
                true
            }
        }

        binding.btnCheck.setOnClickListener {

            SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
            binding.tieNewPassword1.clearFocus()
            binding.tieNewPassword2.clearFocus()

            val newPasswordText1 = binding.tieNewPassword1.text!!.trim().toString()
            val newPasswordText2 = binding.tieNewPassword2.text!!.trim().toString()

            if (newPasswordText1.isEmpty() || newPasswordText2.isEmpty()) {
                Toast.makeText(requireActivity(), "欄位請勿空白", Toast.LENGTH_SHORT).show()
            } else if (newPasswordText1 != newPasswordText2) {
                Toast.makeText(requireActivity(), "新密碼不一致", Toast.LENGTH_SHORT).show()
            } else if (newPasswordText1 == newPasswordText2) {
                val requestBody = RequestUserInfoModify(model.appState.gCMemberId.get()!!, newPasswordText2, "", null)
                GcreateApiClient.gcUserInfoModify(
                    requestBody,
                    object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseSimpleFormat) {
                            showAlertDialog()
                        }
                    })
            }
        }

        return binding.root
    }

    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.apply {
            setTitle("")
            setMessage("密碼修改完成。")
            setPositiveButton("確定") { dialog: DialogInterface?, _: Int ->
                runBlocking {
                    model.appState.setGCMemberId("")
                }
                dialog!!.dismiss()
                findNavController().navigateUp()
            }
            setCancelable(false)
            create().show()
        }
    }

}