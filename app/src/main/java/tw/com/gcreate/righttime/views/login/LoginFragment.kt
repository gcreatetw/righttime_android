package tw.com.gcreate.righttime.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.runBlocking
import retrofit2.Response
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentLogintypeLoginBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestGcLogin
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseGcLogin
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotUserCreate

class LoginFragment(private val mode: LoginType) : Fragment() {

    private lateinit var binding: FragmentLogintypeLoginBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLogintypeLoginBinding.inflate(inflater, container, false)

        binding.apply {

            fixTvPhone.text = String.format("%-8s%s：", "手", "機")
            fixTvPassword.text = String.format("%-8s%s：", "密", "碼")

            btnCancel.setOnClickListener {
                findNavController().popBackStack()
            }

            btnCheck.setOnClickListener {
                val requestBody = RequestGcLogin(edLoginPhone.text.toString(), edLoginPassword.text.toString(), global.FireBasToken)
                GcreateApiClient.gcLogin(requestBody, object : ApiController<ResponseGcLogin>(requireActivity(), true) {
                    override fun onSuccess(response: ResponseGcLogin) {
                        if (response.login) {

                            runBlocking {
                                model.appState.setGCMemberId(response.member_id)
                                model.appState.setGCLoginMobile(edLoginPhone.text.toString())
                                model.appState.setGCLoginPassword(edLoginPassword.text.toString())
                                RelativesDataMaintain.saveRelativesList(requireContext(), response.relatives)
                            }

                            if (mode == LoginType.ClinicInfo) {
                                //登入成功，但是有可能該會員沒在該診所(組織)下面註冊過
                                checkUserHasRegisteredShopMember(response)
                            } else {
                                findNavController().popBackStack()
                            }


                        } else {
                            Toast.makeText(requireActivity(), "登入失敗，請檢查帳號密碼", Toast.LENGTH_LONG).show()
                        }
                    }
                })
            }

            tvLoginForgetPassword.setOnClickListener {
                val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToForgetCheckFragment()
                findNavController().navigate(action)
            }

        }

        return binding.root
    }

    // 找User在這診所有沒有註冊
    private fun checkUserHasRegisteredShopMember(response: ResponseGcLogin) {
        // 2.1.1 檢查使⽤者是否已經存在
        val requestBody = RequestOhBotModel(
            "/user/findByOriginId",
            "GET",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(RequestQuestItem("originId", model.appState.gCMemberId.get()!!)),
            null
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToClinicInfoFragmentReservation()
                findNavController().navigate(action)
            }

            override fun onFail(httpResponse: Response<ResponseModel1>): Boolean {
                // 404 not found
                createOhBotMember(response)
                return super.onFail(httpResponse)
            }
        })
    }

    private fun createOhBotMember(response: ResponseGcLogin) {
        // 2.1.2 建⽴⽤⼾使⽤者
        val inputMobile = response.cell_phone
        val inputName = response.name
        val inputGender = if (response.sex == "M") "male" else "female"
        val inputBirthday = response.birth

        val requestBody = RequestOhBotModel(
            "/user/createClientUser",
            "POST",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(),
            RequestOhBotUserCreate("right_time", response.member_id, inputMobile, inputName, inputBirthday, inputGender)
        )

        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                // 註冊流程結束。
                if (mode == LoginType.ClinicInfo) {
                    // 進到診所資訊
                    val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToClinicInfoFragmentReservation()
                    findNavController().navigate(action)
                } else {
                    // 回會員資訊
                    val action = LoginTypeFragmentDirections.actionLoginTypeFragmentToMemberCentreFragment2()
                    findNavController().navigate(action)
                }
            }
        })
    }

}