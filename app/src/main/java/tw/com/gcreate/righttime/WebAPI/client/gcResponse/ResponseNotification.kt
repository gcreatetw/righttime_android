package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseNotification(
    val message: String?,
    val notify_list: MutableList<Notify>,
    val success: Boolean
)

data class Notify(
    val created_at: String,
    val message: String,
    val title: String
)