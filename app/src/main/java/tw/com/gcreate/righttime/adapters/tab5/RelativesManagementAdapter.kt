package tw.com.gcreate.righttime.adapters.tab5

import android.annotation.SuppressLint
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.RvItemReletiveBinding
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.Relative
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestUserInfoModify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat

class RelativesManagementAdapter(val gcMemberId:String,data: MutableList<Relative>) :
    BaseQuickAdapter<Relative, DataBindBaseViewHolder>(R.layout.rv_item_reletive, data) {

    private var deletedRelatives: Relative? = null
    private var deletedRelativesPosition = 0

    override fun convert(holder: DataBindBaseViewHolder, item: Relative) {
        val binding: RvItemReletiveBinding = holder.getViewBinding() as RvItemReletiveBinding
        binding.executePendingBindings()
        binding.data = item
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun refresh(clinicNameCards: MutableList<Relative>) {
        clinicNameCards.removeAt(0)
        data = clinicNameCards
        notifyDataSetChanged()

        val requestBody = RequestUserInfoModify(
            gcMemberId,
            null,
            "",
            RelativesDataMaintain.getRelativesDataList(context))

        GcreateApiClient.gcUserInfoModify(requestBody, object : ApiController<ResponseSimpleFormat>(context, false) {
            override fun onSuccess(response: ResponseSimpleFormat) {

            }
        })
    }

    fun deleteItem(position: Int) {
//        if (position != 0) {
//            deletedRelatives = data[position]
//            deletedRelativesPosition = position
//            notifyItemRemoved(position)
//            data.removeAt(position)
//            RelativesDataMaintain.removeRelativesListItem(context, position)
//        } else {
//            Toast.makeText(context, "無法移除自己", Toast.LENGTH_SHORT).show()
//        }
        deletedRelatives = data[position]
        deletedRelativesPosition = position
        notifyItemRemoved(position)
        data.removeAt(position)
        RelativesDataMaintain.removeRelativesListItem(context, position + 1)

        refresh(RelativesDataMaintain.getRelativesDataList(context))
    }

}