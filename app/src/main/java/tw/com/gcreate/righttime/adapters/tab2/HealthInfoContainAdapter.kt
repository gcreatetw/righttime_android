package tw.com.gcreate.righttime.adapters.tab2

import android.content.Intent
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.views.webView.WebViewActivity
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseHeHoArticleItem

class HealthInfoContainAdapter(data: MutableList<ResponseHeHoArticleItem>, layoutResId: Int) :
    BaseQuickAdapter<ResponseHeHoArticleItem, DataBindBaseViewHolder>(layoutResId, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: ResponseHeHoArticleItem) {

        val imgView = holder.itemView.rootView.findViewById<ImageView>(R.id.img_healthInfoContain)
        Glide.with(context).load(item.fetured_image).into(imgView)

        holder.setText(R.id.tv_healthInfoContain_date, item.post_date.substring(0, 10))
        holder.setText(R.id.tv_healthInfoContain, item.post_title)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("articleUrl", item.guid)
            startActivity(context,intent,null)
        }

    }
}