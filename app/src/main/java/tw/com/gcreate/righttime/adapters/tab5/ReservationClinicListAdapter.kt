package tw.com.gcreate.righttime.adapters.tab5

import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.ClinicNameCardBinding
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Shop

class ReservationClinicListAdapter(data: MutableList<Shop>) :
    BaseQuickAdapter<Shop, DataBindBaseViewHolder>(R.layout.clinic_name_card, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Shop) {
        val binding: ClinicNameCardBinding = holder.getViewBinding() as ClinicNameCardBinding
        binding.executePendingBindings()

        holder.setText(R.id.textClinicName, item.name)
        holder.setText(R.id.textClinicAddr, String.format(holder.itemView.context.resources.getString(R.string.text_address), item.address))
        holder.setText(R.id.textClinicPhone, String.format(holder.itemView.context.resources.getString(R.string.text_phone), item.phone))

        holder.itemView.findViewById<TextView>(R.id.leave).visibility = View.GONE
    }
}
