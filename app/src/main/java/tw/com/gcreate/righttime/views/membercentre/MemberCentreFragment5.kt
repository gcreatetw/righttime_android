package tw.com.gcreate.righttime.views.membercentre

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentMemberCentre5Binding
import tw.com.gcreate.righttime.util.SomeFunc

/**親友管理 -編輯親友* */
class MemberCentreFragment5 : Fragment(), TextView.OnEditorActionListener {

    private lateinit var binding: FragmentMemberCentre5Binding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_member_centre5, container, false)

        setView()


        return binding.root
    }


    private fun setView() {
        //  Toolbar
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbarMember5.materialToolbar, "列表", binding.toolbarMember5.toolbarLogo, true)

        binding.tvMemberName.setOnEditorActionListener(this)


        binding.btnCheck.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        // 關閉軟鍵盤
        SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
        binding.tvMemberName.clearFocus()
        return true
    }


}