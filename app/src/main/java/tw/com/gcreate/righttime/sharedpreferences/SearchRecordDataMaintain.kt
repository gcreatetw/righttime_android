package tw.com.gcreate.righttime.sharedpreferences


import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object SearchRecordDataMaintain {
    var searchRecordDataList: MutableList<String> = mutableListOf()

    fun getSearchRecordDataList(context: Context): MutableList<String> {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val responseString = sp.getString("SearchRecordDataList", null)

        val gson = Gson()
        if (!responseString.isNullOrEmpty()) {
            searchRecordDataList = gson.fromJson(responseString, object : TypeToken<MutableList<String>>() {}.type)
        }

        return searchRecordDataList
    }

    fun addSearchRecordItem(context: Context, newData: String) {
        searchRecordDataList!!.add(newData)
        saveSearchRecordDataList(context, searchRecordDataList!!)

    }

    fun saveSearchRecordDataList(context: Context, searchRecordDataList: MutableList<String>) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("SearchRecordDataList", Gson().toJson(SearchRecordDataMaintain.searchRecordDataList))
        editor.apply()
    }

    fun removeSearchRecordItem(context: Context, pos: Int) {
        searchRecordDataList.removeAt(pos)
        saveSearchRecordDataList(context, searchRecordDataList)
    }

}