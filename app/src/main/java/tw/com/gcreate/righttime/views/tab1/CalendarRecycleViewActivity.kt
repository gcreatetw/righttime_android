package tw.com.gcreate.righttime.views.tab1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import tw.com.gcreate.righttime.R
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import tw.com.gcreate.righttime.util.indicator.CalendarIndicator
import androidx.recyclerview.widget.PagerSnapHelper
import tw.com.gcreate.righttime.adapters.tab1.CalendarAdapter

class CalendarRecycleViewActivity : AppCompatActivity() {
    private var button: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calendar_recycle_view)

        button = findViewById<View>(R.id.imgbtn_calendar) as ImageView
        button!!.setOnClickListener { finish() }

        val getScheduleUrl = intent.getStringArrayListExtra("ScheduleUrl")
        val recyclerview = findViewById<View>(R.id.recyclerView_calendar) as RecyclerView
        val adapter = CalendarAdapter(getScheduleUrl!!)

        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerview.addItemDecoration(CalendarIndicator())

        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(recyclerview)
    }
}