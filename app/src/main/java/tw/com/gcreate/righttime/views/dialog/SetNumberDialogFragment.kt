package tw.com.gcreate.righttime.views.dialog


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.DialogSetClinicNumberBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.TodayNotifyCardDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.views.tab1.ClinicInfoFragmentDirections
import tw.com.gcreate.righttime.views.tab1.ClinicTypeFragmentDirections
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestClinicList
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestDeviceNotify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseDeviceNotify
import java.util.*

class SetNumberDialogFragment(private val isPrivView: Boolean, private val navController: NavController) : DialogFragment() {

    private lateinit var binding: DialogSetClinicNumberBinding

    private var currentTime = ""
    private var startTime = ""
    private var bookingDate = ""
    private var currentNum = 0
    private var bookingNum = 0
    private var notifyNum = 0

    //    private var date: Date? = null
//    private var today: Date? = null
    private var datePickerSelectedDate: String = ""
//    private var dateFormat = SimpleDateFormat("yyyyMMdd", Locale.TAIWAN)
//    private var dateFormatAPI = SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN)

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogSetClinicNumberBinding.inflate(inflater, container, false)

        initView()
        setSegmentedTime()
        checkComeFormClinicInfo()

        return binding.root
    }

    private fun initView() {
        setDatePicker()

        binding.apply {
            editTextBookingNum.setOnEditorActionListener { _, _, _ ->
                hideSoftKeyBoard()
                true
            }

            editTextNotifyNum.setOnEditorActionListener { _, _, _ ->
                hideSoftKeyBoard()
                true
            }

            goBackClinicInfo.setOnClickListener {
                // 從主頁回診所
                try {
                    val requestBody = RequestClinicList("", "", arrayListOf(model.tempNotifyCardInfo.get()!!.clinicId), null)
                    GcreateApiClient.getClinicList(requestBody, object : ApiController<ResponseClinicList>(requireContext(), false) {
                        @SuppressLint("NotifyDataSetChanged")
                        override fun onSuccess(response: ResponseClinicList) {
                            model.selectedClinicSimple.set(response[0])
                            val action = ClinicTypeFragmentDirections.actionClinicTypeFragmentToClinicInfoFragment()
                            action.clinicId = model.tempNotifyCardInfo.get()!!.clinicId
                            action.isComeFormQR = false
                            findNavController().navigate(action)
                            dialog!!.dismiss()
                        }

                        override fun onFail(httpResponse: Response<ResponseClinicList>): Boolean {
                            Toast.makeText(context, "資料更新失敗，請檢查網路狀態", Toast.LENGTH_LONG).show()
                            return super.onFail(httpResponse)
                        }
                    })

                } catch (e: Exception) {

                }
            }

            segmentedTime.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.radioButtonM -> {
                        startTime = "M"
                        return@OnCheckedChangeListener
                    }

                    R.id.radioButtonA -> {
                        startTime = "A"
                        return@OnCheckedChangeListener
                    }

                    R.id.radioButtonE -> {
                        startTime = "E"
                        return@OnCheckedChangeListener
                    }
                }
            })

            buttonFinish.setOnClickListener(View.OnClickListener {
                var tmpCurrentNum = 0
                if (datePickerSelectedDate.compareTo(global.date) == 0) {
                    tmpCurrentNum = currentNum
                }

                bookingNum = if (binding.editTextBookingNum.text.toString().isEmpty()) 0
                else binding.editTextBookingNum.text.toString().toInt()

                notifyNum = if (binding.editTextNotifyNum.text.trim().toString().isEmpty()) 0
                else binding.editTextNotifyNum.text.trim().toString().toInt()

                if (startTime.isEmpty()) {
                    Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail1), Toast.LENGTH_LONG).show()
                    return@OnClickListener
                }
                if (bookingNum == 0) {
                    Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail2), Toast.LENGTH_LONG).show()
                    return@OnClickListener
                } else if (notifyNum == 0 || notifyNum > 10) {
                    Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail3), Toast.LENGTH_LONG).show()
                    return@OnClickListener
                }

                if (startTime == currentTime) {
                    when {
                        bookingNum <= tmpCurrentNum -> {
                            Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail4), Toast.LENGTH_LONG).show()
                            return@OnClickListener
                        }
                        bookingNum - notifyNum <= tmpCurrentNum -> {
                            Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail5), Toast.LENGTH_LONG).show()
                            return@OnClickListener
                        }
                        else -> {
                            sendNotifyToServer()
                        }
                    }
                } else {
                    if (bookingNum - notifyNum <= 0) {
                        Toast.makeText(context, resources.getText(R.string.text_notify_setting_fail5), Toast.LENGTH_LONG).show()
                        return@OnClickListener
                    } else {
                        sendNotifyToServer()
                    }
                }
            })
        }

    }

    private fun checkComeFormClinicInfo() {
        if (global.isComeFromClinicInfo) {
            binding.goBackClinicInfo.visibility = View.INVISIBLE
            global.isComeFromClinicInfo = false
        } else {
            binding.goBackClinicInfo.visibility = View.VISIBLE
        }
    }

    private fun returnClinicTypeFragment() {
        val action = ClinicInfoFragmentDirections.actionClinicInfoFragmentToClinicTypeFragment()
        findNavController().navigate(action)
    }

    private fun setDatePicker() {
        val today = Calendar.getInstance(Locale.TAIWAN)

        binding.datePicker.apply {
            minDate = System.currentTimeMillis()
            init(today[Calendar.YEAR], today[Calendar.MONTH], today[Calendar.DAY_OF_MONTH])
            { _, year, monthOfYear, dayOfMonth ->
                datePickerSelectedDate = year.toString() + String.format("%02d", monthOfYear + 1) + String.format("%02d", dayOfMonth)
                if (datePickerSelectedDate == global.date) {
                    binding.textcurrentNumber.text = model.tempNotifyCardInfo.get()!!.currentNum.toString()
                    if (currentTime == "A") {
                        binding.radioButtonM.isEnabled = false
                    } else if (currentTime == "E") {
                        binding.radioButtonM.isEnabled = false
                        binding.radioButtonA.isEnabled = false
                    }
                } else {
                    binding.textcurrentNumber.text = "回診提醒"
                    binding.radioButtonE.isEnabled = true
                    binding.radioButtonM.isEnabled = true
                    binding.radioButtonA.isEnabled = true
                }
            }
        }
    }

    private fun setSegmentedTime() {
        val tempClinicInfo = model.tempNotifyCardInfo.get()!!
        currentTime = getCurrentTime(tempClinicInfo)
        startTime = tempClinicInfo.bookingTime
        currentNum = tempClinicInfo.currentNum
        bookingNum = tempClinicInfo.bookingNum
        notifyNum = tempClinicInfo.notifyNum
        bookingDate = tempClinicInfo.date // 取得今天日期
        datePickerSelectedDate = tempClinicInfo.date


//        binding.datePicker.minDate = binding.datePicker.drawingTime
//        if (bookingDate.length == 8) {
//            binding.datePicker.updateDate(bookingDate.substring(0, 4).toInt(),
//                bookingDate.substring(4, 6).toInt() - 1,
//                bookingDate.substring(6, 8).toInt())
//        }
        binding.datePicker.apply {
            // 如果從首頁已經預約的通知近來。 時間要變動
            updateDate(bookingDate.substring(0, 4).toInt(), bookingDate.substring(4, 6).toInt() - 1, bookingDate.substring(6, 8).toInt())
            if (datePickerSelectedDate == global.date) {
                binding.textcurrentNumber.text = model.tempNotifyCardInfo.get()!!.currentNum.toString()
                if (currentTime == "A") {
                    binding.radioButtonM.isEnabled = false
                } else if (currentTime == "E") {
                    binding.radioButtonM.isEnabled = false
                    binding.radioButtonA.isEnabled = false
                }
            } else {
                binding.textcurrentNumber.text = "回診提醒"
                binding.radioButtonE.isEnabled = true
                binding.radioButtonM.isEnabled = true
                binding.radioButtonA.isEnabled = true
            }
        }

//        if (dateFormat.format(date!!) != global.date) {
//            binding.textcurrentNumber.text = "回診"
//        } else {
//            binding.textcurrentNumber.text = currentNum.toString()
//        }
        binding.textcurrentNumber.text = currentNum.toString()

        if (bookingNum > 0) binding.editTextBookingNum.setText(bookingNum.toString())
        if (notifyNum > 0) binding.editTextNotifyNum.setText(notifyNum.toString())

        //Time radio button settings
        if (currentTime == "A") {
            binding.radioButtonM.isEnabled = false
        } else if (currentTime == "E") {
            binding.radioButtonM.isEnabled = false
            binding.radioButtonA.isEnabled = false
        }
        when (startTime) {
            "E" -> {
                binding.segmentedTime.check(R.id.radioButtonE)
            }
            "A" -> {
                binding.segmentedTime.check(R.id.radioButtonA)
            }
            "M" -> {
                binding.segmentedTime.check(R.id.radioButtonM)
            }
            else -> {
                when (currentTime) {
                    "E" -> {
                        startTime = "E"
                        binding.segmentedTime.check(R.id.radioButtonE)
                    }
                    "A" -> {
                        startTime = "A"
                        binding.segmentedTime.check(R.id.radioButtonA)
                    }
                    else -> {
                        startTime = "M"
                        binding.segmentedTime.check(R.id.radioButtonM)
                    }
                }
            }
        }
    }

    private fun getCurrentTime(notifyCard: ObjectNotifyCard2): String {
        global.updateTime()
        val rightNow = Calendar.getInstance()
        val crHr = rightNow[Calendar.HOUR_OF_DAY] * 100 + rightNow[Calendar.MINUTE]
        val morningEnd = notifyCard.afternoonS.split(":".toRegex()).toTypedArray()
        val afternoonEnd = notifyCard.eveningS.split(":".toRegex()).toTypedArray()
        val morningE = morningEnd[0].toInt() * 100 + morningEnd[1].toInt()
        val afternoonE = afternoonEnd[0].toInt() * 100 + morningEnd[1].toInt()
        Log.d("CurrentTime", "$crHr $morningE $afternoonE")
        return when {
            crHr in 0..morningE -> {
                "M"
            }
            crHr <= afternoonE -> {
                "A"
            }
            else -> {
                "E"
            }
        }
    }

    private fun sendNotifyToServer() {
        val tempClinicInfo = model.tempNotifyCardInfo.get()!!
        tempClinicInfo.apply {
            bookingTime = startTime
            bookingNum = this@SetNumberDialogFragment.bookingNum
            notifyNum = this@SetNumberDialogFragment.notifyNum
            date = datePickerSelectedDate
        }

        val inputDateString =
            datePickerSelectedDate.substring(0, 4) + "-" + datePickerSelectedDate.substring(4, 6) + "-" + datePickerSelectedDate.substring(6, 8)

        val requestBody = RequestDeviceNotify(tempClinicInfo.roomId,
            global.FireBasToken,
            tempClinicInfo.bookingNum.toString(),
            (tempClinicInfo.bookingNum - tempClinicInfo.notifyNum).toString(),
            tempClinicInfo.bookingTime, inputDateString)

        GcreateApiClient.setDeviceNotify(global.UUID, requestBody, object : ApiController<ResponseDeviceNotify>(requireActivity(), false) {
            override fun onSuccess(response: ResponseDeviceNotify) {
                if (response.success) {
                    TodayNotifyCardDataMaintain.addToNotifyList(requireContext(), tempClinicInfo)
                    val tmpNotifyInfo = model.tempNotifyCardInfo.get()!!
                    for (item in TodayNotifyCardDataMaintain.getNotifyCardList(requireContext())) {
                        if (tmpNotifyInfo.roomId == item.roomId && tmpNotifyInfo.date.compareTo(item.date) == 0) {
                            item.bookingNum = tmpNotifyInfo.bookingNum
                            item.notifyNum = tmpNotifyInfo.notifyNum
                        }
                    }
                    Toast.makeText(context, resources.getText(R.string.text_notify_success), Toast.LENGTH_LONG).show()

                    if (!isPrivView) returnClinicTypeFragment()


                    hideSoftKeyBoard()
                    dismiss()
                } else {
                    Toast.makeText(context, resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                }
            }

            override fun onFail(httpResponse: Response<ResponseDeviceNotify>): Boolean {
                Toast.makeText(context, resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                dismiss()
                return super.onFail(httpResponse)
            }
        })

    }

    private fun hideSoftKeyBoard() {
        binding.apply {
            editTextBookingNum.clearFocus()
            editTextNotifyNum.clearFocus()
        }
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)

    }

}