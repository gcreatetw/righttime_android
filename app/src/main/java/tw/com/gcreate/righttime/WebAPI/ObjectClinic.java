package tw.com.gcreate.righttime.webAPI;

public class ObjectClinic implements Comparable<ObjectClinic> {

    @Override
    public int compareTo(ObjectClinic o) {
        if (this.isOpen() == false && o.isOpen() == true)
//        if (this.center_len > o.center_len)   < 取消遮罩排序>
        {
            return 1;
//        } else if (this.center_len > o.center_len) {
//            return 1;
        }
//    else if (this.center_len < o.center_len ) < 取消遮罩排序>
        else if (this.center_len < o.center_len || this.isOpen() == true && o.isOpen() == false) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * id : 1
     * name : 昇安診所
     * pic : http://demoweb.gcreate.com.tw/righttime-pic/005.png
     * address : 新竹市東區龍山東路1號 (興世代1樓)
     * contact : 03-6662783
     * extension :
     * lat : 24.796667
     * lng : 120.96825
     * center_len : 209
     * open : false
     * updated_time : 2019-07-12T14:32:06+08:00
     */

    private int id;
    private String name;
    private String pic;
    private String address;
    private String contact;
    private String extension;
    private double lat;
    private double lng;
    public double center_len;
    private boolean open;
    private String updated_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getCenter_len() {
        return center_len;
    }

    public void setCenter_len(int center_len) {
        this.center_len = center_len;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }


}

