package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ResponseHeHoCategory : ArrayList<ResponseHeHoCategoryItem>()

data class ResponseHeHoCategoryItem(
    val name: String,
    val type_id: Int
)