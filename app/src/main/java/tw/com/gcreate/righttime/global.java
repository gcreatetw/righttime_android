package tw.com.gcreate.righttime;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import tw.com.gcreate.righttime.util.LocationFinder;
import tw.com.gcreate.righttime.webAPI.ObjectClinic;

public class global extends Application {
    //----   App init use ----
    public static Context context;

    public static String UUID;
    public static String FireBasToken = "";
    public static String date = "";

    public static Double latitude = 1000.0;
    public static Double longitude = 1000.0;

    public static ObjectClinic tmpClinic;

    public static int windowWidthPixels, windowHeightPixels;
    public static boolean isCheckVersion = true;

    public static boolean isComeFromClinicInfo;


    public static <T> void printToLogcat(String prompt, T output) {
        String tmp = String.valueOf(output);
        Log.d("Puwu", prompt + ":" + tmp);
    }

    public static void initloadDB() {
        updateTime();
    }

    public static boolean getTeachMode() {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getBoolean("TeachMode", true);
    }

    public static void setTeachMode(boolean mode) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("TeachMode", mode);
        editor.commit();
    }

    private static final double EARTH_RADIUS = 6378137.0;

    public static double getDistance2(double lat_a, double lng_a, double lat_b, double lng_b) {
        double radLat1 = (lat_a * Math.PI / 180.0);
        double radLat2 = (lat_b * Math.PI / 180.0);
        double a = radLat1 - radLat2;
        double b = (lng_a - lng_b) * Math.PI / 180.0;
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) *
                Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = (double) Math.round(s * 1000) / 100;
        return s;
    }

    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        return getDistance(lat1, lon1, lat2, lon2, 0, 0);
    }

    public static double getDistance(double lat1, double lon1, double lat2, double lon2, double el1, double el2) {
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to Kmeters

        double height = el1 - el2;
        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        return (Math.sqrt(distance)) / 1000;
    }

    public static double getDistance(double lat, double lng) {
        Double f = getDistance(latitude, longitude, lat, lng);
        BigDecimal b = new BigDecimal(f);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void getCurrentPosition(Context mContext) {
        LocationFinder finder;
        finder = new LocationFinder(mContext);
        if (finder.canGetLocation()) {
            latitude = finder.getLatitude();
            longitude = finder.getLongitude();
            printToLogcat("lat", latitude);
            printToLogcat("long", longitude);
        } else {
            finder.showSettingsAlert();
        }

    }

    public static String getCurrentTime() {
        updateTime();
        Calendar rightNow = Calendar.getInstance();
        int crHr = rightNow.get(Calendar.HOUR_OF_DAY);
        if (crHr >= 0 && crHr <= 13) {
            return "M";
        } else if (crHr > 13 && crHr <= 19) {
            return "A";
        } else {
            return "E";
        }
    }

    public static void updateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date dt = new Date();
        date = sdf.format(dt);
    }

    private static String dateFormat(String date, String format) {
        return date.substring(0, 4) + format + date.substring(4, 6) + format + date.substring(6, 8);
    }

    public static String dateToApiFormat(String date) {
        return dateFormat(date, "-");
    }


}
