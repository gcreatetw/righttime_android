package tw.com.gcreate.righttime.views.reservation.deprecated

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoVaccine2Binding
import tw.com.gcreate.righttime.model.ModelForGoogleCalendar
import tw.com.gcreate.righttime.util.CalendarAsyncQueryHandler
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservation


class ClinicInfoFragmentVaccine2 : Fragment() {

    private lateinit var binding: FragmentClinicInfoVaccine2Binding


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_clinic_info_vaccine2 , container , false)
        ClinicInfoFragmentReservation.vaccineCurrentPage = "ClinicInfoFragmentVaccine2"

        binding.btnGoogleCalendar2.setOnClickListener {

            if (ContextCompat.checkSelfPermission(requireContext() , Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                applyPermission()
            } else {
                val startTime = ModelForGoogleCalendar(2021,7,22,15,26)
                val endTime = ModelForGoogleCalendar(2021,7,22,15,30)
                CalendarAsyncQueryHandler.insertEvent(requireActivity(), startTime,endTime, "GC 好棒棒" , "Hello World")
            }

        }

        binding.btnCheck.setOnClickListener {
            //  跳轉畫面
            ClinicInfoFragmentReservation.vaccineCurrentPage = "ClinicInfoFragmentVaccine"
            requireActivity().onBackPressed()
        }


        return binding.root
    }

    /** google calendar 權限*/
    private fun applyPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(arrayOf(Manifest.permission.READ_CALENDAR , Manifest.permission.WRITE_CALENDAR) , 1)
        }
    }

}