package tw.com.gcreate.righttime.adapters.tab1.clinicDetail

import android.graphics.Color
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.DoctorHeadshotBinding

class ClinicInfoExtraLinkAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, DataBindBaseViewHolder>(R.layout.doctor_headshot, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: String) {
        val binding: DoctorHeadshotBinding = holder.getViewBinding() as DoctorHeadshotBinding
        binding.executePendingBindings()

        when (item) {
            "Line@" -> {
                binding.doctorHeadshotImg.borderColor = Color.parseColor("#00b900")
                binding.doctorHeadshotName.setTextColor(Color.parseColor("#00b900"))
                Glide.with(holder.itemView).load(R.drawable.icon_line).error(R.drawable.icon_doctor).into(binding.doctorHeadshotImg)
            }
            "OfficialWebsite" -> {
                binding.doctorHeadshotImg.borderColor = Color.parseColor("#FF1977F3")
                binding.doctorHeadshotName.setTextColor(Color.parseColor("#FF1977F3"))
                Glide.with(holder.itemView).load(R.mipmap.doctor_web).error(R.drawable.icon_doctor).into(binding.doctorHeadshotImg)
            }
            "FaceBook" -> {
                binding.doctorHeadshotImg.borderColor = Color.parseColor("#FF1977F3")
                binding.doctorHeadshotName.setTextColor(Color.parseColor("#FF1977F3"))
                Glide.with(holder.itemView).load(R.drawable.icon_facebook).error(R.drawable.icon_doctor).into(binding.doctorHeadshotImg)
            }
            "latestNews" -> {
                binding.doctorHeadshotImg.borderColor = Color.parseColor("#ffc134")
                binding.doctorHeadshotName.setTextColor(Color.parseColor("#ffc134"))
                Glide.with(holder.itemView).load(R.drawable.icon_news).error(R.drawable.icon_doctor).into(binding.doctorHeadshotImg)
            }
        }

        holder.setText(R.id.doctorHeadshotName, item)

    }
}