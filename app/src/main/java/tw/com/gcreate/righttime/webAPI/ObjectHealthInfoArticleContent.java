package tw.com.gcreate.righttime.webAPI;

public class ObjectHealthInfoArticleContent {

    /**
     * rowid : 5d4ad953-f1ed-11ea-ac2a-f23c917293d4
     * category : car
     * type_id : 1
     * post_date : 2020-09-08
     * post_title : 乘勝追擊追求再成長，Kia Picanto / Stonic愛線特仕版報到
     * guid : https://www.carexpert.com.tw/?p=39523
     * fetured_image : https://www.carexpert.com.tw/wp-content/uploads/2020/09/S__13328522.jpg
     */

    private String rowid;
    private String category;
    private int type_id;
    private String post_date;
    private String post_title;
    private String guid;
    private String fetured_image;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getFetured_image() {
        return fetured_image;
    }

    public void setFetured_image(String fetured_image) {
        this.fetured_image = fetured_image;
    }
}
