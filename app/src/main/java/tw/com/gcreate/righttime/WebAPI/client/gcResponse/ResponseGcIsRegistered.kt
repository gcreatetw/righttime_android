package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseGcIsRegistered(
    val success: Boolean,
    val member_id: String?,
    val message: String?,
)