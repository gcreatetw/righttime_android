package tw.com.gcreate.righttime.adapters.tab1.clinicDetail

import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Doctor

class DoctorIntroAdapter(data: MutableList<Doctor>) : BaseQuickAdapter<Doctor, DataBindBaseViewHolder>(R.layout.doctor_intro, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Doctor) {

        if (item.education.isNotEmpty()) {
            val stringBuffer = StringBuffer()
            for (text in item.education) {
                stringBuffer.append(text + '\n')
            }
            holder.setText(R.id.doctorIntro_education, stringBuffer)
        }

        if (item.experience.isNotEmpty()) {
            val stringBuffer = StringBuffer()
            for (text in item.experience) {
                stringBuffer.append(text + '\n')
            }
            holder.setText(R.id.doctorIntro_experience, stringBuffer)
        }

    }

}