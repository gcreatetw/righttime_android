package tw.com.gcreate.righttime.adapters.tab1

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import tw.com.gcreate.righttime.views.login.LoginFragment
import tw.com.gcreate.righttime.views.login.LoginType
import tw.com.gcreate.righttime.views.login.RegisteredFragment


class LoginTabAdapter(baseFragment: Fragment,val mode:LoginType, val oriId:String,private val dateList: List<String?>) : FragmentStateAdapter(baseFragment) {
    //tab數量
    override fun getItemCount(): Int {
        return dateList.size
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return LoginFragment(mode)
            1 -> return RegisteredFragment(mode,oriId)
        }

        return LoginFragment(mode)
    }

}