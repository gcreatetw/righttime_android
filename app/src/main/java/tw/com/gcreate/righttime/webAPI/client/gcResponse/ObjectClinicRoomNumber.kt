package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ObjectClinicRoomNumber {
    /**
     * clinic_id : 1
     * room_id : 1
     * currentNum : 52
     * currentDoctor : 賴醫師
     */
    var clinic_id = 0
    var room_id = 0
    var currentNum = 0
    var currentDoctor: String? = null
}