package tw.com.gcreate.righttime.adapters.tab2

import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.CardHealthinfoitemBinding
import tw.com.gcreate.righttime.views.tab2.HealthInfoFragment
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseHeHoCategoryItem

class HealthInfoCategoryAdapter(data: MutableList<ResponseHeHoCategoryItem>) :
    BaseQuickAdapter<ResponseHeHoCategoryItem, DataBindBaseViewHolder>(R.layout.card_healthinfoitem, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: ResponseHeHoCategoryItem) {
        val binding: CardHealthinfoitemBinding = holder.getViewBinding() as CardHealthinfoitemBinding
        binding.executePendingBindings()
        binding.data = item
        if (holder.layoutPosition == HealthInfoFragment.healthTitleDefaultPosition) {
            binding.imgUnderlineHealth.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        } else {
            binding.imgUnderlineHealth.background = null
        }
    }
}