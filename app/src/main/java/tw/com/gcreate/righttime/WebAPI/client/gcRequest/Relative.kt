package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class Relative(
    var name: String,
    var kinship: String,
    var personalId: String,
    var birthday: String,
)