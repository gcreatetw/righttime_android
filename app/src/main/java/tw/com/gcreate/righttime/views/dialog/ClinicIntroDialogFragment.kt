package tw.com.gcreate.righttime.views.dialog

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab1.clinicDetail.ClinicDetailIntroAdapter
import tw.com.gcreate.righttime.adapters.tab1.clinicDetail.ClinicInfoExtraLinkAdapter
import tw.com.gcreate.righttime.adapters.tab1.clinicDetail.DoctorHeadShotAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicIntroDialogBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.indicator.LinePagerIndicatorDecoration
import tw.com.gcreate.righttime.views.webView.WebViewActivity
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Doctor
import tw.com.gcreate.righttime.webAPI.client.gcResponse.Pic
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicInfo
import tw.com.gcreate.righttime.webAPI.client.gcResponse.SocialInfo

class ClinicIntroDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentClinicIntroDialogBinding
    private var clinic: ResponseClinicInfo? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val dm = DisplayMetrics()
            requireActivity().windowManager.defaultDisplay.getMetrics(dm)
            dialog.window!!.setLayout((dm.widthPixels * 0.9).toInt(), (dm.heightPixels * 0.9).toInt())
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentClinicIntroDialogBinding.inflate(inflater, container, false)

        GcreateApiClient.getClinicInfo(model.selectedClinicInfo.get()!!.clinic_id,
            object : ApiController<ResponseClinicInfo>(requireActivity(), false) {
                override fun onSuccess(response: ResponseClinicInfo) {
                    clinic = response

                    //----  Clinic Intro Banner ----
                    val picsBeans = clinic!!.pics
                    val picsBeanFilterList = mutableListOf<Pic>()
                    for (item in picsBeans) {
                        if (item.type == "Intro") {
                            picsBeanFilterList.add(item)
                        }
                    }
                    // 診所圖片
                    setupClinicIntroRecyclerView(picsBeanFilterList)
                    // 社群外聯
                    setupExtraLinkRecyclerView(clinic!!.socialInfo)
                    // 醫生圖片
                    setupDoctorHeadshotRecyclerview(clinic!!.doctors)
                    // 醫生介紹
                    setupDoctorIntroRecyclerview()
                }

            })


        return binding.root
    }


    private fun setupClinicIntroRecyclerView(list: List<Pic>) {
        binding.clinicIntro.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            addItemDecoration(LinePagerIndicatorDecoration())

            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(binding.clinicIntro)

            val mAdapter = ClinicDetailIntroAdapter(list)
            adapter = mAdapter
        }

        /*  Recycler 輪播 */
        /*
                final SmoothLinearLayoutManager layoutManager = new SmoothLinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerview.setLayoutManager(layoutManager);
                recyclerview.setHasFixedSize(true);
                recyclerview.setAdapter(adapter);
                recyclerview.scrollToPosition(list.size() *10);

                PagerSnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView(recyclerview);

                final BannerIndicator bannerIndicator = view.findViewById(R.id.indicator);
                if (list.size() > 1) {
                    bannerIndicator.setPosition(0);
                    bannerIndicator.setNumber(list.size());

                    recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                int i = layoutManager.findFirstVisibleItemPosition() % list.size();
                                bannerIndicator.setPosition(i);
                            }
                        }
                    });

                    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
                    scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                        @Override
                        public void run() {
                            recyclerview.smoothScrollToPosition(layoutManager.findFirstVisibleItemPosition() + 1);
                        }
                    }, 2000, 2000, TimeUnit.MILLISECONDS);
                }
        */
    }

    private fun setupExtraLinkRecyclerView(list: List<SocialInfo>) {
        binding.clinicDialogExtraLink.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            val itemList = mutableListOf<String>()
            for (item in list) {
                if (!item.facebook.isNullOrEmpty()) {
                    itemList.add("FaceBook")
                }
                if (!item.line.isNullOrEmpty()) {
                    itemList.add("Line@")
                }
                if (!item.website.isNullOrEmpty()) {
                    itemList.add("OfficialWebsite")
                }
            }
            val mAdapter = ClinicInfoExtraLinkAdapter(itemList)
            adapter = mAdapter
            mAdapter.setOnItemClickListener { _, _, position ->
                when (itemList[position]) {
                    "Line@" -> {
                        if (isAppInstalled(context.getString(R.string.package_name_line))) {
                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("line://ti/p/" + clinic!!.socialInfo[1].line))
                            startActivity(intent)
                        } else {
                            Toast.makeText(requireActivity(), "please install Line app", Toast.LENGTH_LONG).show()
                        }
                    }
                    "OfficialWebsite" -> {
                        val intent = Intent(requireActivity(), WebViewActivity::class.java)
                        intent.putExtra("officialWebUrl", clinic!!.socialInfo[2].website)
                        startActivity(intent)
                    }
                    "FaceBook" -> {
                        goToFacebookPage(clinic!!.socialInfo[0].facebook)
                    }
                    "latestNews" -> {
                    }
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupDoctorHeadshotRecyclerview(list: MutableList<Doctor>) {
        binding.doctorHeadshot.apply {
            /* HeadShot 點選一致中間
            CenterLayoutManager centerLayoutManager = new CenterLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            centerLayoutManager.smoothScrollToPosition(recyclerview, new RecyclerView.State(), position); */
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)

            val mAdapter = DoctorHeadShotAdapter(list)
            adapter = mAdapter
            mAdapter.setOnItemClickListener { _, _, position ->
                DoctorHeadShotAdapter.currentPosition = position
                mAdapter.notifyDataSetChanged()

                binding.includeDoctorIntro.doctorIntroEducation.apply {
                    if (clinic!!.doctors[position].education.isNotEmpty()) {
                        val stringBuffer = StringBuffer()
                        for (text in clinic!!.doctors[position].education) {
                            stringBuffer.append(text + '\n')
                        }
                        text = stringBuffer
                    }
                }

                binding.includeDoctorIntro.doctorIntroExperience.apply {
                    if (clinic!!.doctors[position].experience.isNotEmpty()) {
                        val stringBuffer = StringBuffer()
                        for (text in clinic!!.doctors[position].experience) {
                            stringBuffer.append(text + '\n')
                        }
                        text = stringBuffer
                    }
                }
            }
        }
    }

    private fun setupDoctorIntroRecyclerview() {
        binding.includeDoctorIntro.doctorIntroEducation.apply {
            if (clinic!!.doctors.size > 0) {
                if (clinic!!.doctors[0].education.isNotEmpty()) {
                    val stringBuffer = StringBuffer()
                    for (text in clinic!!.doctors[0].education) {
                        stringBuffer.append(text + '\n')
                    }
                    text = stringBuffer
                }
            }
        }

        binding.includeDoctorIntro.doctorIntroExperience.apply {
            if (clinic!!.doctors.size > 0) {
                if (clinic!!.doctors[0].experience.isNotEmpty()) {
                    val stringBuffer = StringBuffer()
                    for (text in clinic!!.doctors[0].experience) {
                        stringBuffer.append(text + '\n')
                    }
                    text = stringBuffer
                }
            }
        }
    }

    private fun goToFacebookPage(id: String?) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/$id"))
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/$id"))
            startActivity(intent)
        }
    }

    /**
     * check the app is installed
     * Android11及以上APP無法獲取到所有安装包的問題。https://blog.csdn.net/mozushixin_1/article/details/125663302
     */
    private fun isAppInstalled(packageName: String): Boolean {
        var packageInfo: PackageInfo?
        try {
            packageInfo = requireContext().packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            packageInfo = null
            e.printStackTrace()
        }
        return packageInfo != null
    }

    override fun onStop() {
        super.onStop()
        DoctorHeadShotAdapter.currentPosition = 0
    }
}