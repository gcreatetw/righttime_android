package tw.com.gcreate.righttime.adapters.tab1

import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.ClinicNameCardBinding
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ClinicListItem

class ClinicListAdapter(data: MutableList<ClinicListItem>) :
    BaseQuickAdapter<ClinicListItem, DataBindBaseViewHolder>(R.layout.clinic_name_card, data) {

    init {
        data.sortWith(
            Comparator.comparing(ClinicListItem::open)
                .reversed()
                .thenComparing(ClinicListItem::center_len)
        )
    }


    override fun convert(holder: DataBindBaseViewHolder, item: ClinicListItem) {

        val binding: ClinicNameCardBinding = holder.getViewBinding() as ClinicNameCardBinding
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.pic).into(binding.imageClinic)

        holder.setText(R.id.textClinicName, item.name)
        holder.setText(R.id.textClinicAddr, String.format(holder.itemView.context.resources.getString(R.string.text_address), item.address))

        val phoneText = String.format(holder.itemView.context.resources.getString(R.string.text_phone), item.contact, " ", item.extension)
        holder.setText(R.id.textClinicPhone, phoneText)

        val distanceText = String.format(holder.itemView.context.resources.getString(R.string.text_distance), item.center_len.toString())
        holder.setText(R.id.textClinicDistance, distanceText)
        if (item.open) {
            holder.itemView.findViewById<LinearLayout>(R.id.maskLinearLayout).visibility = View.INVISIBLE
        } else {
            holder.itemView.findViewById<LinearLayout>(R.id.maskLinearLayout).visibility = View.VISIBLE
        }
    }

}