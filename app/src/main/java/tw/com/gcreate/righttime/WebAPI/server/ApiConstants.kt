package tw.com.gcreate.righttime.webAPI.server


import android.content.Context
import tw.com.gcreate.righttime.R

object ApiConstants {

    enum class ServerType {
        API_GCreate_DEBUG,
        API_GCreate_FORMAL,
        API_OhBotTech_DEBUG,
        API_OhBotTech_FORMAL,
    }

    var SERVER_PATH = ""
    var SERVER_URL = ""

    fun getServerUrl(context: Context, serverType: ServerType): String {
        when (serverType) {
            ServerType.API_GCreate_DEBUG -> {
                SERVER_URL = context.getString(R.string.gcreate_server_host_debug)
            }
            ServerType.API_GCreate_FORMAL -> {
                SERVER_URL = context.getString(R.string.gcreate_server_host_formal)
            }
            ServerType.API_OhBotTech_DEBUG -> {
                SERVER_URL = context.getString(R.string.ohBotTech_server_host_debug)
                SERVER_PATH = context.getString(R.string.ohBotTech_server_path)
            }
            ServerType.API_OhBotTech_FORMAL -> {
                SERVER_URL = context.getString(R.string.ohBotTech_server_host_formal)
                SERVER_PATH = context.getString(R.string.ohBotTech_server_path)
            }
        }
        return SERVER_URL
    }

}