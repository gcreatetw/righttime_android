package tw.com.gcreate.righttime.util

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import tw.com.gcreate.righttime.R

class SomeFunc {

    companion object {

        fun hideSoftKeyBoard(activity: FragmentActivity, view: View) {
            // 關閉軟鍵盤
            val imm = (activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun setToolbarView(context: FragmentActivity, fragment: Fragment, toolbar: Toolbar, titleText: String, Logo: ImageView,isHideLogo:Boolean) {

            toolbar.apply {
                title = titleText
                setTitleTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                setNavigationIcon(R.drawable.toolbar_icon_back)
            }

            if (isHideLogo){
                Logo.background = null
            }

            fragment.setHasOptionsMenu(true)
            (context as AppCompatActivity?)!!.setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener {
                context.onBackPressed()
            }
        }
    }


}