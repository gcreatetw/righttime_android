package tw.com.gcreate.righttime.viewModel

//import tw.com.gcreate.righttime.global.getNotifyList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import tw.com.gcreate.righttime.adapters.tab1.ClinicNumberNotifyAdapter
import tw.com.gcreate.righttime.databinding.FragmentClinicTypeBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.sharedpreferences.TodayNotifyCardDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import java.util.*

class ClinicTypeViewModel(private val binding: FragmentClinicTypeBinding, navController: NavController, private val model: MainViewModel) :
    ViewModel() {

    var isShowTodayNotifyCard = ObservableField<Boolean>(false)


    private var notifyList: MutableLiveData<MutableList<ObjectNotifyCard2>> = MutableLiveData() //今日就診
    private var notifyList2: MutableList<ObjectNotifyCard2> = ArrayList() //今日就診

    private var notifyListPriv: MutableList<ObjectNotifyCard2> = ArrayList() //回診提醒

    private var mClinicNumberNotifyAdapter: ClinicNumberNotifyAdapter =
        ClinicNumberNotifyAdapter(navController, notifyList2, binding.root, model)   //  今日就診


    fun setTodayNotifyAdapter(newAdapter: ClinicNumberNotifyAdapter) {
        mClinicNumberNotifyAdapter = newAdapter
    }

    fun getTodayNotifyAdapter(): ClinicNumberNotifyAdapter {
        return mClinicNumberNotifyAdapter
    }

    fun getTodayNotifyData() {
        TodayNotifyCardDataMaintain.getNotifyCardList(binding.root.context)
//        notifyListPriv.clear()
        notifyList2.clear()

        for (i in TodayNotifyCardDataMaintain.getNotifyCardList(binding.root.context)) {
            if (i.date.toInt() - global.date.toInt() != 0) {
                //notifyListPriv.add(i)
            } else {
                notifyList2.add(i)
            }
        }

        if (notifyList2.size > 0) {
            isShowTodayNotifyCard.set(true)
        } else {
            isShowTodayNotifyCard.set(false)
        }
        notifyList.postValue(notifyList2)
    }

    fun getTodayNotifyListObserver(): MutableLiveData<MutableList<ObjectNotifyCard2>> {
        return notifyList
    }

    fun setTodayNotifyList(data: MutableList<ObjectNotifyCard2>) {
        mClinicNumberNotifyAdapter.data = data
        mClinicNumberNotifyAdapter.refresh(data)
    }

}

class ClinicTypeViewModelModelFactory(
    private val binding: FragmentClinicTypeBinding,
    private val navController: NavController,
    private val model: MainViewModel,
) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ClinicTypeViewModel(binding, navController, model) as T
    }

}