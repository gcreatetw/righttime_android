package tw.com.gcreate.righttime.views.webView

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.ActivityWebviewBinding


class WebViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebviewBinding

    private var url: String? = null

    companion object {
        private const val HeHoDomain = "https://heho.com.tw/archives/"
        private const val WebFixedEnding = "?utm_source=app&utm_medium=heho&utm_campaign=rtime"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview)

        val webView = binding.webView
        val toolbar = binding.toolbar.materialToolbar

        when {
            intent.getStringExtra("bannerUrlLink") != null -> {
                url = intent.getStringExtra("bannerUrlLink")
                toolbar.visibility = View.GONE
            }
            intent.getStringExtra("officialWebUrl") != null -> {
                url = intent.getStringExtra("officialWebUrl")
                toolbar.visibility = View.VISIBLE
                toolbar.setNavigationOnClickListener { onBackPressed() }
            }
            intent.getStringExtra("articleUrl") != null -> {
                url = intent.getStringExtra("articleUrl")
                if (url!!.contains("heho")) {
                    val articleID = url!!.split("=").toTypedArray()
                    url = HeHoDomain + articleID[1] + WebFixedEnding
                }
                toolbar.visibility = View.VISIBLE
                toolbar.setNavigationOnClickListener { onBackPressed() }
            }
        }

        //賦值
        binding.webView.apply {
            settings.javaScriptEnabled = true
            webViewClient = WebViewClient()
        }

        webView.loadUrl(url!!)
    }

}