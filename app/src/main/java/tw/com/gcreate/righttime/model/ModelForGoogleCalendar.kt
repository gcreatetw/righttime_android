package tw.com.gcreate.righttime.model

class ModelForGoogleCalendar {

    constructor(year: Int , month: Int , date: Int , hourOfDay: Int , minute: Int) {
        this.year = year
        this.month = month
        this.date = date
        this.hourOfDay = hourOfDay
        this.minute = minute
    }

    var year: Int = 0
    var month: Int = 0
    var date: Int = 0
    var hourOfDay: Int = 0
    var minute: Int = 0



}
