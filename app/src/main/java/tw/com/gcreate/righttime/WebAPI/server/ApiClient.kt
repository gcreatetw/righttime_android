package tw.com.gcreate.righttime.webAPI.server

//import tw.com.gcreate.righttime.webAPI.Webapi
import android.annotation.SuppressLint
import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import tw.com.gcreate.righttime.BuildConfig
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.webAPI.client.GcreateApi
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak") // TODO <<==  what is it ?
object ApiClient {

    lateinit var gcreateApi: GcreateApi
    lateinit var mContext: Context

    fun header(includeToken: Boolean): HashMap<String, String> {
        val map = HashMap<String, String>()

        return map
    }

    /*
    因為 Retrofit 的限制，如果要將 API 分不同介面的話，必須分別 create，無法使用類似以下的方式來重構
    public interface ApiEndpoints extends InvoiceApi, ConfigApi, CreditCardApi, EventApi, LoginApi, MessageApi, MyApi, PayApi, ReimburseApi, ProductApi {
    }
    */
    fun initApi(context: Context) {
        mContext = context

        val gcreateApiServerUrl = ApiConstants.getServerUrl(context, ApiConstants.ServerType.API_GCreate_FORMAL)

        gcreateApi = getRetrofit(gcreateApiServerUrl).create(GcreateApi::class.java)
        //  global.webapi = getRetrofit2(gcreateApiServerUrl).create(Webapi::class.java)
    }

    private fun getRetrofit(serverPath: String): Retrofit {
        val builder = Retrofit.Builder().baseUrl(serverPath).addConverterFactory(GsonConverterFactory.create())
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            val httpClient =
                OkHttpClient.Builder()
                    .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS).build()
            builder.client(httpClient)
        }
        return builder.build()
    }

    private fun getRetrofit2(serverPath: String): Retrofit {
        val builder = Retrofit.Builder().baseUrl(serverPath).addConverterFactory(GsonConverterFactory.create())
        //var FBToken = FirebaseInstanceId.getInstance().token
        val logging = HttpLoggingInterceptor()
        val httpClient =
            OkHttpClient.Builder()
                .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor { chain ->
                    if (global.FireBasToken != null) {
                        val builder = chain.request().newBuilder()
                            .header("Device-ID", global.UUID)
                            .header("FCM-token", global.FireBasToken)
                        val build: Request = builder.build()
                        chain.proceed(build)
                    } else {
                        val builder = chain.request().newBuilder()
                            .header("Device-ID", global.UUID)
                        val build: Request = builder.build()
                        chain.proceed(build)
                    }
                    //                                .header("sessionId", "155056366467311249");
                }
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(3, TimeUnit.SECONDS)
                .build()

        builder.client(httpClient)

        return builder.build()
    }

}
