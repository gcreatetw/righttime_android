package tw.com.gcreate.righttime.views.tab1

import android.Manifest.permission
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import info.hoang8f.android.segmented.SegmentedGroup
import kotlinx.coroutines.runBlocking
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab1.ClinicRoomNumberAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.DataBean
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.FavClinicListDataMaintain
import tw.com.gcreate.righttime.util.PdfTool
import tw.com.gcreate.righttime.views.dialog.ClinicIntroDialogFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestGcLogin
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotUserCreate
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.*
import java.util.*

class ClinicInfoFragment : Fragment(), RadioGroup.OnCheckedChangeListener {

    private lateinit var binding: FragmentClinicInfoBinding
    private lateinit var navController: NavController

    private val args: ClinicInfoFragmentArgs by navArgs()
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: ClinicRoomNumberAdapter? = null
    private var isFav = false
    private var isRefresh = false
    private var menu: Menu? = null

    private lateinit var segmented: SegmentedGroup

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    /** 每 20 秒更新門診號碼*/
    private val handler = Handler(Looper.getMainLooper())
    private val task: Runnable = object : Runnable {
        override fun run() {
            // TODOAuto-generated method stub
            handler.postDelayed(this, (20 * 1000).toLong()) //设置延迟时间，此处是20秒
            //需要执行的代码
            getNewClinicRoom()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handler.postDelayed(task, 20000) //延迟调用

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_info, container, false)
        navController = NavHostFragment.findNavController(this)

        setView()
        viewAction()

        getApiData()

        return binding.root
    }


    private fun getApiData() {
        GcreateApiClient.getClinicInfo(args.clinicId, object : ApiController<ResponseClinicInfo>(requireActivity(), false) {
            override fun onSuccess(response: ResponseClinicInfo) {
                model.selectedClinicInfo = ObservableField(response)
                binding.data = response

                setToolbarView()

                // 診所圖
                for (item in response.pics) {
                    if (item.type == "Cover") {
                        Glide.with(requireActivity()).load(item.url).error(R.mipmap.page_icon).into(binding.imageClinic)
                        model.selectedClinicInfo.get()!!.pic = item.url
                    } else if (item.type == "Schedule") {
                        binding.ivCalendar.visibility = View.VISIBLE
                    }
                }

                // 每周門診時刻表
                when (Calendar.getInstance()[Calendar.DAY_OF_WEEK]) {
                    Calendar.MONDAY -> segmented.check(R.id.btn_monday)
                    Calendar.TUESDAY -> segmented.check(R.id.btn_tuesday)
                    Calendar.WEDNESDAY -> segmented.check(R.id.btn_wednesday)
                    Calendar.THURSDAY -> segmented.check(R.id.btn_thusday)
                    Calendar.FRIDAY -> segmented.check(R.id.btn_friday)
                    Calendar.SATURDAY -> segmented.check(R.id.btn_saturday)
                    Calendar.SUNDAY -> segmented.check(R.id.btn_sunday)
                }

                if (isRefresh) {
                    mAdapter!!.refresh(response.room)
                    isRefresh = false
                } else {
                    mAdapter!!.refresh(response.room)
                }

                if (model.selectedClinicSimple.get() == null) {
                    val clinicInfo = model.selectedClinicInfo.get()!!
                    val simpleData = ClinicListItem(
                        clinicInfo.address,
                        global.getDistance(clinicInfo.lat, clinicInfo.lng),
                        clinicInfo.contact,
                        clinicInfo.extension,
                        clinicInfo.clinic_id,
                        clinicInfo.lat,
                        clinicInfo.lng,
                        clinicInfo.name,
                        clinicInfo.open,
                        model.selectedClinicInfo.get()!!.pic,
                        ""
                    )
                    model.selectedClinicSimple.set(simpleData)
                }

                // - NOTE: 如果從QRCODE掃進來與沒加入最愛就開啟dialog
                if (!isFav && args.isComeFormQR) {
                    //設定 false 不然從 qrcode 進來往下滑會一直問要不要加入最愛
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage("是否要加入最愛")
                        setPositiveButton("OK") { _, _ ->
                            isFav = true
                            FavClinicListDataMaintain.addFavClinicListItem(requireContext(), model.selectedClinicSimple.get()!!)
                            menu!!.findItem(R.id.action_add_fav).setIcon(R.drawable.icon_like_selected)
                        }
                        setNegativeButton("Cancel") { _, _ -> }
                    }.create().show()
                }
                binding.refreshLayout.isRefreshing = false
            }

            override fun onFail(httpResponse: Response<ResponseClinicInfo>): Boolean {
                Toast.makeText(context, resources.getText(R.string.text_rest_api_fail), Toast.LENGTH_LONG).show()
                binding.refreshLayout.isRefreshing = false
                return super.onFail(httpResponse)
            }
        })

        if(model.clinicBannerList.get().isNullOrEmpty()){
            GcreateApiClient.getClinicBanner(object : ApiController<ObjectBannerList>(requireActivity(), false) {
                override fun onSuccess(response: ObjectBannerList) {
                    model.clinicBannerList.set(response)
                    setBanner(model.clinicBannerList.get())
                }
                override fun onFail(httpResponse: Response<ObjectBannerList>): Boolean {
                    setBanner(null)
                    return super.onFail(httpResponse)
                }
            })
        }else{
            setBanner(model.clinicBannerList.get())
        }
    }

    /**   Banner function    */
    private fun setBanner(response: ObjectBannerList?) {
        val banner = binding.clinicTypeBanner.banner
        banner.layoutParams.height = ((global.windowHeightPixels * 0.123).toInt())

        if (response == null) {
            //  Set Default Data
            banner.setAdapter(object : BannerImageAdapter<DataBean>(DataBean.bannerImages()) {
                override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
                    val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(activity as Context, data.imageRes)[0])
                    Glide.with(holder.itemView).load(dm).into(holder.imageView)
                }
            }).addBannerLifecycleObserver(this@ClinicInfoFragment).indicator = CircleIndicator(activity)

        } else {
            val imagesUrl: MutableList<String> = ArrayList()
            for (item in response) {
                imagesUrl.add(item.url)
            }
            banner.setAdapter(object : BannerImageAdapter<String>(imagesUrl) {
                override fun onBindView(holder: BannerImageHolder, data: String, position: Int, size: Int) {
                    Glide.with(holder.itemView).load(imagesUrl[position]).into(holder.imageView)
                }
            }).addBannerLifecycleObserver(this@ClinicInfoFragment).indicator = CircleIndicator(activity)

            banner.setOnBannerListener { _, position ->
                if (response[position].link.isNotEmpty()) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(response[position].link))
                    startActivity(browserIntent)
//                    val intent = Intent(activity, WebViewActivity::class.java)
//                    intent.putExtra("bannerUrlLink", response[position].link)
//                    startActivity(intent)
                }
            }
        }

        // 輪播方式
        banner.setPageTransformer(AlphaPageTransformer())
            .setIndicatorSelectedColor(ContextCompat.getColor(activity as Context, R.color.colorYellow)).setIndicatorSpace(16)
            .setIndicatorWidth(15, 15).start()

    }

    //自動刷新診所號碼
    private fun getNewClinicRoom() {
        GcreateApiClient.getClinicInfo(args.clinicId, object : ApiController<ResponseClinicInfo>(requireActivity(), false) {
            override fun onSuccess(response: ResponseClinicInfo) {
                model.selectedClinicInfo = ObservableField(response)
                // 診所圖
                for (item in response.pics) {
                    when (item.type) {
                        "Cover" -> {
                            Glide.with(requireActivity()).load(item.url).error(R.mipmap.page_icon).into(binding.imageClinic)
                            model.selectedClinicInfo.get()!!.pic = item.url
                        }
                        "Schedule" -> {
                            binding.ivCalendar.visibility = View.VISIBLE
                        }
                        else -> {
                            model.selectedClinicInfo.get()!!.pic = ""
                        }
                    }
                }

                if (isRefresh) {
                    mAdapter!!.refresh(response.room)
                    isRefresh = false
                } else {
                    mAdapter!!.refresh(response.room)
                }
                binding.refreshLayout.isRefreshing = false
            }

            override fun onFail(httpResponse: Response<ResponseClinicInfo>): Boolean {
                binding.refreshLayout.isRefreshing = false
                return super.onFail(httpResponse)
            }
        })
    }

    //  跳轉畫面取消執行續
    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(task)
        segmented.clearCheck()
    }

    private fun setView() {

        binding.apply {
            //跑馬燈
            textAnnouncement.isSelected = true

            segmentedTab.apply {
                segmented = binding.segmentedTab
                setTintColor(ContextCompat.getColor(requireActivity(), R.color.colorWhite),
                    ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
                setOnCheckedChangeListener(this@ClinicInfoFragment)
            }

            rvClinicRoomNumber.apply {
                mRecyclerView = binding.rvClinicRoomNumber
                layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                mAdapter = ClinicRoomNumberAdapter(findNavController(), mutableListOf(), model)
                adapter = mAdapter
            }
        }
    }

    private fun setToolbarView() {
        for (item in FavClinicListDataMaintain.getFavClinicDataList(requireContext())) {
            if (args.clinicId == item.id) {
                isFav = true
                break
            }
        }

        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity?)!!.setSupportActionBar(binding.clinicInfoToolbar.materialToolbar)

        binding.clinicInfoToolbar.materialToolbar.title = ""
        binding.clinicInfoToolbar.materialToolbar.setNavigationIcon(R.drawable.toolbar_icon_back)
        binding.clinicInfoToolbar.materialToolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        this.menu = menu
        inflater.inflate(R.menu.clinicinfo_without_detail, menu)
        if (isFav) menu.findItem(R.id.action_add_fav).setIcon(R.drawable.icon_like_selected)
        else menu.findItem(R.id.action_add_fav).setIcon(R.drawable.icon_like)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val favClinicList = FavClinicListDataMaintain.getFavClinicDataList(requireContext())
        when (item.itemId) {
            R.id.action_add_fav -> isFav = if (isFav) {
                for (i in 0 until favClinicList.size) {
                    if (favClinicList[i].id == model.selectedClinicSimple.get()?.id) {
                        FavClinicListDataMaintain.removeFromFavListItem(requireContext(), i)
                        break
                    }
                }
                item.setIcon(R.drawable.icon_like)
                !isFav
            } else {
                FavClinicListDataMaintain.addFavClinicListItem(requireContext(), model.selectedClinicSimple.get()!!)
                item.setIcon(R.drawable.icon_like_selected)
                !isFav
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /* ViewAction*/
    private fun viewAction() {
        //  畫面重整
        binding.refreshLayout.setOnRefreshListener {
            isRefresh = true
            getApiData()
        }

        //  點擊診所地址開google map 導航
        binding.fixTvAddress.setOnClickListener {
            showGoogleMap(binding.fixTvAddress.text.toString().replace(requireActivity().resources.getString(R.string.text_address), ""))
        }

        //  點擊撥打診所電話
        binding.textClinicPhone.setOnClickListener {
            if (ContextCompat.checkSelfPermission(requireContext(), permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                val dial = model.selectedClinicInfo.get()!!.contact
                val ext = model.selectedClinicInfo.get()!!.extension
                if (ext.isNullOrEmpty()) {
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$dial"))
                    startActivity(intent)
                } else {
                    val tel = dial + ";" + ext.substring(1, 3)
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$tel"))
                    startActivity(intent)
                }
            } else {
                // request phone call
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission.CALL_PHONE), 100);
            }
        }

        //  點擊查看門診班表
        binding.ivCalendar.setOnClickListener {
            val scheduleUrl: MutableList<String> = ArrayList()
            for (item in model.selectedClinicInfo.get()!!.pics) {
                if (item.type == "Schedule") {
                    scheduleUrl.add(item.url)
                }
            }
            val intent = Intent(activity, CalendarRecycleViewActivity::class.java)
            intent.putStringArrayListExtra("ScheduleUrl", scheduleUrl as ArrayList<String>)
            startActivity(intent)

        }

        //  點擊預約掛號
        binding.tvClinicInfoReservation.setOnClickListener {
            if (model.appState.gCMemberId.get().equals("")) {
                // 未登入
                val action = ClinicInfoFragmentDirections.actionClinicInfoFragmentToLoginTypeFragment()
                if (model.selectedClinicInfo.get()!!.orgId != null) {
                    action.orgId = model.selectedClinicInfo.get()!!.orgId!!
                }
                findNavController().navigate(action)
            } else {
                // 有登入
                getUserInfo()
            }
        }

        //  點擊詳細資訊
        binding.tvClinicInfoDetail.setOnClickListener {
            val dialogFragment = ClinicIntroDialogFragment()
            dialogFragment.show(requireActivity().supportFragmentManager, "simple dialog")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            100 -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                return
            }
        }
    }

    private fun showGoogleMap(address: String) {
        val uri = Uri.parse("geo:0,0?q=$address")
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = uri
        if (intent.resolveActivity(global.context.packageManager) != null) {
            startActivity(intent)
        }
    }

    /*門診時刻表*/
    private fun setDoctorNameOnSchedule(clinicInfo: ResponseClinicInfo, day: Int) {
        val schedule = clinicInfo.schedule
        when (day) {
            Calendar.MONDAY -> {
                segmented.check(R.id.btn_monday)
                setClinicDoctors(schedule.monday.morning, schedule.monday.afternoon, schedule.monday.evening)
            }
            Calendar.TUESDAY -> {
                segmented.check(R.id.btn_tuesday)
                setClinicDoctors(schedule.tuesday.morning, schedule.tuesday.afternoon, schedule.tuesday.evening)
            }
            Calendar.WEDNESDAY -> {
                segmented.check(R.id.btn_wednesday)
                setClinicDoctors(schedule.wednesday.morning, schedule.wednesday.afternoon, schedule.wednesday.evening)
            }
            Calendar.THURSDAY -> {
                segmented.check(R.id.btn_thusday)
                setClinicDoctors(schedule.thursday.morning, schedule.thursday.afternoon, schedule.thursday.evening)
            }
            Calendar.FRIDAY -> {
                segmented.check(R.id.btn_friday)
                setClinicDoctors(schedule.friday.morning, schedule.friday.afternoon, schedule.friday.evening)
            }
            Calendar.SATURDAY -> {
                segmented.check(R.id.btn_saturday)
                setClinicDoctors(schedule.saturday.morning, schedule.saturday.afternoon, schedule.saturday.evening)
            }
            Calendar.SUNDAY -> {
                segmented.check(R.id.btn_sunday)
                setClinicDoctors(schedule.sunday.morning, schedule.sunday.afternoon, schedule.sunday.evening)
            }
        }

        binding.apply {
            textViewMorningTime.text = String.format(resources.getString(R.string.text_opentime), clinicInfo.morningFrom, clinicInfo.morningTo)
            textViewAfternoonTime.text = String.format(resources.getString(R.string.text_opentime), clinicInfo.afternoonFrom, clinicInfo.afternoonTo)
            textViewEveningTime.text = String.format(resources.getString(R.string.text_opentime), clinicInfo.eveningFrom, clinicInfo.eveningTo)
        }
    }

    private fun setClinicDoctors(morningDrName: List<String>, afternoonDrName: List<String>, eveningDrName: List<String>) {
        val morningDr = binding.textViewMorningName
        val afternoonDr = binding.textViewAfternoonName
        val nightDr = binding.textViewNightName

        morningDr.text = ""
        afternoonDr.text = ""
        nightDr.text = ""

        //  上午診
        if (morningDrName.isEmpty()) morningDr.append("休診")
        morningDrName.indices.forEach { i ->
            if (i < morningDrName.size - 1) {
                morningDr.append(morningDrName[i] + '\n')
            } else {
                morningDr.append(morningDrName[i])
            }
        }
        //  下午診
        if (afternoonDrName.isEmpty()) afternoonDr.append("休診")
        afternoonDrName.indices.forEach { i ->
            if (i < afternoonDrName.size - 1) {
                afternoonDr.append(afternoonDrName[i] + '\n')
            } else {
                afternoonDr.append(afternoonDrName[i])
            }
        }
        //  晚上診
        if (eveningDrName.isEmpty()) nightDr.append("休診")
        eveningDrName.indices.forEach { i ->
            if (i < eveningDrName.size - 1) {
                nightDr.append(eveningDrName[i] + '\n')
            } else {
                nightDr.append(eveningDrName[i])
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        try{
            val clinicInfo = model.selectedClinicInfo.get()!!
            when (checkedId) {
                R.id.btn_monday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.MONDAY)
                    return
                }

                R.id.btn_tuesday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.TUESDAY)
                    return
                }

                R.id.btn_wednesday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.WEDNESDAY)
                    return
                }

                R.id.btn_thusday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.THURSDAY)
                    return
                }

                R.id.btn_friday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.FRIDAY)
                    return
                }

                R.id.btn_saturday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.SATURDAY)
                    return
                }

                R.id.btn_sunday -> {
                    setDoctorNameOnSchedule(clinicInfo, Calendar.SUNDAY)
                    return
                }
            }
        }catch (e:Exception){

        }

    }

    private fun cancelItemSelected() {
        // get selected radio button from radioGroup
        val radioButtonID = segmented.checkedRadioButtonId
        // find the radiobutton by returned id
        val radioButton: RadioButton = segmented.findViewById(radioButtonID)
        radioButton.isChecked = false
    }

    /*預約掛號確認是否在這間診所有註冊*/
    private fun getUserInfo() {
        val phoneText = model.appState.gCLoginMobile.get()!!
        val passwordText = model.appState.gCLoginPassword.get()!!
        val requestBody = RequestGcLogin(phoneText, passwordText, global.FireBasToken)
        GcreateApiClient.gcLogin(requestBody, object : ApiController<ResponseGcLogin>(requireActivity(), true) {
            override fun onSuccess(response: ResponseGcLogin) {
                if (response.login) {
                    runBlocking {
                        model.appState.setGCMemberId(response.member_id)
                    }
                    //登入成功，但是有可能該會員沒在該診所(組織)下面註冊過
                    checkUserHasRegisteredShopMember(response)
                } else {
                    Toast.makeText(requireActivity(), "登入失敗，請檢查帳號密碼", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun checkUserHasRegisteredShopMember(responseGclogin: ResponseGcLogin) {
        // 2.1.1 檢查使⽤者是否已經存在
        val requestBody = RequestOhBotModel(
            "/user/findByOriginId",
            "GET",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(RequestQuestItem("originId", model.appState.gCMemberId.get()!!)),
            null
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                if (response.ohbot_obj != null) {
                    cancelItemSelected()
                    val action = ClinicInfoFragmentDirections.actionClinicInfoFragmentToClinicInfoFragmentReservation()
                    findNavController().navigate(action)
                } else {
                    createOhBotMember(responseGclogin)
                }
            }
        })
    }

    private fun createOhBotMember(response: ResponseGcLogin) {
        // 2.1.2 建⽴⽤⼾使⽤者
        val inputMobile = response.cell_phone
        val inputName = response.name
        val inputGender = if (response.sex == "M") "male" else "female"
        val inputBirthday = response.birth

        val requestBody = RequestOhBotModel(
            "/user/createClientUser",
            "POST",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(),
            RequestOhBotUserCreate("right_time", response.member_id, inputMobile, inputName, inputBirthday, inputGender)
        )

        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                // 註冊流程結束。
                cancelItemSelected()
                val action = ClinicInfoFragmentDirections.actionClinicInfoFragmentToClinicInfoFragmentReservation()
                findNavController().navigate(action)
            }
        })
    }
}