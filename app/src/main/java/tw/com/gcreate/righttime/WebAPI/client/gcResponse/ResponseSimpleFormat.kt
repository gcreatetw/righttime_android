package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseSimpleFormat(
    val success: Boolean,
    val message: String?,
)