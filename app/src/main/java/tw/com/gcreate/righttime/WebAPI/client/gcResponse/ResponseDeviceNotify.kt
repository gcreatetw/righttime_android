package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ResponseDeviceNotify {
    /**
     * success : true
     * message : null
     * targetNum : 100
     * notifyNum : 95
     * deviceToken : dlO260cCa2I:APA91bGoT1iXU459bgaUgSkbeb5kSFwdwoidBD6q5IRhmcIy0LG0tjrwTb6usJO2kaXnUhGX_h6hzlwBp4fpaNzEdGWwxabPWoykwhBcmgVxQWMQjDvHSBGIv0a5_M_037NvhjirXI2u
     */
    var success : Boolean = false
    var message : String? = ""
    var targetNum : String? = ""
    var notifyNum : String? = ""
    var deviceToken: String? = ""
}