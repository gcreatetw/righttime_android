package tw.com.gcreate.righttime.views.tab5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentMemberRelativesAddBinding
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListener
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.util.RightTimeReservationString
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.views.membercentre.DatePickerDialogFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.Relative
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestUserInfoModify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat
import java.util.*
import java.util.regex.Pattern


/**親友管理 -新增親友* */
class RelativesAddFragment : Fragment(), UpdateWheelPickerTextListener {

    private lateinit var binding: FragmentMemberRelativesAddBinding
    private val args: RelativesAddFragmentArgs by navArgs()
    private var dateString: String = ""

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMemberRelativesAddBinding.inflate(inflater, container, false)
        UpdateWheelPickerTextListenerManager.getInstance().registerListener(this)

        SomeFunc.setToolbarView(requireActivity(), this,
            binding.toolbarMember4.materialToolbar, "列表",
            binding.toolbarMember4.toolbarLogo, true)

        if (args.isAdd) {
            binding.apply {
                dateString = Date().RightTimeReservationString
                edRelativeBirthday.apply {
                    text = dateString
                    setOnClickListener {
                        val datePickerDialogFragment = DatePickerDialogFragment(dateString)
                        datePickerDialogFragment.show(requireActivity().supportFragmentManager, "upgrade_dialog")
                    }
                }

                btnCancel.setOnClickListener {
                    requireActivity().onBackPressed()
                }

                btnCheck.setOnClickListener {
                    val relativeDataList = RelativesDataMaintain.getRelativesDataList(requireContext())
                    val inputRelativeName = edRelativeName.text.toString()
                    val inputRelativeKinship = edRelativeKinship.text.toString()
                    val inputRelativePersonalId = edRelativePersonalId.text.toString()
                    val inputRelativeBirthday = edRelativeBirthday.text.toString()

                    if (inputRelativeName.isNotEmpty() && inputRelativeKinship.isNotEmpty() && inputRelativePersonalId.isNotEmpty() && inputRelativeBirthday.isNotEmpty()) {
                        if (Pattern.compile("^[A-Z]\\d{9,11}$").matcher(inputRelativePersonalId).matches()) {
                            relativeDataList.add(Relative(inputRelativeName, inputRelativeKinship, inputRelativePersonalId, inputRelativeBirthday))

                            val requestBody = RequestUserInfoModify(model.appState.gCMemberId.get()!!, null, "", relativeDataList)

                            GcreateApiClient.gcUserInfoModify(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    RelativesDataMaintain.saveRelativesList(requireContext(), relativeDataList)
                                    findNavController().popBackStack()
                                }
                            })
                        } else {
                            Toast.makeText(requireActivity(), "身分證格式不符", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        } else {
            val relativeMemberData = RelativesDataMaintain.getRelativesDataList(requireContext())[args.relativePositon+1]
            dateString = relativeMemberData.birthday
            binding.apply {

                edRelativeKinship.setText(relativeMemberData.kinship)
                edRelativeName.setText(relativeMemberData.name)
                edRelativePersonalId.setText(relativeMemberData.personalId)

                edRelativeBirthday.apply {
                    text = dateString
                    setOnClickListener {
                        val datePickerDialogFragment = DatePickerDialogFragment(dateString)
                        datePickerDialogFragment.show(requireActivity().supportFragmentManager, "upgrade_dialog")
                    }
                }

                btnCancel.setOnClickListener {
                    requireActivity().onBackPressed()
                }

                btnCheck.apply {
                    text = "儲存"
                    setOnClickListener {
                        val relativeDataList = RelativesDataMaintain.getRelativesDataList(requireContext())
                        val inputRelativeName = edRelativeName.text.toString()
                        val inputRelativeKinship = edRelativeKinship.text.toString()
                        val inputRelativePersonalId = edRelativePersonalId.text.toString()
                        val inputRelativeBirthday = edRelativeBirthday.text.toString()

                        if (inputRelativeName.isNotEmpty() && inputRelativeKinship.isNotEmpty() && inputRelativePersonalId.isNotEmpty() && inputRelativeBirthday.isNotEmpty()) {
                            if (Pattern.compile("^[A-Z]\\d{9,11}$").matcher(inputRelativePersonalId).matches()) {
                                relativeDataList.removeAt(args.relativePositon + 1)
                                relativeDataList.add(args.relativePositon + 1,
                                    Relative(inputRelativeName, inputRelativeKinship, inputRelativePersonalId, inputRelativeBirthday))
                                RelativesDataMaintain.saveRelativesList(requireContext(), relativeDataList)
                                findNavController().popBackStack()
                            } else {
                                Toast.makeText(requireActivity(), "身分證格式不符", Toast.LENGTH_SHORT).show()
                            }

                        } else {
                            Toast.makeText(requireActivity(), "欄位不能空白", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
        return binding.root
    }

    override fun updateWheelPicker(mode: Int, textString: String?) {
        dateString = textString!!

        if (mode == 6) {
            binding.edRelativeBirthday.text = textString
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateWheelPickerTextListenerManager.getInstance().unRegisterListener(this)
    }

}