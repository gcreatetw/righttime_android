package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ResponseCityList : ArrayList<CityListItem>()

data class CityListItem(
    val city_id: Int,
    val name: String
)