package tw.com.gcreate.righttime.views.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.config.IndicatorConfig
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.DialogTeachBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.DataBean


/**
 * 教學畫面
 */
class TeachDialogFragment() : DialogFragment() {

    private lateinit var binding: DialogTeachBinding
    private var currentPage: Int? = 0

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog!!.window!!.setBackgroundDrawableResource(R.drawable.layout_bg)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_teach, container, false)

        setView()
        loadBanner()
        arrowClickFunction()

        return binding.root
    }

    /*
         *   Toolbar functions
         * */
    private fun setView() {
        binding.toolbar.materialToolbar.title = "回首頁"
        binding.toolbar.clToolbar.setBackgroundColor(ContextCompat.getColor(requireActivity(), android.R.color.transparent))
        binding.toolbar.materialToolbar.setTitleTextColor(ContextCompat.getColor(requireActivity(), R.color.colorWhite))
        binding.toolbar.toolbarLogo.visibility = GONE
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.toolbar_icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener {
            dialog!!.dismiss()
        }

    }

    /*
        *   Banner function
        * */
    private fun loadBanner() {
        //  Set Default Data
        binding.teachBanner.setAdapter(object : BannerImageAdapter<DataBean>(DataBean.initTeachImages()) {
            override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
                Glide.with(holder.itemView).load(data.imageRes).into(holder.imageView)
            }
        }, false).addBannerLifecycleObserver(this)
            .setIndicator(CircleIndicator(activity), true)

        // 輪播方式
        binding.teachBanner.setPageTransformer(AlphaPageTransformer())
            // Indicator parameters
            .setIndicatorSelectedColor(ContextCompat.getColor(requireActivity(), R.color.colorYellow))
            .setIndicatorSpace(16).setIndicatorWidth(15, 15)
            .setIndicatorMargins(IndicatorConfig.Margins((global.windowHeightPixels * 0.02).toInt()))
            .isAutoLoop(false)
            .setUserInputEnabled(false)
            .setBannerRound(20.0f)
            .stop()
    }

    /** Arrow Onclick Function
     * */
    private fun arrowClickFunction() {
        //  向左
        binding.leftArrow.setOnClickListener {
            if (currentPage == 0) {
                currentPage = binding.teachBanner.realCount - 1
                binding.teachBanner.setCurrentItem(currentPage!!, true)
            } else {
                currentPage = currentPage!! - 1
                binding.teachBanner.setCurrentItem(currentPage!!, true)
            }
        }

        //  向右
        binding.rightArrow.setOnClickListener {
            if (currentPage == binding.teachBanner.realCount - 1) {
                currentPage = 0
                binding.teachBanner.setCurrentItem(currentPage!!, true)
            } else {
                currentPage = currentPage!! + 1
                binding.teachBanner.setCurrentItem(currentPage!!, true)
            }
        }
    }
}