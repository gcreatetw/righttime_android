package tw.com.gcreate.righttime.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab1.LoginTabAdapter
import tw.com.gcreate.righttime.databinding.FragmentLogintypeBinding

class LoginTypeFragment : Fragment() {

    private lateinit var binding: FragmentLogintypeBinding
    private val args: LoginTypeFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLogintypeBinding.inflate(inflater, container, false)

        binding.toolbar.materialToolbar.apply {
            title = ""
            setNavigationIcon(R.drawable.toolbar_icon_back)
            setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }
        }

        //TabLayout
        val tabList = listOf("登入", "註冊資料")
        val tabAdapter = LoginTabAdapter(this, args.mode, args.orgId, tabList)
        binding.apply {
            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = tabAdapter
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                tab.text = tabList[position]
            }.attach()

            return binding.root
        }

    }

}


