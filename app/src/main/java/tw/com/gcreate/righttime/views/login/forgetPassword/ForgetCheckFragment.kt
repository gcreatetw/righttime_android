package tw.com.gcreate.righttime.views.login.forgetPassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import tw.com.gcreate.righttime.databinding.FragmentForgetCheckBinding
import tw.com.gcreate.righttime.util.SomeFunc


class ForgetCheckFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentForgetCheckBinding.inflate(inflater, container, false)
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, true)

        binding.btnCheck.setOnClickListener {
            val action = ForgetCheckFragmentDirections.actionForgetCheckFragmentToForgetInputMobileFragment()
            findNavController().navigate(action)
        }

        return binding.root
    }
}