package tw.com.gcreate.righttime.sharedpreferences


import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ClinicListItem

object FavClinicListDataMaintain {
    var favClinicListDataList: MutableList<ClinicListItem> = mutableListOf()

    fun getFavClinicDataList(context: Context): MutableList<ClinicListItem> {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val responseString = sp.getString("FavClinicListDataList", null)

        val gson = Gson()
        if (!responseString.isNullOrEmpty()) {
            favClinicListDataList = gson.fromJson(responseString, object : TypeToken<MutableList<ClinicListItem>>() {}.type)
        }

        return favClinicListDataList
    }

    fun addFavClinicListItem(context: Context, newData: ClinicListItem) {
        favClinicListDataList!!.add(newData)
        saveFavClinicList(context, favClinicListDataList!!)

    }

    fun saveFavClinicList(context: Context, reservationDataList: MutableList<ClinicListItem>) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("FavClinicListDataList", Gson().toJson(reservationDataList))
        editor.apply()
    }

    fun removeFromFavListItem(context: Context, pos: Int) {
        favClinicListDataList.removeAt(pos)
        saveFavClinicList(context, favClinicListDataList)
    }

}