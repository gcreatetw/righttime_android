package tw.com.gcreate.righttime.listener

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View? , position: Int)
}