package tw.com.gcreate.righttime.adapters.tab1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import tw.com.gcreate.righttime.R

class CalendarAdapter(private val scheduleUrlList: List<String>) : RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.calendar_photoview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageUrl = scheduleUrlList[position]
        Glide.with(holder.itemView).load(imageUrl).into(holder.photoView)
    }

    override fun getItemCount(): Int {
        return scheduleUrlList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var photoView: PhotoView = itemView.findViewById<View>(R.id.PhotoView) as PhotoView

    }
}