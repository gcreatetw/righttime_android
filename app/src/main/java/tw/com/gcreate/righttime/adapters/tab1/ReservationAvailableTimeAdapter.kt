package tw.com.gcreate.righttime.adapters.tab1


import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.RvItemReservationTimeBinding


class ReservationAvailableTimeAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, DataBindBaseViewHolder>(R.layout.rv_item_reservation_time, data) {

    companion object {
        var availableTimeSelectedPosition = 9999
    }

    override fun convert(holder: DataBindBaseViewHolder, item: String) {
        val binding: RvItemReservationTimeBinding = holder.getViewBinding() as RvItemReservationTimeBinding

        binding.executePendingBindings()

        // item time 時區為 UCT 0 ， 顯示要自行 +8小時
        holder.setText(R.id.tv_time, changeTime(item))

        if (getItemPosition(item) == availableTimeSelectedPosition) {
            binding.lyContent.setBackgroundResource(R.drawable.border_reservation_foucus)
            binding.tvTime.setTextColor(ContextCompat.getColor(binding.root.context, R.color.colorPrimary))
        } else {
            binding.lyContent.setBackgroundResource(R.drawable.border_reservation_unfoucus)
            binding.tvTime.setTextColor(ContextCompat.getColor(binding.root.context, R.color.colorBlack))
        }

    }

    private fun changeTime(item: String): String {
        val hour = item.substring(0, 2).toInt() + 8
        return String.format("%02d", hour) + item.substring(2, 5)
    }
}
