package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ResponseClinicTypeList : ArrayList<ClinicTypeItem>()

data class ClinicTypeItem(
    val name: String,
    val pic: String,
    val type_id: Int,
)



