package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ResponseHeHoArticle : ArrayList<ResponseHeHoArticleItem>()

data class ResponseHeHoArticleItem(
    val category: String,
    val fetured_image: String,
    val guid: String,
    val post_date: String,
    val post_title: String,
    val rowid: String,
    val type_id: Int
)