package tw.com.gcreate.righttime.webAPI.client.gcResponse

class ObjectBannerList : ArrayList<ObjectBannerListItem>()

data class ObjectBannerListItem(
    val link: String,
    val seq: Int,
    val title: String,
    val url: String
)