package tw.com.gcreate.righttime.views.reservation

import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import tw.com.gcreate.righttime.adapters.tab1.ReservationAvailableTimeAdapter
import tw.com.gcreate.righttime.adapters.tab1.ReservationAvailableTimeAdapter.Companion.availableTimeSelectedPosition
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoReservation1Binding
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListener
import tw.com.gcreate.righttime.listener.UpdateWheelPickerTextListenerManager
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.util.DateTool
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservationWheelPicker.Companion.reservationRelativesSelectedPosition
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservationWheelPicker.Companion.serviceAvailableTimeSelectedPosition
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservationWheelPicker.Companion.serviceDoctorSelectedPosition
import tw.com.gcreate.righttime.views.reservation.ClinicInfoFragmentReservationWheelPicker.Companion.serviceItemSelectedPosition
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.ClinicReservationDataList
import tw.com.gcreate.righttime.webAPI.client.ClinicServiceInfo
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestOhBotModel
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestQuestItem
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel1
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseModel2

@SuppressLint("NotifyDataSetChanged")
class ClinicInfoFragmentReservation1 : Fragment(), UpdateWheelPickerTextListener {

    private lateinit var binding: FragmentClinicInfoReservation1Binding

    private val serviceInfo = mutableListOf<ClinicServiceInfo>()
    private val reservationInfo = mutableListOf<ClinicReservationDataList>()
    val mAdapter = ReservationAvailableTimeAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    // pass to next page arg
    private var shopMemberId = ""
    private var reservationServiceId = ""
    private var reservationServiceDoctorId = ""
    private var reservationTime = ""
    private var userComment = ""
    private var userName = ""
    private var isDateTimeSelect = false
    private val serviceItemNameList = mutableListOf<String>()

    // selected service position
    private var selectedServicePosition = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentClinicInfoReservation1Binding.inflate(inflater, container, false)
        UpdateWheelPickerTextListenerManager.getInstance().registerListener(this)

        serviceInfo.clear()

        setView()
        checkUserHasRegisteredShopMember()
        getClinicService()

        return binding.root
    }


    private fun setView() {
        val clinicInfo = model.selectedClinicInfo.get()!!
        Glide.with(requireActivity()).load(clinicInfo.pics[0].url).into(binding.imgClinic.img)

        setReservationRelativeSpinner()

        binding.apply {
            imgClinic.tvClinicName.text = clinicInfo.name
            spinnerServiceItem.tvTitle.text = "門診項目"
            spinnerDoctor.tvTitle.text = "門診醫生"
            spinnerTimeInterval.tvTitle.text = "門診時段"
            spinnerRelatives.tvTitle.text = "掛號對象"
        }

        binding.rvContent.apply {
            layoutManager = GridLayoutManager(requireActivity(), 4, GridLayoutManager.VERTICAL, false)

            adapter = mAdapter
            mAdapter.setOnItemClickListener { _, _, position ->
                availableTimeSelectedPosition = position
                isDateTimeSelect = true
                reservationTime = mAdapter.data[position]
                mAdapter.notifyDataSetChanged()
            }

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    super.getItemOffsets(outRect, view, parent, state)
                    val pos = parent.getChildAdapterPosition(view)
                    val spanCount = 4
                    val spacing = 25

                    val column: Int = pos % spanCount
                    outRect.left = spacing - column * spacing / spanCount
                    outRect.right = (column + 1) * spacing / spanCount
                    if (pos < spanCount) {
                        outRect.top = spacing
                    }
                    outRect.bottom = spacing
                }
            })
        }

        binding.reservationBtnCheck.btnCheck.setOnClickListener {
            if (isDateTimeSelect) {
                val action = ClinicInfoFragmentReservation1Directions.actionClinicInfoFragmentReservation1ToClinicInfoFragmentReservation2()
                action.reservationMemberId = shopMemberId
                action.reservationClinicName = model.selectedClinicInfo.get()!!.name
                action.reservationDoctorName = binding.spinnerDoctor.tvWheelPicker.text.toString()
                action.reservationDoctorId = reservationServiceDoctorId
                action.reservationDate = binding.spinnerTimeInterval.tvWheelPicker.text.toString()
                action.reservationServiceId = reservationServiceId
                action.reservationTime = reservationTime
                action.reservationUserName = userName
                action.reservationUserComment = userComment
                findNavController().navigate(action)
            } else {
                Toast.makeText(requireActivity(), "請選擇掛號時間。", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun updateWheelPicker(mode: Int, textString: String) {
        when (mode) {
            1 -> {
                binding.spinnerServiceItem.tvWheelPicker.text = textString
                mAdapter.data.clear()

                for (i in 0 until serviceInfo.size) {
                    if (serviceInfo[i].serviceName == textString) {
                        selectedServicePosition = i
                        reservationServiceId = serviceInfo[i].serviceId
                        reservationServiceDoctorId = serviceInfo[i].serviceDoctorIdList[0]
                        binding.spinnerDoctor.tvWheelPicker.text = serviceInfo[i].serviceDoctorNameList[0]

                        reservationInfo.clear()
                        serviceDoctorSelectedPosition = 0
                        serviceAvailableTimeSelectedPosition = 0
                        // 因服務人員改變，需要重新取得可預約時段
                        availableTimeSelectedPosition = 9999
                        mAdapter.notifyDataSetChanged()
                        getServiceReservationTime(reservationServiceDoctorId)
                        break
                    }
                }
            }

            2 -> {
                binding.spinnerDoctor.tvWheelPicker.text = textString
                mAdapter.data.clear()

                for (i in 0 until serviceInfo.size) {
                    for (j in 0 until serviceInfo[i].serviceDoctorNameList.size) {
                        if (serviceInfo[i].serviceDoctorNameList[j] == textString) {
                            reservationServiceDoctorId = serviceInfo[i].serviceDoctorIdList[j]
                        }
                    }
                }
                reservationInfo.clear()
                serviceAvailableTimeSelectedPosition = 0
                // 因服務人員改變，需要重新取得可預約時段
                availableTimeSelectedPosition = 9999
                mAdapter.notifyDataSetChanged()
                getServiceReservationTime(reservationServiceDoctorId)
            }

            3 -> {
                binding.spinnerTimeInterval.tvWheelPicker.text = textString
                for (i in 0 until reservationInfo.size) {
                    if (reservationInfo[i].availableTime == textString) {
                        mAdapter.data = reservationInfo[i].availableTimeList
                        mAdapter.notifyDataSetChanged()
                    }
                }
            }

            4 -> {
                binding.spinnerRelatives.tvWheelPicker.text = textString
                for (item in RelativesDataMaintain.getRelativesDataList(requireContext())) {
                    if (item.kinship == textString) {
                        userComment = String.format("掛號對象：%s ,身分證字號：%s", item.name, item.personalId)
                        userName = item.name
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateWheelPickerTextListenerManager.getInstance().unRegisterListener(this)
    }

    // 3.2.1 取得診所全部服務
    private fun getClinicService() {
        serviceInfo.clear()
        val transPath = "/shop/${model.selectedClinicInfo.get()!!.shopId!!}/appointmentService"
        GcreateApiClient.getOhBotDataForGet2(transPath, model.selectedClinicInfo.get()!!.orgId!!,
            object : ApiController<ResponseModel2>(requireActivity(), false) {
                override fun onSuccess(response: ResponseModel2) {
                    // 跑有多少個服務
                    if (response.ohbot_obj != null) {
                        for (item in response.ohbot_obj) {
                            val serviceDoctorNameList = mutableListOf<String>()
                            val serviceDoctorIdList = mutableListOf<String>()
                            // 該服務下有多少個服務人員
                            for (i in item.AppointmentUnits.indices) {
                                serviceDoctorNameList.add(item.AppointmentUnits[i].name)
                                serviceDoctorIdList.add(item.AppointmentUnits[i].id)
                            }
                            serviceInfo.add(ClinicServiceInfo(item.name, item.id, serviceDoctorNameList, serviceDoctorIdList))
                        }

                        setClinicServiceSpinner()
                        setClinicDoctorNameSpinner()

                        if (serviceInfo.size > 0) {
                            getServiceReservationTime(serviceInfo[0].serviceDoctorIdList[0])
                        }
                    } else {
                        Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

    // API 3.3.1 ⽤服務⼈員及服務取得可預約時間
    private fun getServiceReservationTime(unitId: String) {
        val startDateOhBotFormat = DateTool.getTimeOhBotFormat(DateTool.nowDate)
        val endDate = DateTool.calculateByDate(DateTool.LongToDate(DateTool.nowDate), 14)
        val endDateOhBotFormat = DateTool.getTimeOhBotFormat2(endDate!!)

        val requestBody = RequestOhBotModel(
            "/shop/${model.selectedClinicInfo.get()!!.shopId!!}/appointmentUnit/${unitId}/availableTime",
            "GET",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(
                RequestQuestItem("start", startDateOhBotFormat),
                RequestQuestItem("end", endDateOhBotFormat),
                RequestQuestItem("appointmentServiceId", reservationServiceId)
            ),
            null
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                val reservationDateList = mutableListOf<String>()
                var availableTimeList = mutableListOf<String>()

                if (response.ohbot_obj != null) {
                    if (reservationInfo.size == 0) {
                        for (item in response.ohbot_obj.times) {
                            val dateText = item.substring(0, 10)
                            if (!reservationDateList.contains(dateText)) {
                                if (reservationDateList.size > 0) {
                                    val availableTimeName = reservationDateList[reservationDateList.size - 1]
                                    // 添加可預約時段
                                    reservationInfo.add(ClinicReservationDataList(availableTimeName, availableTimeList))
                                    availableTimeList = mutableListOf()
                                }
                                reservationDateList.add(dateText)
                                availableTimeList.add(item.substring(11, 16))
                            } else {
                                availableTimeList.add(item.substring(11, 16))
                            }
                        }
                    }

                    setClinicReservationDateSpinner()

                    if (reservationInfo.isNotEmpty()) {
                        mAdapter.data = reservationInfo[0].availableTimeList
                        mAdapter.notifyDataSetChanged()
                    }
                } else {
                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    /* wheelPicker */
    private fun setClinicServiceSpinner() {
        serviceItemNameList.clear()
        for (i in serviceInfo.indices) {
            serviceItemNameList.add(serviceInfo[i].serviceName)
        }
        // 預設第一個服務項目
        if (serviceItemNameList.size > 0) {
            binding.spinnerServiceItem.tvWheelPicker.text = serviceItemNameList[0]
            reservationServiceId = serviceInfo[0].serviceId
            binding.spinnerServiceItem.tvWheelPicker.setOnClickListener {
                val wheelPickerDialogFragment: DialogFragment = ClinicInfoFragmentReservationWheelPicker(1, serviceItemNameList)
                wheelPickerDialogFragment.show(requireActivity().supportFragmentManager, "dialog")
            }
        }

    }

    private fun setClinicDoctorNameSpinner() {
        // 預設第一個服務人員
        if (serviceItemNameList.size > 0) {
            binding.spinnerDoctor.tvWheelPicker.text = serviceInfo[0].serviceDoctorNameList[0]
            reservationServiceDoctorId = serviceInfo[0].serviceDoctorIdList[0]
            binding.spinnerDoctor.tvWheelPicker.setOnClickListener {
                val wheelPickerDialogFragment: DialogFragment =
                    ClinicInfoFragmentReservationWheelPicker(2, serviceInfo[serviceItemSelectedPosition].serviceDoctorNameList)
                wheelPickerDialogFragment.show(requireActivity().supportFragmentManager, "dialog")
            }
        }

    }

    private fun setClinicReservationDateSpinner() {
        val availableTimeNameList = mutableListOf<String>()
        for (i in reservationInfo.indices) {
            availableTimeNameList.add(reservationInfo[i].availableTime)
        }
        // 預設第一個有服務時間的日期
        if (reservationInfo.isNotEmpty()) {
            binding.spinnerTimeInterval.tvWheelPicker.text = reservationInfo[0].availableTime
            binding.spinnerTimeInterval.tvWheelPicker.setOnClickListener {
                val wheelPickerDialogFragment: DialogFragment = ClinicInfoFragmentReservationWheelPicker(3, availableTimeNameList)
                wheelPickerDialogFragment.show(requireActivity().supportFragmentManager, "dialog")
            }
        } else {
            binding.spinnerTimeInterval.tvWheelPicker.text = "無預約時段"
        }

    }

    private fun setReservationRelativeSpinner() {
        val relativesList = RelativesDataMaintain.getRelativesDataList(requireContext())
        userName = relativesList[0].name
        userComment = String.format("掛號對象：%s ,身分證字號：%s", relativesList[0].name, relativesList[0].personalId)
        val kinshipList = mutableListOf<String>()
        for (item in relativesList) {
            kinshipList.add(item.kinship)
        }

        binding.spinnerRelatives.tvWheelPicker.text = relativesList[reservationRelativesSelectedPosition].kinship
        binding.spinnerRelatives.tvWheelPicker.setOnClickListener {
            val wheelPickerDialogFragment: DialogFragment =
                ClinicInfoFragmentReservationWheelPicker(4, kinshipList)
            wheelPickerDialogFragment.show(requireActivity().supportFragmentManager, "dialog")
        }
    }

    // 找User在這診所有沒有註冊
    private fun checkUserHasRegisteredShopMember() {
        // 2.1.1 檢查使⽤者是否已經存在
        val requestBody = RequestOhBotModel(
            "/user/findByOriginId",
            "GET",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(RequestQuestItem("originId", model.appState.gCMemberId.get()!!)),
            null
        )
        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                if (response.ohbot_obj != null) {
                    if (response.ohbot_obj.Members.isNotEmpty()) {
                        // 有註冊過組織(總店)。但不一定有在這間店家(分店)註冊過
                        var isFound = false
                        for (item in response.ohbot_obj.Members) {
                            if (item.ShopId == model.selectedClinicInfo.get()!!.shopId!!) {
                                // 表示有找到
                                shopMemberId = item.id
                                isFound = true
                                break
                            }
                        }
                        if (!isFound) {
                            // 去註冊店家會員
                            registeredShopMember(response.ohbot_obj.id)
                        }
                    } else {
                        // 沒註冊過任何店家
                        registeredShopMember(response.ohbot_obj.id)
                    }
                } else {
                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun registeredShopMember(ohbotId: String) {
        val requestJsonObject = JsonObject()
        requestJsonObject.addProperty("userId", ohbotId)
        // 2.2.1 建⽴店家會員
        val requestBody = RequestOhBotModel(
            "/shop/${model.selectedClinicInfo.get()!!.shopId!!}/member/registerShopMemberByUser",
            "POST",
            mutableListOf(RequestQuestItem("orgId", model.selectedClinicInfo.get()!!.orgId!!)),
            mutableListOf(),
            requestJsonObject
        )

        GcreateApiClient.getOhBotDataForPost1(requestBody, object : ApiController<ResponseModel1>(requireActivity(), false) {
            override fun onSuccess(response: ResponseModel1) {
                if (response.ohbot_obj != null) {
                    shopMemberId = response.ohbot_obj.id
                } else {
                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onStop() {
        super.onStop()
        serviceItemSelectedPosition = 0
        serviceDoctorSelectedPosition = 0
        serviceAvailableTimeSelectedPosition = 0
        availableTimeSelectedPosition = 9999
        mAdapter.notifyDataSetChanged()
        reservationRelativesSelectedPosition = 0
        isDateTimeSelect = false
    }
}