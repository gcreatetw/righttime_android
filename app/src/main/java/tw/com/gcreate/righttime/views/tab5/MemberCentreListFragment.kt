package tw.com.gcreate.righttime.views.tab5

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.runBlocking
import tw.com.gcreate.righttime.BuildConfig
import tw.com.gcreate.righttime.adapters.tab5.MemberCentre1Adapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentMemberCentreBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.views.login.LoginType


class MemberCentreListFragment : Fragment() {

    private val mAdapter = MemberCentre1Adapter(mutableListOf())
    private val itemList = mutableListOf<String>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentMemberCentreBinding.inflate(inflater, container, false)

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            itemList.clear()

            if (model.appState.gCMemberId.get().equals("")) {
                itemList.addAll(mutableListOf("會員資料查詢", "掛號紀錄", "親友管理", "推薦診所及問題回饋", "隱私權政策"))
            } else {
                itemList.addAll(mutableListOf("會員資料查詢", "掛號紀錄", "親友管理", "推薦診所及問題回饋", "隱私權政策", "登出"))
            }

            mAdapter.data = itemList
            adapter = mAdapter

            mAdapter.setOnItemClickListener { _, _, position ->
                when (position) {
                    0 -> {  //  會員資料
                        if (model.appState.gCMemberId.get().equals("")) {
                            // 登入
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToLoginTypeFragment()
                            action.mode = LoginType.MemberCentre
                            findNavController().navigate(action)
                        } else {
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToMemberCentreFragment2()
                            findNavController().navigate(action)
                        }
                    }

                    1 -> {  //  掛號紀錄
                        if (model.appState.gCMemberId.get().equals("")) {
                            // 登入
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToLoginTypeFragment()
                            action.mode = LoginType.MemberCentre
                            findNavController().navigate(action)
                        } else {
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToReservationClinicListFragment()
                            findNavController().navigate(action)
                        }
                    }

                    2 -> {  //  親友管理
                        if (model.appState.gCMemberId.get().equals("")) {
                            // 登入
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToLoginTypeFragment()
                            action.mode = LoginType.MemberCentre
                            findNavController().navigate(action)
                        } else {
                            val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToRelativesManagementFragment()
                            findNavController().navigate(action)
                        }
                    }

                    3 -> {  //  推薦診所及問題回饋
                        val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToFeedbackTypeFragment()
                        findNavController().navigate(action)
                    }

                    4 -> {  //  隱私權政策
                        val action = MemberCentreListFragmentDirections.actionMemberCentreFragmentToPolicyWebViewFragment()
                        action.linkType = 0
                        findNavController().navigate(action)
                    }

                    5 -> {
                        val builder = AlertDialog.Builder(requireActivity())
                        builder.apply {
                            setTitle("登出")
                            setMessage("請問是否登出Right Time?")
                            setPositiveButton("確定") { _: DialogInterface?, _: Int ->
                                runBlocking {
                                    model.appState.setGCMemberId("")
                                    model.appState.setGCLoginMobile("")
                                    model.appState.setGCLoginPassword("")
                                }
                                itemList.removeAt(5)
                                mAdapter.data = itemList
                                mAdapter.notifyDataSetChanged()
                                RelativesDataMaintain.saveRelativesList(requireContext(), mutableListOf())
                                Toast.makeText(requireActivity(), "登出成功", Toast.LENGTH_SHORT).show()
                            }
                            setNegativeButton("取消") { dialog, _ -> dialog.dismiss() }
                            setCancelable(false)
                            create().show()
                        }
                    }
                }
            }

        }

        val versionName = "APP VERSION = ${BuildConfig.VERSION_NAME}"
        binding.tvVersion.text = versionName

        return binding.root
    }
}