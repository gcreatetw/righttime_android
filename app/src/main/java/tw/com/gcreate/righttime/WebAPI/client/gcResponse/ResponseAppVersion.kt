package tw.com.gcreate.righttime.webAPI.client.gcResponse

data class ResponseAppVersion(
    val version: String,
    val APP_iOS: APPIOS,
    val APP_Android: APPAndroid,
)

data class APPIOS(
    val isOpen: Boolean,
    val message: String,
    val verion: List<Verion>
)

data class APPAndroid(
    val isOpen: Boolean,
    val message: String,
    val verion: MutableList<Verion>
)

data class Verion(
    val timestamp: Int,
    val version: String
)

