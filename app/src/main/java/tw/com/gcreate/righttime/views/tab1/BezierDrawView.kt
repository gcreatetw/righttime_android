package tw.com.gcreate.righttime.views.tab1

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.widget.ImageView
import androidx.core.content.ContextCompat
import tw.com.gcreate.righttime.R

@SuppressLint("AppCompatCustomView")
class BezierDrawView : ImageView {

    private var widthSpecSize = 0
    private var heightSpecSize = 0

    private val p = Paint()
    val path = Path()

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthSpecMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSpecSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSpecMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSpecSize = MeasureSpec.getSize(heightMeasureSpec)
        this.heightSpecSize = heightSpecSize
        this.widthSpecSize = widthSpecSize
        if (widthSpecMode == MeasureSpec.AT_MOST && heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(250, 250)
        } else if (widthSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(250, heightSpecSize)
        } else if (heightSpecMode == MeasureSpec.AT_MOST) {
            setMeasuredDimension(widthSpecSize, 250)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        p.isAntiAlias = true
        p.color = ContextCompat.getColor(context, R.color.colorPrimary)

        val x = widthSpecSize.toFloat()
        val y = heightSpecSize.toFloat()
        path.moveTo(0f, y * 0.2f)
        path.cubicTo(x * 0.35f, y*1.1f, x * 0.8f, y*0.1f , x*1.05f, y)
        path.lineTo(0f, y)
        path.close()
        canvas.drawPath(path, p)
    }
}