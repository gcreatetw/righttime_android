package tw.com.gcreate.righttime.sharedpreferences.savedmodel

import java.text.SimpleDateFormat
import java.util.*

class ObjectNotifyCard2(
    var clinicId: Int,
    var clinicName: String,
    pic: String,
    var roomId: Int,
    var roomName: String,
    var currentDoctor: String,
    var currentNum: Int,
    var bookingNum: Int,
    var notifyNum: Int,
    var bookingTime: String,
    morningS: String,
    morningE: String,
    afternoonS: String,
    afternoonE: String,
    eveningS: String,
    eveningE: String
) {
    var date: String
    val morningS: String
    val morningE: String
    val afternoonS: String
    val afternoonE: String
    val eveningS: String
    val eveningE: String
    var pic: String

    init {
        val sdf = SimpleDateFormat("yyyyMMdd")
        val dt = Date()
        date = sdf.format(dt)
        bookingTime = bookingTime
        this.morningS = morningS
        this.afternoonS = afternoonS
        this.eveningS = eveningS
        this.morningE = morningE
        this.afternoonE = afternoonE
        this.eveningE = eveningE
        this.pic = pic
    }
}