package tw.com.gcreate.righttime.views.tab4

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import tw.com.gcreate.righttime.adapters.tab4.NotifyAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentNotificationBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.util.DateTool
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseNotification
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseNotify


class NotificationFragment : Fragment() {

    private lateinit var binding: FragmentNotificationBinding
    private val latestNewsAdapter = NotifyAdapter(mutableListOf())
    private val oldNewsAdapter = NotifyAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentNotificationBinding.inflate(inflater, container, false)

        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, false)
        binding.toolbar.materialToolbar.navigationIcon = null

        getNotificationData()

        binding.apply {
            rvLatestNews.apply {
                hasFixedSize()
                layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                adapter = latestNewsAdapter
            }

            rvOldNews.apply {
                hasFixedSize()
                layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                adapter = oldNewsAdapter
            }

            refreshLayout.setOnRefreshListener {
                getNotificationData()
                binding.refreshLayout.isRefreshing = false
            }
        }

        return binding.root
    }

    private fun getNotificationData() {
        val userId = model.appState.gCMemberId.get()!!

        if (userId.isNotEmpty()) {
            GcreateApiClient.getNotifications(userId, object : ApiController<ResponseNotification>(requireActivity(), false) {
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(response: ResponseNotification) {
                    if (response.success) {
                        if (response.notify_list.size != 0) {
                            binding.refreshLayout.visibility = View.VISIBLE
                            binding.lyNoData.visibility = View.GONE

                            val latestData = mutableListOf<ResponseNotify>()
                            val oldData = mutableListOf<ResponseNotify>()

                            for (item in response.notify_list) {
                                if (item.created_at.contains(DateTool.getDate())) {
                                    latestData.add(ResponseNotify(item.title, item.message, item.created_at))
                                } else {
                                    oldData.add(ResponseNotify(item.title, item.message, item.created_at))
                                }
                            }
                            latestNewsAdapter.data = latestData
                            oldNewsAdapter.data = oldData
                            latestNewsAdapter.notifyDataSetChanged()
                            oldNewsAdapter.notifyDataSetChanged()
                        } else {
                            binding.refreshLayout.visibility = View.GONE
                            binding.lyNoData.visibility = View.VISIBLE
                        }
                    }
                }
            })
        }
    }

}