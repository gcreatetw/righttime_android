package tw.com.gcreate.righttime.webAPI

import android.app.Application
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiErrorManager
import tw.com.gcreate.righttime.webAPI.server.ApiClient

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        apiErrorManager = ApiErrorManager(applicationContext)
        ApiClient.initApi(applicationContext)
    }

    companion object {
        lateinit var apiErrorManager: ApiErrorManager
    }
}