package tw.com.gcreate.righttime.views.reservation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import tw.com.gcreate.righttime.databinding.FragmentClinicInfoReservationBinding
import tw.com.gcreate.righttime.util.SomeFunc

class ClinicInfoFragmentReservation : Fragment() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var navController: NavController? = null

        var vaccineCurrentPage = "ClinicInfoFragmentVaccine"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentClinicInfoReservationBinding.inflate(inflater, container, false)

        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "列表", binding.toolbar.toolbarLogo,true)
        navController = findNavController()

        return binding.root
    }

}