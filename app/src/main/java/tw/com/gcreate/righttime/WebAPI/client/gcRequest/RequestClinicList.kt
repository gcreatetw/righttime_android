package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestClinicList(
    val city_id: String,
    val type_id: String,
    val clinic_id_list: List<Int>,
    val center: Center?,
)