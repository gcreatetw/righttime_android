package tw.com.gcreate.righttime.adapters.tab1

import androidx.navigation.NavController
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.CardClinictypeBinding
import tw.com.gcreate.righttime.views.tab1.ClinicTypeFragmentDirections
import tw.com.gcreate.righttime.webAPI.client.ClinicTypeDataBean

/** 首頁-選擇科別 Adapter
 * */
class ClinicTypeAdapter(private val navController: NavController, dataList: MutableList<ClinicTypeDataBean>) :
    BaseQuickAdapter<ClinicTypeDataBean, DataBindBaseViewHolder>(R.layout.card_clinictype, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: ClinicTypeDataBean) {
        val binding: CardClinictypeBinding = holder.getViewBinding() as CardClinictypeBinding
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.clinicImageResource).into(binding.clinicTypePicture)
        holder.setText(R.id.clinicTypeName, item.clinicTypeName)
        holder.setText(R.id.clinicTypeSubname, item.clinicTypeDescription)

        holder.itemView.setOnClickListener {
            val action = ClinicTypeFragmentDirections.actionClinicTypeFragmentToClinicListFragment()
            action.clinicTypeID = item.clinicId.toInt()
            action.userSelectClinicType = item.clinicTypeName
            navController.navigate(action)
        }
    }

}