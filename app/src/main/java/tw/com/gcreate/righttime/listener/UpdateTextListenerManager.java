package tw.com.gcreate.righttime.listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class UpdateTextListenerManager {
    /**
     * 單例模式
     */
    public static UpdateTextListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private final List<UpdateTextListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static UpdateTextListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new UpdateTextListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(UpdateTextListener iListener) {
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(UpdateTextListener iListener) {
        iListenerList.remove(iListener);
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(boolean isSelectCity,String text) {
        for (UpdateTextListener updateNameListener : iListenerList) {
            updateNameListener.updateWheelPicker(isSelectCity,text);

        }
    }


}

