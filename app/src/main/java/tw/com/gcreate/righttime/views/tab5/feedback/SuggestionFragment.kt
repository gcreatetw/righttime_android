package tw.com.gcreate.righttime.views.tab5.feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.databinding.FragmentSuggestionBinding
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestFeedback
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat


class SuggestionFragment : Fragment() {

    private lateinit var binding: FragmentSuggestionBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSuggestionBinding.inflate(inflater, container, false)

        binding.apply {
            edSuggestUserName.setOnEditorActionListener { _, _, _ ->
                binding.edSuggestUserName.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            edSuggestUserMail.setOnEditorActionListener { _, _, _ ->
                binding.edSuggestUserMail.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            edSuggestUserComment.editText!!.setOnEditorActionListener { _, _, _ ->
                binding.edSuggestUserComment.clearFocus()
                SomeFunc.hideSoftKeyBoard(requireActivity(), binding.root)
                true
            }

            tvCheck.setOnClickListener {
                sendFeedback()
            }

        }

        return binding.root
    }

    private fun sendFeedback() {

        if (!binding.edSuggestUserName.text.isNullOrEmpty() && !binding.edSuggestUserMail.text.isNullOrEmpty() && !binding.edSuggestUserComment.editText!!.text.isNullOrEmpty()) {
            val requestBody = RequestFeedback(
                binding.edSuggestUserName.text.toString(),
                binding.edSuggestUserMail.text.toString(),
                "suggest",
                binding.edSuggestUserComment.editText!!.text.toString())

            GcreateApiClient.sendFeedBack(requestBody, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (response.success) {
                        Toast.makeText(context, resources.getText(R.string.text_feedback_success), Toast.LENGTH_LONG).show()
                        binding.edSuggestUserName.text.clear()
                        binding.edSuggestUserMail.text.clear()
                        binding.edSuggestUserComment.editText!!.text.clear()
                    } else {
                        Toast.makeText(context, resources.getText(R.string.text_feedback_fail), Toast.LENGTH_LONG).show()
                    }
                }
            })
        } else {
            Toast.makeText(context, resources.getText(R.string.text_feedback_none), Toast.LENGTH_LONG).show()
        }
    }
}