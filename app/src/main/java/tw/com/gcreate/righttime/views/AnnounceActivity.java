package tw.com.gcreate.righttime.views;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import tw.com.gcreate.righttime.R;

public class AnnounceActivity extends AppCompatActivity {

    TextView announceMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announce);
        announceMessage = findViewById(R.id.tv_announceMessage);
        announceMessage.setText(getIntent().getStringExtra("message"));
    }
}
