package tw.com.gcreate.righttime.webAPI.client


import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*
import tw.com.gcreate.righttime.webAPI.client.gcRequest.*
import tw.com.gcreate.righttime.webAPI.client.gcResponse.*


interface GcreateApi {

    // 登入
    @POST("/api/member/login")
    fun gcLogin(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestGcLogin,
    ): Call<ResponseGcLogin>

    // 註冊
    @POST("/api/member/register")
    fun gcRegistered(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestGcRegistered,
    ): Call<ResponseSimpleFormat>

    // 判斷電話有無註冊
    @POST("/api/member/check")
    fun gcWhetherRegistered(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: JsonObject,
    ): Call<ResponseGcIsRegistered>

    // 會員資料修改
    @POST("/api/member/edit")
    fun gcUserInfoModify(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestUserInfoModify,
    ): Call<ResponseSimpleFormat>

    // App version
    @GET("/api/version")
    fun getAppVersion(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ResponseAppVersion>

    // 全臺縣市
    @GET("/api/city/list")
    fun getCityList(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ResponseCityList>

    // Banner
    @GET("/api/appFile/banner/list")
    fun getBannerList(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ObjectBannerList>

    // Clinic Banner
    @GET("/api/appFile/clinicBanner/list")
    fun getClinicBannerList(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ObjectBannerList>

    // 推薦 & 問題回饋
    @POST("/api/feedBack/add")
    fun sendFeedBack(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestFeedback,
    ): Call<ResponseSimpleFormat>


    /* 診所相關 */
    // 診所科別
    @GET("/api/clinicType/list")
    fun getClinicType(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ResponseClinicTypeList>

    // 診所列表
    @POST("/api/clinic/list")
    fun getClinicList(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestClinicList,
    ): Call<ResponseClinicList>

    // 單一診所資訊
    @GET("/api/clinic/{id}/info")
    fun getClinicInfo(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "id", encoded = true) clinicId: Int,
    ): Call<ResponseClinicInfo>

    // 診所搜尋
    @POST("/api/clinic/search")
    fun search(@HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestSearch,
    ): Call<ResponseClinicList>

    // 取得診所的診間號碼
    @POST("/api/clinicRoom")
    fun getClinicRoomNumber(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestClinicRoomNumber,
    ): Call<MutableList<ObjectClinicRoomNumber>>

    // 通知提醒 & 取消提醒
    @POST("/api/setDeviceNotify")
    fun setDeviceNotify(
        @HeaderMap headers: HashMap<String, String>,
        @Body requestBody: RequestDeviceNotify,
    ): Call<ResponseDeviceNotify>

    /* Tab2 **/
    // HeHO 分類
    @GET("/api/articleType/list")
    fun getHehoCategory(
        @HeaderMap headers: HashMap<String, String>,
    ): Call<ResponseHeHoCategory>


    @GET("/api/article/list")
    fun getHeHoArticle(
        @HeaderMap headers: HashMap<String, String>,
        @Query(value = "type_id", encoded = true) type_id: Int,
    ): Call<ResponseHeHoArticle>

    /* Tab4 推播 **/
    @GET("/api/member/notify/{gcMemberId}")
    fun getNotifications(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "gcMemberId", encoded = true) gcMemberId: String,
    ): Call<ResponseNotification>

    /* 因 facebook API 權限更新，要不到資料。要再使用的話要再研究
    @Headers({"Content-Type: application/json"})
    @GET("{FBID}/posts?fields=full_picture,message,created_time&access_token=EAAS8LGISx9wBAAA63SGCPvaMpdErnzXVncg2LcPm8UWKK2k2sPijKMztPI3RffHtTo4JoSVZBL1It0dpZAIusIymg4l7tEaTfAmjOBVZCgGSoXNFOta9DGnXvwSYmgZAVesghtnoDZCDRIfIZCIDYYI6JPlWZBdCg5bE8w5oPWzagZDZD")
    Call<ObjectFBLatestNews> GetClinicFBLatestNews(@Path("FBID") String FBID);

     * */

    /* GC x Ohbot */
    @GET("/api/OhBot/trans_get")
    fun getOhBotDataForGet1(
        @Query(value = "trans_path", encoded = true) trans_path: String,
        @Query(value = "orgId", encoded = true) orgId: String,
    ): Call<ResponseModel1>

    @GET("/api/OhBot/trans_get")
    fun getOhBotDataForGet2(
        @Query(value = "trans_path", encoded = true) trans_path: String,
        @Query(value = "orgId", encoded = true) orgId: String,
    ): Call<ResponseModel2>

    @POST("/api/OhBot/trans")
    fun getOhBotDataForPost1(
        @Body requestBody: RequestOhBotModel,
    ): Call<ResponseModel1>

    @POST("/api/OhBot/trans")
    fun getOhBotDataForPost2(
        @Body requestBody: RequestOhBotModel,
    ): Call<ResponseModel2>
}