package tw.com.gcreate.righttime.adapters.tab1

import android.annotation.SuppressLint
import android.view.View
import android.widget.Toast
import androidx.navigation.NavController
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.google.android.material.snackbar.Snackbar
import retrofit2.Response
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.ClinicNumberNotifyCardPrivBinding
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.sharedpreferences.TodayNotifyCardDataMaintain
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.views.tab1.ClinicTypeFragmentDirections
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestClinicList
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestDeviceNotify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseDeviceNotify

class ClinicNumberNotifyAdapterPriv(
    private val navController: NavController,
    private var notifyCardList: MutableList<ObjectNotifyCard2>,
    var v: View,
    val model: MainViewModel,
) : BaseQuickAdapter<ObjectNotifyCard2, DataBindBaseViewHolder>(R.layout.clinic_number_notify_card_priv, notifyCardList) {

    private var deletedNotifyCard: ObjectNotifyCard2? = null
    private var deletedNotifyCardPosition = 0

    override fun convert(holder: DataBindBaseViewHolder, item: ObjectNotifyCard2) {
        val binding: ClinicNumberNotifyCardPrivBinding = holder.getViewBinding() as ClinicNumberNotifyCardPrivBinding
        binding.executePendingBindings()

        Glide.with(context).load(item.pic).error(R.mipmap.page_icon).into(binding.imageClinic)

        holder.setText(R.id.textClinicName, item.clinicName)
        holder.setText(R.id.textClinicRoom, item.roomName)
        holder.setText(R.id.textClinicBookingNumber, item.bookingNum.toString())

        when (item.bookingTime) {
            "M" -> holder.setText(R.id.textClinicTime, dateToApiFormat(item.date, "上午診"))
            "A" -> holder.setText(R.id.textClinicTime, dateToApiFormat(item.date, "下午診"))
            else -> holder.setText(R.id.textClinicTime, dateToApiFormat(item.date, "晚上診"))
        }

        binding.imageViewDel.setOnClickListener {
            deleteItem(holder.adapterPosition)
        }

        holder.itemView.setOnClickListener {
            val requestBody = RequestClinicList("", "", arrayListOf(item.clinicId), null)
            GcreateApiClient.getClinicList(requestBody, object : ApiController<ResponseClinicList>(context, false) {
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(response: ResponseClinicList) {
                    model.selectedClinicSimple.set(response[0])
                    val action = ClinicTypeFragmentDirections.actionClinicTypeFragmentToClinicInfoFragment()
                    action.clinicId = item.clinicId
                    action.isComeFormQR = false
                    navController.navigate(action)
                }

                override fun onFail(httpResponse: Response<ResponseClinicList>): Boolean {
                    Toast.makeText(context, "資料更新失敗，請檢查網路狀態", Toast.LENGTH_LONG).show()
                    return super.onFail(httpResponse)
                }
            })
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    fun refresh(list: MutableList<ObjectNotifyCard2>) {
        data = list
        notifyDataSetChanged()
    }

    private fun deleteItem(position: Int) {
        deletedNotifyCard = notifyCardList[position]
        deletedNotifyCardPosition = position
        notifyCardList.removeAt(position)

        TodayNotifyCardDataMaintain.removeFromNotifyList(context,
            deletedNotifyCard!!.roomId,
            deletedNotifyCard!!.date,
            deletedNotifyCard!!.bookingTime)
        refresh(notifyCardList)
        showUndoSnackBar()
    }

    private fun showUndoSnackBar() {
        val view = v.findViewById<View>(R.id.clinic_notify_number)
        val snackbar = Snackbar.make(view, R.string.text_snack_bar, Snackbar.LENGTH_LONG)
        snackbar.setAction(R.string.snack_bar_undo) { undeleteItem() }
        snackbar.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(snackbar: Snackbar, event: Int) {
                if (event == DISMISS_EVENT_TIMEOUT) {

                    val requestBody = RequestDeviceNotify(deletedNotifyCard!!.roomId,
                        global.FireBasToken,
                        "",
                        "",
                        deletedNotifyCard!!.bookingTime,
                        global.dateToApiFormat(deletedNotifyCard!!.date))

                    GcreateApiClient.setDeviceNotify(global.UUID, requestBody, object : ApiController<ResponseDeviceNotify>(context, false) {
                        override fun onSuccess(response: ResponseDeviceNotify) {
                            if (response.success) {
                                Toast.makeText(context, context.resources.getText(R.string.text_notify_del_success), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(context, context.resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                            }
                        }

                        override fun onFail(httpResponse: Response<ResponseDeviceNotify>): Boolean {
                            Toast.makeText(context, context.resources.getText(R.string.text_notify_fail), Toast.LENGTH_LONG).show()
                            return super.onFail(httpResponse)
                        }
                    })
                }
            }

            override fun onShown(snackbar: Snackbar) {}
        })
        snackbar.show()
    }

    private fun undeleteItem() {
        notifyCardList.add(deletedNotifyCardPosition, deletedNotifyCard!!)
        TodayNotifyCardDataMaintain.addToNotifyList(context, deletedNotifyCard!!)
        refresh(notifyCardList)
    }

    private fun dateToApiFormat(dateString: String, timeInterval: String): String {
        return dateString.substring(0, 4) + "-" + dateString.substring(4, 6) + "-" + dateString.substring(6, 8) + " " + timeInterval
    }

}