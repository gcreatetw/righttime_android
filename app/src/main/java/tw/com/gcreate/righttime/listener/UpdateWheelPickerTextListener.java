package tw.com.gcreate.righttime.listener;

public interface UpdateWheelPickerTextListener {
    void updateWheelPicker(int mode,String textString);
}
