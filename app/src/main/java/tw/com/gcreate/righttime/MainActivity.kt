package tw.com.gcreate.righttime


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import retrofit2.Response
import tw.com.gcreate.righttime.databinding.ActivityMainBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.views.dialog.UpgradeDialogFragment
import tw.com.gcreate.righttime.views.tab1.ClinicTypeFragment
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseAppVersion
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseCityList
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseClinicTypeList


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "appState")

class MainActivity : AppCompatActivity(), ClinicTypeFragment.OnFragmentInteractionListener {

    private lateinit var binding: ActivityMainBinding

    private var isFirstTime = true

    private val model: MainViewModel by viewModels {
        MainViewModelFactory(this.dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        global.context = this
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val navController = Navigation.findNavController(this, R.id.frameContainer)

        this.window.setFlags(-0x80000000, -0x80000000)

        global.initloadDB()
        //global.initFBAPI()
        initialCallApi()

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
//            initAdlocus()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 100);
        }

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_LOCATION)

        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)

    }

    private fun initialCallApi() {
        /* 取得全台縣市 & 診所科別 */
        GcreateApiClient.getCityList(object : ApiController<ResponseCityList>(this@MainActivity, false) {
            override fun onSuccess(response: ResponseCityList) {
                model.cityList.addAll(response)
            }
        })

        GcreateApiClient.getClinicType(object : ApiController<ResponseClinicTypeList>(this@MainActivity, false) {
            override fun onSuccess(response: ResponseClinicTypeList) {
                model.clinicTypeList.addAll(response)
            }
        })
    }

    override fun onFragmentInteraction(uri: Uri?) {}

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager

        val count = fragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
        } else {
            fragmentManager.popBackStack()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    global.getCurrentPosition(this)
                } else {
                    //finish();
                }
//                initAdlocus()
                return
            }
        }
    }

    private fun checkVersionDialog(isFirstTime: Boolean) {
        val mConnectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val mNetworkInfo = mConnectivityManager.activeNetworkInfo

        if (mNetworkInfo != null) {
            val apiVersionList: MutableList<String> = ArrayList()
            GcreateApiClient.getAppVersion(object : ApiController<ResponseAppVersion>(this@MainActivity, false) {
                override fun onSuccess(response: ResponseAppVersion) {

                    for (item in response.APP_Android.verion) {
                        apiVersionList.add(item.version)
                    }

                    try {
                        val currentVersion = packageManager.getPackageInfo(packageName, 0).versionName
                        if (!isFirstTime) {
                            if (!apiVersionList.contains(currentVersion)) {
                                val newFragment = UpgradeDialogFragment()
                                newFragment.show(supportFragmentManager, "upgrade_dialog")
                            }
                        }
                        /*   else {
                            if (!currentVersion.equals(latestVersion))
                            {
                                FragmentTransaction ft = ((FragmentActivity) global.context).getSupportFragmentManager().beginTransaction();
                                Fragment prev = ((FragmentActivity) global.context).getSupportFragmentManager().findFragmentByTag("dialog");
                                if (prev != null) {
                                    ft.remove(prev);
                                }
                                DialogFragment newFragment = new UpgradeDialogFragment();
                                newFragment.show(ft, "dialog");
                            }
                        }
                    }*/
                    } catch (e: PackageManager.NameNotFoundException) {
                        e.printStackTrace()
                    }
                }

                override fun onFail(httpResponse: Response<ResponseAppVersion>): Boolean {
                    Toast.makeText(this@MainActivity, resources.getText(R.string.text_rest_api_fail), Toast.LENGTH_SHORT).show()
                    return super.onFail(httpResponse)
                }
            })

        }
    }

    public override fun onResume() {
        super.onResume()
        checkVersionDialog(isFirstTime)
        isFirstTime = false
    }

    companion object {
        private const val MY_PERMISSIONS_LOCATION = 100
    }


    //Init new AdLocus SDK
//    private fun initAdlocus() {
//        FirebaseApp.initializeApp(this);
//        var token = ""
//        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
//            if (!task.isSuccessful) return@OnCompleteListener
//            token = task.result
//        })
//
//        if (DEBUG) {
//            AdLocus.getInstance(this).checkUserStatement(token,
//                getString(R.string.fcmAppKey),
//                packageName,
//                getString(R.string.appKey))
//
//        } else {
//            AdLocus.getInstance(this).checkUserStatement(token, "pass", packageName, "b36e34405ad649776e82156be7f1a4a5ec6b44ae")
//        }
//    }

}