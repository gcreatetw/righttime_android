package tw.com.gcreate.righttime.util.indicator

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

//import android.graphics.Interpolator;
/*
    診所介紹上方圖片指示器 ( 沒輪播用的)
* */
class LinePagerIndicatorDecoration : ItemDecoration() {
    private val colorActive = -0x38fb
    private val colorInactive = -0x1
    private var lastActivePosition = 0

    /**
     * Height of the space the indicator takes up at the bottom of the view.
     */
    private val mIndicatorHeight = (DP * 16).toInt()

    /**
     * Indicator stroke width.
     */
    private val mIndicatorStrokeWidth = DP * 2

    /**
     * Inactive Indicator radiius.
     */
    private val itemRadius = DP * 4

    /**
     * Indicator width.
     */
    private val mIndicatorItemLength = DP * 16

    /**
     * Padding between indicators.
     */
    private val mIndicatorItemPadding = DP * 8

    /**
     * Some more natural animation interpolation
     */
    private val mInterpolator: Interpolator = AccelerateDecelerateInterpolator()
    private val mPaint = Paint()

    // add Circle Interpolator
    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)
        val itemCount = parent.adapter!!.itemCount
        if (itemCount > 1) {
            val totalLength = itemRadius * 2 * itemCount
            val paddingBetweenItems = Math.max(0, itemCount - 1) * mIndicatorItemPadding
            val indicatorTotalWidth = totalLength + paddingBetweenItems
            val indicatorStartX = (parent.width - indicatorTotalWidth) / 2f

            // center vertically in the allotted space
            val indicatorPosY = (parent.height - parent.height * 10 / 24).toFloat()
            drawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount)


            // find active page (which should be highlighted)
            val layoutManager = parent.layoutManager as LinearLayoutManager?
            val activePosition = layoutManager!!.findFirstVisibleItemPosition()
            if (activePosition == RecyclerView.NO_POSITION) {
                return
            }

            // find offset of active page (if the user is scrolling)
            val activeChild = layoutManager.findViewByPosition(activePosition)
            val left = activeChild!!.left
            val width = activeChild.width

            // on swipe the active item will be positioned from [-width, 0]
            // interpolate offset for smooth animation
            val progress = mInterpolator.getInterpolation(left * -1 / width.toFloat())
            drawHighlights(c, indicatorStartX, indicatorPosY, activePosition, progress)
        }
        // center horizontally, calculate width and subtract half from center
    }

    private fun drawInactiveIndicators(c: Canvas, indicatorStartX: Float, cenY: Float, itemCount: Int) {
        mPaint.color = colorInactive

        // width of item indicator including padding
        val itemWidth = itemRadius * 2 + mIndicatorItemPadding
        var cenX = indicatorStartX + itemRadius
        for (i in 0 until itemCount) {
            // draw the circle for every item
            c.drawCircle(cenX, cenY, itemRadius, mPaint)
            cenX += itemWidth
        }
    }

    private fun drawHighlights(c: Canvas, indicatorStartX: Float, indicatorPosY: Float, highlightPosition: Int, progress: Float) {
        // width of item indicator including padding
        var highlightPosition = highlightPosition
        val itemWidth = itemRadius * 2 + mIndicatorItemPadding
        if (progress == 0f) {
            // no swipe, draw a normal indicator
            mPaint.color = colorActive
            lastActivePosition = highlightPosition
        } else {
            if (lastActivePosition > highlightPosition) {
                highlightPosition = lastActivePosition
            }
            mPaint.color = colorActive
        }
        val highlightStart = indicatorStartX + itemWidth * highlightPosition
        c.drawCircle(highlightStart + itemRadius, indicatorPosY, itemRadius, mPaint)
    }
    //底線
    /**底線
     * //    @Override
     * //    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
     * //        super.onDrawOver(c, parent, state);
     * //
     * //        int itemCount = parent.getAdapter().getItemCount();
     * //
     * //        // center horizontally, calculate width and subtract half from center
     * //        float totalLength = mIndicatorItemLength * itemCount;
     * //        float paddingBetweenItems = Math.max(0, itemCount - 1) * mIndicatorItemPadding;
     * //        float indicatorTotalWidth = totalLength + paddingBetweenItems;
     * //        float indicatorStartX = (parent.getWidth() - indicatorTotalWidth) / 2F;
     * //
     * //        // center vertically in the allotted space
     * //        float indicatorPosY = parent.getHeight() -  ( (parent.getHeight()) * 11 /24)  ;
     * //
     * //            drawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount);
     * //
     * //
     * //        // find active page (which should be highlighted)
     * //        LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
     * //        int activePosition = layoutManager.findFirstVisibleItemPosition();
     * //        if (activePosition == RecyclerView.NO_POSITION) {
     * //            return;
     * //        }
     * //
     * //        // find offset of active page (if the user is scrolling)
     * //        final View activeChild = layoutManager.findViewByPosition(activePosition);
     * //        int left = activeChild.getLeft();
     * //        int width = activeChild.getWidth();
     * //
     * //        // on swipe the active item will be positioned from [-width, 0]
     * //        // interpolate offset for smooth animation
     * //        float progress = mInterpolator.getInterpolation(left * -1 / (float) width);
     * //
     * //        drawHighlights(c, indicatorStartX, indicatorPosY, activePosition, progress, itemCount);
     * //    }
     */
    /**底線
     * //    private void drawInactiveIndicators(Canvas c, float indicatorStartX, float indicatorPosY, int itemCount) {
     * //        mPaint.setColor(colorInactive);
     * //
     * //        // width of item indicator including padding
     * //        final float itemWidth = mIndicatorItemLength + mIndicatorItemPadding;
     * //
     * //        float start = indicatorStartX;
     * //        for (int i = 0; i < itemCount; i++) {
     * //            // draw the line for every item
     * //            c.drawLine(start, indicatorPosY, start + mIndicatorItemLength, indicatorPosY, mPaint);
     * //            c.drawCircle(100,100,90,mPaint);
     * //
     * //            start += itemWidth;
     * //        }
     * //    }
     */
    /**
     * 底線
     * private void drawHighlights(Canvas c, float indicatorStartX, float indicatorPosY,
     * int highlightPosition, float progress, int itemCount) {
     * mPaint.setColor(colorActive);
     *
     *
     * // width of item indicator including padding
     * final float itemWidth = mIndicatorItemLength + mIndicatorItemPadding;
     *
     *
     * if (progress == 0F) {
     * // no swipe, draw a normal indicator
     * float highlightStart = indicatorStartX + itemWidth * highlightPosition;
     * c.drawLine(highlightStart, indicatorPosY,
     * highlightStart + mIndicatorItemLength, indicatorPosY, mPaint);
     * } else {
     * float highlightStart = indicatorStartX + itemWidth * highlightPosition;
     * // calculate partial highlight
     * float partialLength = mIndicatorItemLength * progress;
     *
     *
     * // draw the cut off highlight
     * c.drawLine(highlightStart + partialLength, indicatorPosY,
     * highlightStart + mIndicatorItemLength, indicatorPosY, mPaint);
     *
     *
     * // draw the highlight overlapping to the next item as well
     * if (highlightPosition < itemCount - 1) {
     * highlightStart += itemWidth;
     * c.drawLine(highlightStart, indicatorPosY,
     * highlightStart + partialLength, indicatorPosY, mPaint);
     * }
     * }
     * }
     */
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.bottom = mIndicatorHeight
    }

    companion object {
        private val DP = Resources.getSystem().displayMetrics.density
    }

    init {
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.strokeWidth = mIndicatorStrokeWidth
        /* 底線
              mPaint.setStyle(Paint.Style.STROKE);
             */mPaint.style = Paint.Style.FILL
        mPaint.isAntiAlias = true
    }
}