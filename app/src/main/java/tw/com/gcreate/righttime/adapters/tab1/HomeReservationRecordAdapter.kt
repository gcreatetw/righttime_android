package tw.com.gcreate.righttime.adapters.tab1


import android.annotation.SuppressLint
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.google.android.material.shape.CornerFamily
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.FragmentClinicTypeBinding
import tw.com.gcreate.righttime.databinding.RvItemHomeReservationRecordBinding
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ReservationRecordModel


class HomeReservationRecordAdapter(data: MutableList<ReservationRecordModel>) :
    BaseQuickAdapter<ReservationRecordModel, DataBindBaseViewHolder>(R.layout.rv_item_home_reservation_record, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: ReservationRecordModel) {

        val binding = holder.getViewBinding() as RvItemHomeReservationRecordBinding
        binding.cvReservation.shapeAppearanceModel = binding.cvReservation.shapeAppearanceModel
            .toBuilder()
            .setTopLeftCorner(CornerFamily.ROUNDED, 0.0f)
            .setTopRightCorner(CornerFamily.ROUNDED, 16.0f)
            .setBottomLeftCorner(CornerFamily.ROUNDED, 0.0f)
            .setBottomRightCorner(CornerFamily.ROUNDED, 16.0f)
            .build()

        holder.setText(R.id.tv_reservation_clinic_name, item.getReservationClinicName())
        holder.setText(R.id.tv_reservation_date, item.getReservationServiceTime().substring(0, 10))
        holder.setText(R.id.tv_reservation_time, item.getReservationServiceTime().substring(10))
    }

    @SuppressLint("NotifyDataSetChanged")
    fun refreshDate(binding: FragmentClinicTypeBinding, newData: MutableList<ReservationRecordModel>) {
        if (newData.isEmpty()) {
            binding.clinicReservationRecordLayout.visibility = View.GONE
        } else {
            binding.clinicReservationRecordLayout.visibility = View.VISIBLE
        }
        data = newData
        notifyDataSetChanged()
    }

}
