package tw.com.gcreate.righttime.adapters.tab3

import android.view.View
import android.widget.LinearLayout
import androidx.navigation.NavController
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.google.android.material.snackbar.Snackbar
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.customViewHolder.DataBindBaseViewHolder
import tw.com.gcreate.righttime.databinding.ClinicNameCardBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.sharedpreferences.FavClinicListDataMaintain
import tw.com.gcreate.righttime.views.tab3.FavListFragmentDirections
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ClinicListItem
import java.util.Collections.sort

class FavClinicListAdapter(
    private val navController: NavController,
    private var favClinicList: MutableList<ClinicListItem>,
    var v: View,
    val model: MainViewModel,
) : BaseQuickAdapter<ClinicListItem, DataBindBaseViewHolder>(R.layout.clinic_name_card, favClinicList) {

    private var deletedNameCard: ClinicListItem? = null
    private var deletedNameCardPosition = 0

    init {
        sort(favClinicList)
    }

    fun refresh(clinicNameCards: MutableList<ClinicListItem>) {
        sort(clinicNameCards)
        data = clinicNameCards
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {

        deletedNameCard = data[position]
        deletedNameCardPosition = position
        notifyItemRemoved(position)

        val favList = FavClinicListDataMaintain.getFavClinicDataList(context)

        for (i in favList.indices) {
            if (favList[i].id == data[position].id) {
                favClinicList.removeAt(position)
                FavClinicListDataMaintain.removeFromFavListItem(context, i)
                break
            }
        }
        refresh(FavClinicListDataMaintain.getFavClinicDataList(context))
        showUndoSnackBar()
    }

    private fun showUndoSnackBar() {
        val view = v!!.findViewById<View>(R.id.refresh_layout)
        val snackBar = Snackbar.make(view, R.string.text_snack_bar, Snackbar.LENGTH_LONG)
        snackBar.setAction(R.string.snack_bar_undo) { unDeleteItem() }
        snackBar.show()
    }

    private fun unDeleteItem() {
        favClinicList.add(deletedNameCard!!)
        FavClinicListDataMaintain.addFavClinicListItem(context, deletedNameCard!!)
        refresh(FavClinicListDataMaintain.getFavClinicDataList(context))
    }


    override fun convert(holder: DataBindBaseViewHolder, item: ClinicListItem) {
        val binding: ClinicNameCardBinding = holder.getViewBinding() as ClinicNameCardBinding
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.pic).into(binding.imageClinic)

        holder.setText(R.id.textClinicName, item.name)
        holder.setText(R.id.textClinicAddr, String.format(holder.itemView.context.resources.getString(R.string.text_address), item.address))

        val phoneText = String.format(holder.itemView.context.resources.getString(R.string.text_phone), item.contact, " ", item.extension)
        holder.setText(R.id.textClinicPhone, phoneText)

//        val distanceText = String.format(holder.itemView.context.resources.getString(R.string.text_distance), item.center_len.toString())
//        holder.setText(R.id.textClinicDistance, distanceText)

        if (item.open) {
            holder.itemView.findViewById<LinearLayout>(R.id.maskLinearLayout).visibility = View.INVISIBLE
        } else {
            holder.itemView.findViewById<LinearLayout>(R.id.maskLinearLayout).visibility = View.VISIBLE
        }

        holder.itemView.setOnClickListener {
            model.selectedClinicSimple.set(item)
            val action = FavListFragmentDirections.actionFavListFragmentToClinicInfoFragment()
            action.isComeFormQR = false
            action.clinicId = item.id
            navController.navigate(action)
        }
    }
}