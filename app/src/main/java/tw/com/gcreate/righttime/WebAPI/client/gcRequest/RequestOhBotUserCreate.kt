package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestOhBotUserCreate(
    val origin: String,
    val originId: String,
    val phone: String,
    val name: String,
    val birthday: String,
    val gender: String,
)