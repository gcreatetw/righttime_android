package tw.com.gcreate.righttime.sharedpreferences.savedmodel

class ReservationRecordModel(
    private var reservationClinicName: String,
    private var reservationServiceId: String,
    private var reservationServiceDate: String,
    private var reservationServiceTime: String,
) {
    fun getReservationClinicName(): String {
        return reservationClinicName
    }

    fun getReservationServiceId(): String {
        return reservationServiceId
    }

    fun getReservationServiceDate(): String {
        return reservationServiceDate
    }

    fun getReservationServiceTime(): String {
        return reservationServiceTime
    }

}