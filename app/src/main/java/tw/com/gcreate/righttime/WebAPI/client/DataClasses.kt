package tw.com.gcreate.righttime.webAPI.client

class RegisteredInputData(
    val title: String,
    val hintText: String,
)

class ClinicServiceInfo(
    val serviceName:String,
    val serviceId:String,
    val serviceDoctorNameList:MutableList<String>,
    val serviceDoctorIdList:MutableList<String>,
)

class ClinicReservationDataList(
    val availableTime:String,
    val availableTimeList:MutableList<String>,
)

class ClinicTypeDataBean(
    val clinicId:String,
    val clinicImageResource:Int,
    val clinicTypeName:String,
    val clinicTypeDescription:String,
)