package tw.com.gcreate.righttime.sharedpreferences


import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.sharedpreferences.savedmodel.ObjectNotifyCard2
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ObjectClinicRoomNumber
import java.text.SimpleDateFormat
import java.util.*

object TodayNotifyCardDataMaintain {

    private var notifyCardList: MutableList<ObjectNotifyCard2> = mutableListOf()

    fun getNotifyCardList(context: Context): MutableList<ObjectNotifyCard2> {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val responseString = sp.getString("NotifyCardList2", null)

        updateTime()

        val gson = Gson()
        if (!responseString.isNullOrEmpty()) {
            notifyCardList = gson.fromJson(responseString, object : TypeToken<MutableList<ObjectNotifyCard2>>() {}.type)
        }

        return notifyCardList
    }

    fun addToNotifyList(context: Context, notifyCard: ObjectNotifyCard2) {
        getNotifyCardList(context)
        updateTime()
        for (i in notifyCardList.indices) {
            if (notifyCardList[i].roomId == notifyCard.roomId && notifyCardList[i].date.compareTo(notifyCard.date) == 0) {
                notifyCardList.removeAt(i)
                notifyCardList.add(i, notifyCard)
                saveNotifyList(context, notifyCardList)
                return
            }
        }
        notifyCardList.add(notifyCard)
        saveNotifyList(context, notifyCardList)
    }

    fun removeFromNotifyList(context: Context, roomId: Int, date: String?, time: String?) {
        updateTime()
        for (item in notifyCardList) {
            if (item.roomId == roomId && item.date.compareTo(date!!) == 0 && item.bookingTime.compareTo(time!!) == 0) {
                notifyCardList.remove(item)
                saveNotifyList(context, notifyCardList)
                return
            }
        }
    }

    private fun saveNotifyList(context: Context, notifyCardList: MutableList<ObjectNotifyCard2>) {
        updateTime()
        sortNotifyList()

        var i = 0
        while (i < notifyCardList.size) {

            //expired
            if (notifyCardList[i].date.toInt() - global.date.toInt() < 0) {
                notifyCardList.removeAt(i)
                i--
            }
            i++
        }
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("NotifyCardList2", Gson().toJson(notifyCardList))
        editor.apply()

        getNotifyCardList(context)
    }

    private fun sortNotifyList() {
        for (i in notifyCardList.indices) {
            var j = i
            while (j > 0 && notifyCardList[j - 1].date > notifyCardList[j].date) {
                val temp: ObjectNotifyCard2 = notifyCardList[j]
                notifyCardList[j] = notifyCardList[j - 1]
                notifyCardList[j - 1] = temp
                j--
            }
        }
    }

    private fun updateTime() {
        val sdf = SimpleDateFormat("yyyyMMdd", Locale.TAIWAN)
        val dt = Date()
        global.date = sdf.format(dt)
    }

    fun updateNotifyList(context: Context,clinic: List<ObjectClinicRoomNumber>) {
        getNotifyCardList(context)
        updateTime()
        if (notifyCardList.isNotEmpty()) {
            var i = 0
            while (i < notifyCardList.size) {

                //updated
                notifyCardList[i].currentNum = 0
                for (j in clinic.indices) {
                    if (notifyCardList[i].roomId == clinic[j].room_id && notifyCardList[i].date.toInt() - global.date.toInt() == 0) {
                        notifyCardList[i].currentNum = clinic[j].currentNum
                    }
                }
                //expired
                if (notifyCardList[i].date.toInt() - global.date.toInt() < 0) {
                    notifyCardList.removeAt(i)
                    i--
                }
                i++
            }

            saveNotifyList(context,notifyCardList)
            getNotifyCardList(context)
        }
    }

}