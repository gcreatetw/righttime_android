package tw.com.gcreate.righttime.adapters.deprecated.customCalendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.global
import tw.com.gcreate.righttime.util.*
import java.util.*

class CalendarAdapter(private val mActivity: FragmentActivity , private val calendarMonth: Int , private val dataList: MutableList<Date>) : RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

    private var currentSelectedPosition = 0
    private var onItemClickListener: OnItemClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_item_day , parent , false)
        v.layoutParams.width = (global.windowWidthPixels * 0.8 / 7 * 0.85).toInt()
        v.layoutParams.height = (global.windowWidthPixels * 0.8 / 7 * 0.85).toInt()
        return ViewHolder(v)

    }

    interface OnItemClickListener {
        fun onItemClick(view: View? , position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {

        if (payloads.isEmpty()) {

            if (dataList[position].getMonthSerial() != calendarMonth) {
                holder.date.text = ""
            } else {
                holder.date.text = dataList[position].dateInt.toString()
            }

            disablePastDay(holder , position)
            disableFuture14Day(holder , position)


            if (dataList[position].getDaySerial() == Date().getDaySerial() && dataList[position].getMonthSerial() == Date().getMonthSerial()) {
                currentSelectedPosition = position
                holder.itemView.isClickable = true
                holder.itemView.setBackgroundResource(R.drawable.solid_circle_colorprimary)
                holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.colorWhite))
            }


            holder.itemView.setOnClickListener {
                onItemClickListener!!.onItemClick(holder.itemView , position)
                holder.itemView.setBackgroundResource(R.drawable.solid_circle_colorprimary)
                holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.colorWhite))
                if (position != currentSelectedPosition) {
                    notifyItemChanged(currentSelectedPosition , 0)
                }
                currentSelectedPosition = position
            }


        } else {
            val type = payloads[0] as Int
            when (type) {
                0 -> {
                    holder.itemView.setBackgroundResource(0)
                    holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.colorBlack))
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        // initial clinicTypeCard
        val date: TextView = v.findViewById(R.id.tv_value)

    }


    private fun disablePastDay(holder: ViewHolder, position: Int) {
        /** 將上個月的日期以及本月份已過去之日期取消 Focus*/
        if (dataList[position].getDaySerial() < Date().getDaySerial() && dataList[position].getMonthSerial() == Date().getMonthSerial()) {
            holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.red_fc0000))
            holder.itemView.isFocusable = false
            holder.itemView.isFocusableInTouchMode = false
            holder.itemView.isClickable = false
            holder.itemView.isEnabled = false
        }
    }

    private fun disableFuture14Day(holder: ViewHolder, position: Int) {
        /** 將超過今日14天的日期取消 Focus*/
        if (dataList[position].getMonthSerial() == Date().getMonthSerial()) {
            //  跟今天同個月分
            if (dataList[position].getDaySerial() > Date().getDaySerial() + 13) {
                holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.red_fc0000))
                holder.itemView.isFocusable = false
                holder.itemView.isFocusableInTouchMode = false
                holder.itemView.isClickable = false
                holder.itemView.isEnabled = false
            }
        } else {
            //  今天的下個月分
            if (dataList[position].dateFullInt > Date().getFieldByCount(Calendar.DAY_OF_MONTH , 14).dateFullInt) {
                holder.date.setTextColor(ContextCompat.getColor(mActivity , R.color.red_fc0000))
                holder.itemView.isFocusable = false
                holder.itemView.isFocusableInTouchMode = false
                holder.itemView.isClickable = false
                holder.itemView.isEnabled = false
            }
        }

    }


}