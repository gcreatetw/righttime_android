package tw.com.gcreate.righttime.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import tw.com.gcreate.righttime.MainActivity
import tw.com.gcreate.righttime.R

class FirebaseNotificationService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fb", token).apply()
        Log.i("MyFirebaseService", "token $token")
        //        AdLocus.getInstance().updatePushToken(token);
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        //Log.i(TAG, "onMessageReceived: " + remoteMessage.getFrom());
        if (remoteMessage.notification != null) {
            //Log.i(TAG, "" + remoteMessage.getNotification().getBody());
            sendNotification(this, remoteMessage.notification!!.title, remoteMessage.notification!!.body)
        }

        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            Map<String, String> data = remoteMessage.getData();
//            Logger.d(TAG, "Message data payload: " + remoteMessage.getData());
//            if (TextUtils.equals(data.get(GetFCMDataResponse.TAG_TARGET), Constants.TAG_FCM_TARGET))
//                AdLocus.getInstance().sendFCMMessage(this, data);
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
////                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
////                handleNow();
//            }
//        }
    }

    override fun onMessageSent(s: String) {
        super.onMessageSent(s)
        //Log.i(TAG, "onMessageSent" + s);
    }

    companion object {
        fun getToken(context: Context): String? {
            return context.getSharedPreferences("_", MODE_PRIVATE).getString("fb", "empty")
        }
    }

    private fun sendNotification(context: Context, messageTitle: String?, messageBody: String?) {
        val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val channelId = context.getString(R.string.notification_channel_id)
        val channelDescription = "RightTime Clinic Number Notification"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var notificationChannel = notificationManager.getNotificationChannel(channelId)
            if (notificationChannel == null) {
                val importance = NotificationManager.IMPORTANCE_HIGH //Set the importance level
                notificationChannel = NotificationChannel(channelId, channelDescription, importance)
                notificationChannel.lightColor = Color.GREEN //Set if it is necesssary
                notificationChannel.enableVibration(true) //Set if it is necesssary
                notificationManager.createNotificationChannel(notificationChannel)
            }
        }
        val intent = Intent(context, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_IMMUTABLE)
        val splitLine = messageBody?.split("，")?.toTypedArray()
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context)
            .setSmallIcon(R.mipmap.push2)
            .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
            //.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setChannelId(channelId)
            .setContentIntent(pendingIntent)

        if (splitLine?.size != 0) {
            notificationBuilder.setContentTitle(messageTitle).setContentText(messageBody?.replace(splitLine!![0] + "，", ""))
        }

        notificationManager.notify(0, notificationBuilder.build())
    }
}