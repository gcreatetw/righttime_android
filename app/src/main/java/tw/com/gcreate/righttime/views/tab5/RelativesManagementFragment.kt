package tw.com.gcreate.righttime.views.tab5

import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.swipCallback.UserRelativesSwipeToDeleteCallback
import tw.com.gcreate.righttime.adapters.tab5.RelativesManagementAdapter
import tw.com.gcreate.righttime.dataStore
import tw.com.gcreate.righttime.databinding.FragmentMemberRelativesManagementBinding
import tw.com.gcreate.righttime.model.MainViewModel
import tw.com.gcreate.righttime.model.MainViewModelFactory
import tw.com.gcreate.righttime.sharedpreferences.RelativesDataMaintain
import tw.com.gcreate.righttime.util.SomeFunc
import tw.com.gcreate.righttime.webAPI.apiHandle.ApiController
import tw.com.gcreate.righttime.webAPI.client.GcreateApiClient
import tw.com.gcreate.righttime.webAPI.client.gcRequest.RequestUserInfoModify
import tw.com.gcreate.righttime.webAPI.client.gcResponse.ResponseSimpleFormat

class RelativesManagementFragment : Fragment() {

    private lateinit var binding: FragmentMemberRelativesManagementBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMemberRelativesManagementBinding.inflate(inflater, container, false)

        SomeFunc.setToolbarView(requireActivity(), this,
            binding.toolbarMember3.materialToolbar, "親友管理",
            binding.toolbarMember3.toolbarLogo, true)

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

            // 親友關係，移除自己
            val dataList = RelativesDataMaintain.getRelativesDataList(requireContext())
            dataList.removeAt(0)

            val mAdapter = RelativesManagementAdapter( model.appState.gCMemberId.get()!!,dataList)
            adapter = mAdapter

            val itemTouchHelper = ItemTouchHelper(UserRelativesSwipeToDeleteCallback(mAdapter))
            itemTouchHelper.attachToRecyclerView(binding.rvContent)

            mAdapter.apply {
                setEmptyView(R.layout.layout_no_relatives)
                addFooterView(layoutInflater.inflate(R.layout.rv_item_empty_footer, null))
                emptyLayout?.findViewById<Button>(R.id.btn_add_relative)?.setOnClickListener {
                    val action = RelativesManagementFragmentDirections.actionRelativesManagementFragmentToRelativesAddFragment()
                    findNavController().navigate(action)
                }
                setOnItemClickListener { _, _, position ->
                    val action = RelativesManagementFragmentDirections.actionRelativesManagementFragmentToRelativesAddFragment()
                    action.isAdd = false
                    action.relativePositon = position
                    findNavController().navigate(action)
                }
            }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.relative, menu)
        val relativeDataList = RelativesDataMaintain.getRelativesDataList(requireContext())
        menu.findItem(R.id.relative_add).isVisible = relativeDataList.size != 0
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.relative_add) {
            val action = RelativesManagementFragmentDirections.actionRelativesManagementFragmentToRelativesAddFragment()
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }

}