package tw.com.gcreate.righttime.webAPI.client.gcRequest

data class RequestGcRegistered(
    val cell_phone: String,
    val password: String,
    val person_id: String,
    val name: String,
    val birth: String,
    val sex: String,
    val FCM_token:String,
)