package tw.com.gcreate.righttime.views.tab5.feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import tw.com.gcreate.righttime.R
import tw.com.gcreate.righttime.adapters.tab5.FeedbackTabAdapter
import tw.com.gcreate.righttime.databinding.FragmentFeedbackTypeBinding


class FeedbackTypeFragment : Fragment() {

    private lateinit var binding: FragmentFeedbackTypeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFeedbackTypeBinding.inflate(inflater, container, false)

        binding.toolbar.materialToolbar.apply {
            title = ""
            setNavigationIcon(R.drawable.toolbar_icon_back)
            setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }
        }

        //TabLayout
        val tabList = listOf("推薦診所", "問題回饋")
        val tabIconList = listOf(R.drawable.navigation_icon_recommend,R.drawable.icon_problem_feedback)
        val tabAdapter = FeedbackTabAdapter(this, tabList)
        binding.apply {
            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = tabAdapter
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                tab.customView = customTab(tabList[position], tabIconList[position])
            }.attach()

            return binding.root
        }
    }

    private fun customTab(name: String, iconID: Int): View {
        val newTab: View = LayoutInflater.from(requireActivity()).inflate(R.layout.tab_item, null)
        val tv = newTab.findViewById(R.id.tabtext) as TextView
        val im = newTab.findViewById(R.id.tabicon) as ImageView

        im.setImageResource(iconID)
        tv.text = name

        return newTab
    }

}