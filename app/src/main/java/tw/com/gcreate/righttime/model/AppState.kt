package tw.com.gcreate.righttime.model

import androidx.databinding.ObservableField
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking


class AppState(
    private val dataStore: DataStore<Preferences>,
) {

    val gCMemberId = ObservableField("")
    val gCLoginMobile = ObservableField("")
    val gCLoginPassword = ObservableField("")

    private var MEMBERID = stringPreferencesKey("member_id")
    private var LOGINMOBILE = stringPreferencesKey("login_mobile")
    private var LOGINPASSWORD = stringPreferencesKey("login_password")


    private val gCMemberIdFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[MEMBERID] ?: ""
        } as Flow<String>

    private val gCLoginMobileFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[LOGINMOBILE] ?: ""
        } as Flow<String>

    private val gCLoginPasswordFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[LOGINPASSWORD] ?: ""
        } as Flow<String>

    init {
        runBlocking {
            gCMemberId.set(gCMemberIdFlow.first())
            gCLoginMobile.set(gCLoginMobileFlow.first())
            gCLoginPassword.set(gCLoginPasswordFlow.first())
        }
    }

    suspend fun setGCMemberId(v: String) {
        gCMemberId.set(v)
        dataStore.edit { settings ->
            settings[MEMBERID] = v
        }
    }

    suspend fun setGCLoginMobile(v: String) {
        gCLoginMobile.set(v)
        dataStore.edit { settings ->
            settings[LOGINMOBILE] = v
        }
    }

    suspend fun setGCLoginPassword(v: String) {
        gCLoginPassword.set(v)
        dataStore.edit { settings ->
            settings[LOGINPASSWORD] = v
        }
    }


}