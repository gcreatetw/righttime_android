package tw.com.gcreate.righttime.views.tab5

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import tw.com.gcreate.righttime.databinding.FragmentPolicyWebViewBinding
import tw.com.gcreate.righttime.util.SomeFunc


class PolicyWebViewFragment : Fragment() {

    private val args: PolicyWebViewFragmentArgs by navArgs()

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentPolicyWebViewBinding.inflate(inflater, container, false)
        SomeFunc.setToolbarView(requireActivity(), this, binding.toolbar.materialToolbar, "", binding.toolbar.toolbarLogo, false)

        binding.webView.apply {
            settings.apply {
                javaScriptEnabled = true
                builtInZoomControls = true
                setSupportZoom(true)
                displayZoomControls = true
                blockNetworkImage = false
                databaseEnabled = true
                domStorageEnabled = true
                cacheMode = WebSettings.LOAD_NO_CACHE
                useWideViewPort = false
                userAgentString = "Android"
            }

            binding.webView.webViewClient = WebViewClient()
            when (args.linkType) {
                0 -> {
                    loadUrl("https://www.right-time.com.tw/privacy/")
                }
                1 -> {
                    loadUrl("https://www.right-time.com.tw/delete/")
                }
            }
        }

        return binding.root
    }
}